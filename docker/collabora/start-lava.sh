#!/bin/sh

set -e

DATABASE=`python3 -c 'import os; from urllib.parse import urlparse; url = urlparse(os.environ["DATABASE_URL"]); print("{}:{}".format(url.hostname, url.port), end="")'`

/usr/bin/wait-for-it -t 30 $DATABASE

# These are based on the commands called by the package's
# /usr/share/lava-server/postinst.py.
lava-server manage migrate --noinput --fake-initial
lava-server manage drop_materialized_views
lava-server manage refresh_queries --all

# Make all device types available by default.
lava-server manage device-types add '*'

# These variables are based on the systemd services shipped by the package.
# Most of them are optional, see the sample blank-env file.
if [ -z "$LOGLEVEL" ]; then
    LOGLEVEL=DEBUG
fi

/usr/bin/lava-server manage lava-publisher --host '*' --port 8001 &

while :; do
    /usr/bin/lava-server manage lava-scheduler $EVENT_URL $IPV6
done &

if [ -z "$WORKERS" ]; then
    WORKERS=4
fi

if [ -z "$WORKER_CLASS" ]; then
    WORKER_CLASS=eventlet
fi

if [ -z "$BIND" ]; then
    BIND="--bind 0.0.0.0:8000"
fi

if [ -z "$WORKER_CONNECTIONS" ]; then
    # 23*4 == 92
    WORKER_CONNECTIONS=23
fi

exec /usr/bin/gunicorn3 lava_server.wsgi -u lavaserver -g lavaserver --worker-class $WORKER_CLASS --workers $WORKERS --worker-connections $WORKER_CONNECTIONS $BIND $RELOAD $TIMEOUT
