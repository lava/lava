#!/bin/bash

# Exit script on any error
set -e 

OPTIONS=""
MAKE_OPTIONS=""

if [ "$COMPILER" == "gcc" ]; then
  echo "Building with GCC";
  # without icecc: no options required
  OPTIONS="$OPTIONS -DCMAKE_CXX_COMPILER=/usr/lib/icecc/bin/g++ -DCMAKE_C_COMPILER=/usr/lib/icecc/bin/gcc"
  MAKE_OPTIONS="-j16"
  export ICECC_CXX=/usr/bin/g++ ; export ICECC_CC=/usr/bin/gcc
elif [ "$COMPILER" == "clang" ]; then
  OPTIONS="$OPTIONS -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang"
  echo "Building with CLANG";
elif [ "$COMPILER" == "" ]; then
  echo "No COMPILER variable specified. Use system default settings.";
else
  echo "Unsupported COMPILER variable specified. Supported COMPILER options: 'gcc', 'clang'";
  return 1
fi  

if [ "$LANGUAGE" == "C++11" ]; then
  echo "Building with C++11";
  OPTIONS="$OPTIONS -DCMAKE_CXX_FLAGS='-std=c++11' -DCMAKE_CXX_STANDARD=11"
elif [ "$LANGUAGE" == "C++14" ]; then
  echo "Building with C++14";
  OPTIONS="$OPTIONS -DCMAKE_CXX_FLAGS='-std=c++14' -DCMAKE_CXX_STANDARD=14"
elif [ "$LANGUAGE" == "C++17" ]; then
  echo "Building with C++17";
  OPTIONS="$OPTIONS -DCMAKE_CXX_FLAGS='-std=c++17' -DCMAKE_CXX_STANDARD=17"
elif [ "$LANGUAGE" == "" ]; then
  echo "No LANGUAGE variable specified. Use system default settings."
else
  echo "Unsupported LANGUAGE variable specified. Supported LANGUAGE options: 'C++11', 'C++14', 'C++17'"
  return 1
fi 

if [ "$BUILDDIR" == ""]; then
  echo "No BUILDDIR variable specified. Set default build directory.";
  BUILDDIR = "build"
fi

#=====================================
# Color Settings:
#=====================================
NC='\033[0m'
OUTPUT='\033[0;32m'
WARNING='\033[0;93m'


echo -e "${OUTPUT}"
echo ""
echo "======================================================================"
echo "Basic configuration details:"
echo "======================================================================"
echo -e "${NC}"

echo "Compiler:     $COMPILER"
echo "Options:      $OPTIONS"
echo "Language:     $LANGUAGE"
echo "Make Options: $MAKE_OPTIONS"
echo "Build Dir:    $BUILDDIR"
echo "Path:         $PATH"

echo -e "${OUTPUT}"
echo ""
echo "========================="
echo "Building Release versions"
echo "========================="
echo -e "${NC}"

# Create build dir if it does not exist
if [ ! -d $BUILDDIR ]; then
  mkdir $BUILDDIR
fi

cd $BUILDDIR
cmake -DCMAKE_BUILD_TYPE=Release $OPTIONS ..
cmake .
make $MAKE_OPTIONS

# back to root
cd ..
