# This file contains basic defines and settings that are not provided by cmake out-of-the-box


# Setup important variables
#  - Operating System : LAVA_LINUX LAVA_MAC LAVA_WINDOWS  (also: LAVA_${OPERATING_SYSTEM})
#  - Compiler         : LAVA_GCC LAVA_CLANG LAVA_MSVC     (also: LAVA_${COMPILER})

SET(LINUX   ${UNIX})
SET(WINDOWS ${MSVC})
SET(MAC     ${APPLE})
IF(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    SET(CLANG ON)
    SET(COMPILER CLANG)
    SET(GCC OFF)
ELSE()
    IF(CMAKE_CXX_COMPILER MATCHES ".*[cg]\\+\\+")
        SET(GCC ON)
        SET(COMPILER GCC)
    ELSE()
        SET(GCC OFF)
    ENDIF()
    SET(CLANG OFF)
ENDIF()
IF(${MSVC})
    SET(COMPILER MSVC)
ENDIF()
IF(${WINDOWS})
    SET(OPERATING_SYSTEM WINDOWS)
ENDIF()
IF(${LINUX})
    SET(OPERATING_SYSTEM LINUX)
ENDIF()
IF(${MAC})
    SET(OPERATING_SYSTEM MAC)
ENDIF()

# Build Type (Debug, Release, Deploy, ...)
IF(NOT CMAKE_BUILD_TYPE)
    SET(CMAKE_BUILD_TYPE Release CACHE STRING "" FORCE)
ENDIF()
SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS Release Debug Deploy)

if(CMAKE_BUILD_TYPE STREQUAL Debug)
    set(LAVA_BUILD_TYPE DEBUG)
elseif (CMAKE_BUILD_TYPE STREQUAL Release)
    set(LAVA_BUILD_TYPE RELEASE)
elseif (CMAKE_BUILD_TYPE STREQUAL Deploy)
    set(LAVA_BUILD_TYPE DEPLOY)
else()
    message(ERROR "Unsupported build type! Please specify Debug, Release, or Deploy.")
endif()

# Set cmake search path
SET(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

