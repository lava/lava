# Copyright (C) 2024 Collabora Limited
#
# Author: Igor Ponomarev <igor.ponomarev@collabora.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
from __future__ import annotations

from rest_framework.routers import DefaultRouter

from .testjob import TestJobViewset

router = DefaultRouter()
jobs_router = router.register(r"jobs", TestJobViewset)
