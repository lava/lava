#include "PipelineVertexInputStateCreateInfo.hh"
#include "GraphicsPipelineCreateInfo.hh"

namespace lava
{

vk::PipelineVertexInputStateCreateInfo *
PipelineVertexInputStateCreateInfo::get()
{
    mInfo.setVertexBindingDescriptionCount(uint32_t(mBindings.size()))
        .setPVertexBindingDescriptions(mBindings.data())
        .setVertexAttributeDescriptionCount(uint32_t(mAttributes.size()))
        .setPVertexAttributeDescriptions(mAttributes.data());
    return &mInfo;
}
} // namespace lava
