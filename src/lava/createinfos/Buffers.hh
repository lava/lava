#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

struct BufferCreateInfo : vk::BufferCreateInfo {
    SharedBuffer create(SharedDevice const &device) const;

    BufferCreateInfo &addUsage(vk::BufferUsageFlagBits added)
    {
        usage = usage | added;
        return *this;
    }

    BufferCreateInfo &removeUsage(vk::BufferUsageFlagBits removed)
    {
        usage = usage & ~removed;
        return *this;
    }
};

inline BufferCreateInfo
stagingBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferSrc);

    return info;
}

inline BufferCreateInfo
storageBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eStorageBuffer |
                  vk::BufferUsageFlagBits::eTransferDst |
                  vk::BufferUsageFlagBits::eTransferSrc);
    return info;
}

inline BufferCreateInfo
arrayBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferDst |
                  vk::BufferUsageFlagBits::eVertexBuffer);
    return info;
}

inline BufferCreateInfo
indexBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferDst |
                  vk::BufferUsageFlagBits::eIndexBuffer);
    return info;
}

inline BufferCreateInfo
indexArrayBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferDst |
                  vk::BufferUsageFlagBits::eVertexBuffer |
                  vk::BufferUsageFlagBits::eIndexBuffer);
    return info;
}

inline BufferCreateInfo
downloadBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferDst);

    return info;
}

inline BufferCreateInfo
uniformBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferDst |
                  vk::BufferUsageFlagBits::eUniformBuffer);

    return info;
}

inline BufferCreateInfo
raytracingBuffer(size_t size = 0)
{
    BufferCreateInfo info;
    info.setSize(size);
    info.setUsage(vk::BufferUsageFlagBits::eTransferDst |
                  vk::BufferUsageFlagBits::eRayTracingNV);
    return info;
}

} // namespace lava
