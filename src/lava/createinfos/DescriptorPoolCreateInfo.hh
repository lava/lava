#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class DescriptorPoolCreateInfo
{
  public:
    using Self = DescriptorPoolCreateInfo;

    DescriptorPoolCreateInfo() = default;
    DescriptorPoolCreateInfo(vk::DescriptorPoolCreateInfo const &from);

    Self &setFlags(vk::DescriptorPoolCreateFlags flags);

    Self &allowFreeing(bool val = true);

    Self &setMaxSets(uint32_t count);

    Self &addSize(vk::DescriptorType type, uint32_t count)
    {
        mSizes.push_back({type, count});
        return *this;
    }

    operator vk::DescriptorPoolCreateInfo();

    SharedDescriptorPool create(SharedDevice const &device);

  protected:
    vk::DescriptorPoolCreateInfo mInfo;
    std::vector<vk::DescriptorPoolSize> mSizes;
};

} // namespace lava
