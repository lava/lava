#pragma once
#include "PipelineShaderStageCreateInfo.hh"
#include <lava/fwd.hh>

namespace lava
{
class ComputePipelineCreateInfo
{
  public:
    ComputePipelineCreateInfo() {}

    ComputePipelineCreateInfo &
    setModule(lava::SharedShaderModule const &module,
              std::string const &name = "main")
    {
        mStage.setModule(module);
        mStage.setStage(vk::ShaderStageFlagBits::eCompute);
        mStage.setName(name);
        return *this;
    }

    ComputePipelineCreateInfo &
    setLayout(lava::SharedPipelineLayout const &layout)
    {
        mLayout = layout;
        return *this;
    }

    template <class T>
    ComputePipelineCreateInfo &specialize(uint32_t constantId, T value)
    {
        mStage.specialize(constantId, value);
        return *this;
    }

    SharedComputePipeline create();

  protected:
    lava::PipelineShaderStageCreateInfo mStage;
    lava::SharedPipelineLayout mLayout;
    friend class ComputePipeline;
};

} // namespace lava
