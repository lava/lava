#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{

class PipelineLayoutCreateInfo
{
  public:
    PipelineLayoutCreateInfo(vk::PipelineLayoutCreateInfo const &info);

    /// In most cases, you'll just want to use Device::createPipelineLayout
    /// instead
    PipelineLayoutCreateInfo(
        std::vector<vk::DescriptorSetLayout> const &layouts = {},
        std::vector<vk::PushConstantRange> const &ranges = {});

    operator vk::PipelineLayoutCreateInfo();

    /// Set the push constants block for this layout to T for all shader
    /// stages.
    template <class T> void setPushConstants()
    {
        vk::PushConstantRange range;
        range.size = sizeof(T);
        range.offset = 0;
        range.stageFlags = vk::ShaderStageFlagBits::eAllGraphics;
        mRanges.resize(1);
        mRanges.front() = range;
    }

    void addSetLayout(vk::DescriptorSetLayout layout)
    {
        mLayouts.push_back(layout);
    }
    void addPushConstantRange(vk::PushConstantRange range)
    {
        mRanges.push_back(range);
    }

    template <class T>
    void addPushConstantRange(
        vk::ShaderStageFlags stages = vk::ShaderStageFlagBits::eAllGraphics)
    {

        vk::PushConstantRange range;
        if (!mRanges.empty()) {
            auto const &back = mRanges.back();
            range.offset = back.offset + back.size;
        }
        range.size = sizeof(T);
        range.stageFlags = stages;

        mRanges.push_back(range);
    }

    std::vector<vk::PushConstantRange> const &ranges() const
    {
        return mRanges;
    }

  protected:
    vk::PipelineLayoutCreateInfo mInfo;
    std::vector<vk::DescriptorSetLayout> mLayouts;
    std::vector<vk::PushConstantRange> mRanges;
};
} // namespace lava
