#pragma once
#include "ExtensionStructure.hh"
#include <lava/common/GlLikeFormats.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

struct SubpassDescription {
    std::vector<vk::AttachmentReference> colorAttachments;
    std::vector<vk::AttachmentReference> inputAttachments;
    vk::AttachmentReference depthAttachment;
    vk::SubpassDescriptionFlags flags;

    SubpassDescription &depth(uint32_t index)
    {
        depthAttachment = vk::AttachmentReference(
            index, vk::ImageLayout::eDepthStencilAttachmentOptimal);
        return *this;
    }

    SubpassDescription &colors(std::vector<uint32_t> indices)
    {
        colorAttachments.resize(indices.size());
        std::transform(begin(indices), end(indices),
                       begin(colorAttachments), [](uint32_t idx) {
                           return vk::AttachmentReference(
                               idx,
                               vk::ImageLayout::eColorAttachmentOptimal);
                       });
        return *this;
    }

    SubpassDescription &inputsColor(std::vector<uint32_t> indices)
    {
        auto oldSize = inputAttachments.size();
        inputAttachments.resize(oldSize + indices.size());
        std::transform(begin(indices), end(indices),
                       begin(inputAttachments) + oldSize, [](uint32_t idx) {
                           return vk::AttachmentReference(
                               idx,
                               vk::ImageLayout::eShaderReadOnlyOptimal);
                       });
        return *this;
    }

    SubpassDescription &inputsDepth(std::vector<uint32_t> indices)
    {
        auto oldSize = inputAttachments.size();
        inputAttachments.resize(oldSize + indices.size());
        std::transform(
            begin(indices), end(indices), begin(inputAttachments) + oldSize,
            [](uint32_t idx) {
                return vk::AttachmentReference(
                    idx, vk::ImageLayout::eDepthStencilReadOnlyOptimal);
            });
        return *this;
    }

    SubpassDescription &perViewAttributes(bool active = true,
                                          bool allComponents = false)
    {
        if (active) {
            flags = vk::SubpassDescriptionFlagBits::ePerViewAttributesNVX;
            if (!allComponents)
                flags |= vk::SubpassDescriptionFlagBits::
                    ePerViewPositionXOnlyNVX;
        } else {
            flags = {};
        }
        return *this;
    }

    operator vk::SubpassDescription();
};

struct SubpassDependency : vk::SubpassDependency {
    SubpassDependency(uint32_t from, uint32_t to)
        : vk::SubpassDependency(from, to)
    {
        dependencyFlags = vk::DependencyFlagBits::eByRegion;
    }

    static SubpassDependency first()
    {
        return SubpassDependency(VK_SUBPASS_EXTERNAL, 0);
    }

    static SubpassDependency last(uint32_t current)
    {
        return SubpassDependency(current, VK_SUBPASS_EXTERNAL);
    }

    SubpassDependency &readInVertexShader()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eLateFragmentTests;
        srcAccessMask |= vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eVertexShader;
        dstAccessMask |= vk::AccessFlagBits::eShaderRead;
        return *this;
    }

    SubpassDependency &readInFragmentShader()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eColorAttachmentOutput;
        srcAccessMask |= vk::AccessFlagBits::eColorAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eFragmentShader;
        dstAccessMask |= vk::AccessFlagBits::eShaderRead;
        return *this;
    }

    SubpassDependency &reuseDepthStencil()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eLateFragmentTests;
        srcAccessMask |= vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eEarlyFragmentTests;
        dstAccessMask |= vk::AccessFlagBits::eDepthStencilAttachmentRead;
        return *this;
    }

    SubpassDependency &reuseColor()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eColorAttachmentOutput;
        srcAccessMask |= vk::AccessFlagBits::eColorAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eColorAttachmentOutput;
        dstAccessMask |= vk::AccessFlagBits::eColorAttachmentRead;
        return *this;
    }

    SubpassDependency &sampleColor()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eColorAttachmentOutput;
        srcAccessMask |= vk::AccessFlagBits::eColorAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eFragmentShader;
        if (srcSubpass == VK_SUBPASS_EXTERNAL ||
            dstSubpass == VK_SUBPASS_EXTERNAL)
            dstStageMask |= vk::PipelineStageFlagBits::eVertexShader;
        dstAccessMask |= vk::AccessFlagBits::eShaderRead;
        return *this;
    }

    SubpassDependency &transferColor()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eColorAttachmentOutput;
        srcAccessMask |= vk::AccessFlagBits::eColorAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eTransfer;
        dstAccessMask |= vk::AccessFlagBits::eTransferRead;
        return *this;
    }

    SubpassDependency &transferDepth()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eEarlyFragmentTests |
                        vk::PipelineStageFlagBits::eLateFragmentTests;
        srcAccessMask |= vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eTransfer;
        dstAccessMask |= vk::AccessFlagBits::eTransferRead;
        return *this;
    }

    SubpassDependency &sampleDepthStencil()
    {
        srcStageMask |= vk::PipelineStageFlagBits::eLateFragmentTests;
        srcAccessMask |= vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        dstStageMask |= vk::PipelineStageFlagBits::eFragmentShader;
        if (srcSubpass == VK_SUBPASS_EXTERNAL ||
            dstSubpass == VK_SUBPASS_EXTERNAL)
            dstStageMask |= vk::PipelineStageFlagBits::eVertexShader;
        dstAccessMask |= vk::AccessFlagBits::eShaderRead;
        return *this;
    }
};

struct AttachmentDescription {
    AttachmentDescription() = default;
    AttachmentDescription(vk::AttachmentDescription const &o) : mInfo(o) {}

    /// Attachment for Depth/Stencil Buffer. Loads/Stores contents by
    /// default(!)
    static AttachmentDescription depth24Stencil8();
    /// Attachment for Depth/Stencil Buffer. Loads/Stores contents by
    /// default(!)
    static AttachmentDescription depth16();
    /// Attachment for Depth/Stencil Buffer. Loads/Stores contents by
    /// default(!)
    static AttachmentDescription depth32float();

    /// Attachment for Color Buffer. Loads/Stores contents by default(!)
    static AttachmentDescription color(lava::GenericFormat format);

    /// Marks the Attachment for clearing at the start of the pass
    AttachmentDescription &clear();
    /// Marks the Attachment for discarding the results at the end of the
    /// pass
    AttachmentDescription &discard();

    AttachmentDescription &finalLayout(vk::ImageLayout layout);

    /// Mark this attachment to be used as attachment again in a subsequent
    /// pass
    AttachmentDescription &finalLayout_Attachment();

    /// Mark this attachment to be used for sampling in a subsequent pass
    AttachmentDescription &finalLayout_ShaderRead();

    AttachmentDescription &finalLayout_TransferSrc();
    AttachmentDescription &finalLayout_DepthStencilRead();
    AttachmentDescription &finalLayout_PresentSrc();

    operator vk::AttachmentDescription &() { return mInfo; }
    operator vk::AttachmentDescription const &() const { return mInfo; }

    vk::Format format() const { return mInfo.format; }

  private:
    vk::AttachmentDescription mInfo;
};

// make sure we can pass AttachmentDescription directly to vulkan
static_assert(
    sizeof(AttachmentDescription) == sizeof(vk::AttachmentDescription),
    "AttachmentDescription between lava and vulkan have different sizes.");

class RenderPassCreateInfo
{
  public:
    using Self = RenderPassCreateInfo;
    RenderPassCreateInfo();

    /// a Renderpass with color and depth buffer and a single subpass,
    /// transitions color attachment to ePresentSrcKHR layout
    static RenderPassCreateInfo
    simpleForward(lava::GenericFormat colorFormat = Format::RGBA8);

    Self &addDependency(vk::SubpassDependency const &dep)
    {
        mDependencies.push_back(dep);
        return *this;
    }
    Self &addSubpass(SubpassDescription const &sub)
    {
        mSubpasses.push_back(sub);
        return *this;
    }
    Self &addAttachment(AttachmentDescription const &att)
    {
        mAttachments.push_back(att);
        return *this;
    }

    /// Returns a subpass that uses all of the passes' attachments
    SubpassDescription fullSubpass();

    /**
     * @brief Automatically place conservative dependencies around the
     * subpasses
     *
     * Waits for the complete pipeline to run through before starting the
     * subpass. Similarly waits for the whole subpass to be done before
     * continuing with the pipeline. This makes interaction with the rest of
     * the pipeline as safe as possible, but leaves room for optimization
     * potentially.
     *
     * The subpasses among themselves are synchronized such that shader
     * reads of the current subpass wait for the color attachment output of
     * the previous subpass to be complete. This is safe regarding the
     * access patterns allowed between subpasses (only allowed to read the
     * fragments of previous subpasses with the same coordinate).
     */
    void autoAddDependencies();

    void setNext(SharedExtensionStructure const &next) { mNext = next; }

    operator vk::RenderPassCreateInfo();

    std::vector<SubpassDescription> const &getSubpasses() const
    {
        return mSubpasses;
    }

    SharedExtensionStructure const &next() const { return mNext; }

    std::vector<AttachmentDescription> const &attachments() const
    {
        return mAttachments;
    }

    SharedRenderPass create(const SharedDevice &device) const;

  protected:
    std::vector<vk::SubpassDependency> mDependencies;
    std::vector<SubpassDescription> mSubpasses;
    std::vector<AttachmentDescription> mAttachments;
    vk::RenderPassCreateInfo mInfo;
    SharedExtensionStructure mNext;

    std::vector<vk::SubpassDescription> mVulkanSubpasses;
};
} // namespace lava
