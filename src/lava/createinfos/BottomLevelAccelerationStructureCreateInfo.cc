#include "BottomLevelAccelerationStructureCreateInfo.hh"
#include <lava/objects/BottomLevelAccelerationStructure.hh>

namespace lava
{

BottomLevelAccelerationStructureCreateInfo::Self &
BottomLevelAccelerationStructureCreateInfo::addTriangleGeometry(
    const SharedBuffer &vbuffer, uint32_t voffset, uint32_t vcount,
    uint32_t vstride, GenericFormat vformat, const SharedBuffer &ibuffer,
    uint32_t ioffset, uint32_t icount, vk::IndexType itype)
{

    mBuffers.insert(vbuffer);
    mBuffers.insert(ibuffer);

    vk::GeometryNV geo;
    geo.setGeometryType(vk::GeometryTypeNV::eTriangles)
        .geometry.triangles.setIndexData(ibuffer->handle())
        .setIndexType(itype)
        .setIndexOffset(ioffset)
        .setIndexCount(icount)
        .setVertexData(vbuffer->handle())
        .setVertexCount(vcount)
        .setVertexFormat(vformat.vkhpp())
        .setVertexOffset(voffset)
        .setVertexStride(vstride);
    mGeometries.push_back(geo);

    return *this;
}

BottomLevelAccelerationStructureCreateInfo::Self &
BottomLevelAccelerationStructureCreateInfo::addAabbGeometry(
    const SharedBuffer &aabbbuffer, uint32_t count, uint32_t offset,
    uint32_t stride)
{
    mBuffers.insert(aabbbuffer);

    vk::GeometryNV geo;
    geo.setGeometryType(vk::GeometryTypeNV::eAabbs)
        .geometry.aabbs.setAabbData(aabbbuffer->handle())
        .setOffset(offset)
        .setStride(stride)
        .setNumAABBs(count);
    mGeometries.push_back(geo);

    return *this;
}

BottomLevelAccelerationStructureCreateInfo::Self &
BottomLevelAccelerationStructureCreateInfo::addTriangleGeometry(
    SharedBuffer const &vbuffer, SharedBuffer const &ibuffer,
    vk::IndexType itype)
{
    const int vec3bytes = 3 * sizeof(float);
    auto n_verts = vbuffer->size() / vec3bytes;
    auto n_indices =
        ibuffer->size() / (itype == vk::IndexType::eUint16 ? 2 : 4);

    return addTriangleGeometry(vbuffer, 0, n_verts, vec3bytes,
                               vk::Format::eR32G32B32Sfloat, ibuffer, 0,
                               n_indices, itype);
}

SharedBottomLevelAccelerationStructure
BottomLevelAccelerationStructureCreateInfo::create(
    const lava::SharedDevice &on)
{
    return std::make_shared<BottomLevelAccelerationStructure>(*this, on);
}

vk::AccelerationStructureCreateInfoNV
BottomLevelAccelerationStructureCreateInfo::info() const
{
    vk::AccelerationStructureCreateInfoNV result;
    result.info.setGeometryCount(mGeometries.size())
        .setPGeometries(mGeometries.data())
        .setType(vk::AccelerationStructureTypeNV::eBottomLevel);
    return result;
}

} // namespace lava
