#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

#include <lava/createinfos/PipelineShaderStageCreateInfo.hh>

namespace lava
{

struct RayTracingPipelineCreateInfo {
    using Self = RayTracingPipelineCreateInfo;

    using GroupType = vk::RayTracingShaderGroupTypeNV;
    struct Group {
        SharedShaderModule module;
        GroupType type = GroupType::eGeneral;
    };

    RayTracingPipelineCreateInfo(SharedPipelineLayout const &layout);
    SharedRayTracingPipeline create() const;

    /// Convenience Interface: add shader modules as shader groups. Stages
    /// and Groups are automatically created and deduplicated.
    Self &addRayGeneration(SharedShaderModule const &rgen);
    Self &addMiss(SharedShaderModule const &rmiss);
    Self &addTriangleHitGroup(SharedShaderModule const &closestHit,
                              SharedShaderModule const &anyHit = nullptr);
    Self &addProceduralHitGroup(SharedShaderModule const &intersection,
                                SharedShaderModule const &closestHit,
                                SharedShaderModule const &anyHit = nullptr);

    vk::RayTracingPipelineCreateInfoNV const &info();

    SharedPipelineLayout const &layout() const { return mLayout; }
    std::vector<vk::RayTracingShaderGroupCreateInfoNV> const &groups() const
    {
        return mGroups;
    }

    int firstRaygenIndex() const { return 0; }
    int firstHitIndex() const { return mNumRaygens; }
    int firstMissIndex() const { return mNumRaygens + mNumHitGroups; }

  protected:
    uint32_t insertModule(SharedShaderModule const &module);

    vk::RayTracingPipelineCreateInfoNV mInfo;
    std::vector<SharedShaderModule> mModules;
    std::vector<PipelineShaderStageCreateInfo> mStages;
    int mNumRaygens = 0, mNumHitGroups = 0;
    std::vector<vk::RayTracingShaderGroupCreateInfoNV> mGroups;
    SharedPipelineLayout mLayout;

  private:
    std::vector<vk::PipelineShaderStageCreateInfo> mStagesData;
};

} // namespace lava
