#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{
struct SamplerCreateInfo : vk::SamplerCreateInfo {
    /// Roughly the default way a texture is sampled in OpenGL
    SamplerCreateInfo()
    {
        magFilter = minFilter = vk::Filter::eLinear;
        mipmapMode = vk::SamplerMipmapMode::eLinear;
        addressModeU = addressModeV = addressModeW =
            vk::SamplerAddressMode::eRepeat;
        maxLod = VK_LOD_CLAMP_NONE;
        borderColor = vk::BorderColor::eFloatTransparentBlack;
        maxAnisotropy = 1.0;
    }

    SamplerCreateInfo(vk::SamplerCreateInfo const &info)
        : vk::SamplerCreateInfo(info)
    {
    }

    /// a non-shadow depth sampler
    static SamplerCreateInfo depth()
    {
        SamplerCreateInfo result;
        result.setAddressMode(vk::SamplerAddressMode::eClampToBorder);
        result.borderColor = vk::BorderColor::eFloatOpaqueWhite;
        return result;
    }

    /// a shadow sampler that
    static SamplerCreateInfo shadow()
    {
        auto result = depth();
        result.compareEnable = true;
        result.compareOp = vk::CompareOp::eGreaterOrEqual;
        return result;
    }

    SamplerCreateInfo &setAddressMode(vk::SamplerAddressMode mode)
    {
        addressModeU = addressModeV = addressModeW = mode;
        return *this;
    }

    SamplerCreateInfo &setFilter(vk::Filter filter)
    {
        minFilter = magFilter = filter;
        return *this;
    }

    SamplerCreateInfo &setUnnormalizedCoordinates(bool val = true)
    {
        unnormalizedCoordinates = val;

        if (val) {
            maxLod = minLod = 0;
            mipmapMode = vk::SamplerMipmapMode::eNearest;
            addressModeU = addressModeV = addressModeW =
                vk::SamplerAddressMode::eClampToBorder;
        }

        return *this;
    }
};

} // namespace lava
