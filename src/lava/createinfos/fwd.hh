#pragma once

namespace lava
{
class DescriptorPoolCreateInfo;
class DescriptorSetLayoutCreateInfo;
class RenderPassCreateInfo;
class GraphicsPipelineCreateInfo;
class FramebufferCreateInfo;
class RenderPassBeginInfo;
struct BufferCreateInfo;
struct ImageCreateInfo;
struct SamplerCreateInfo;
struct PipelineShaderStageCreateInfo;
} // namespace lava
