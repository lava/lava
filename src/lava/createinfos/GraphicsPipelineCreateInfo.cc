#include "GraphicsPipelineCreateInfo.hh"
#include <algorithm>
#include <lava/createinfos/PipelineShaderStageCreateInfo.hh>
#include <lava/objects/PipelineLayout.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/objects/ShaderModule.hh>

namespace lava
{

GraphicsPipelineCreateInfo::GraphicsPipelineCreateInfo() {}

GraphicsPipelineCreateInfo
GraphicsPipelineCreateInfo::defaults()
{
    GraphicsPipelineCreateInfo info;
    info.inputAssemblyState.primitiveRestartEnable = false;
    info.inputAssemblyState.topology = vk::PrimitiveTopology::eTriangleList;

    info.viewportState.addViewport(
        {0, 0, float(INT32_MAX), float(INT32_MAX), 0.f, 1.f});
    info.viewportState.addScissor({{0, 0}, {INT32_MAX, INT32_MAX}});

    info.rasterizationState.cullMode = vk::CullModeFlagBits::eNone;
    info.rasterizationState.frontFace = vk::FrontFace::eCounterClockwise;
    info.rasterizationState.lineWidth = 1.0f;
    info.rasterizationState.polygonMode = vk::PolygonMode::eFill;
    info.depthStencilState.depthCompareOp = vk::CompareOp::eGreater;

    info.multisampleState.rasterizationSamples =
        vk::SampleCountFlagBits::e1;
    info.colorBlendState.addNoBlend();

    info.dynamicState.addState(vk::DynamicState::eViewport);
    return info;
}

void
GraphicsPipelineCreateInfo::deriveFrom(vk::Pipeline other)
{
    mInfo.flags |= vk::PipelineCreateFlagBits::eDerivative;
    mInfo.basePipelineHandle = other;
}

void
GraphicsPipelineCreateInfo::allowDerivatives(bool val)
{
    if (val) {
        mInfo.flags |= vk::PipelineCreateFlagBits::eAllowDerivatives;
    } else {
        mInfo.flags &= ~vk::PipelineCreateFlagBits::eAllowDerivatives;
    }
}

void
GraphicsPipelineCreateInfo::disableOptimization(bool val)
{
    if (val) {
        mInfo.flags |= vk::PipelineCreateFlagBits::eDisableOptimization;
    } else {
        mInfo.flags &= ~vk::PipelineCreateFlagBits::eDisableOptimization;
    }
}

void
GraphicsPipelineCreateInfo::setLayout(const SharedPipelineLayout &layout)
{
    mInfo.layout = layout->handle();
    mLayout = layout;
}

void
GraphicsPipelineCreateInfo::addStage(const SharedShaderModule &module,
                                     std::string const &name,
                                     vk::ShaderStageFlagBits stage)
{
    mStages.emplace_back();
    auto &info = mStages.back();

    info.setModule(module);
    info.setName(name);
    if (stage == vk::ShaderStageFlagBits::eAll) {
        assert(
            module->stage() != vk::ShaderStageFlagBits::eAll &&
            "Couldn't find out the type of stage you want to add. Either "
            "load from a file with a hint for the stage in the name, or "
            "explicitly provide the stage either to the ShaderModule or "
            "when you add the stage.");
        stage = module->stage();
    }
    info.setStage(stage);
}

void
GraphicsPipelineCreateInfo::replaceStage(size_t index,
                                         const SharedShaderModule &module)
{
    if (index >= mStages.size())
        throw std::out_of_range("No stage with this index!");

    mStages[index].setModule(module);
}

PipelineShaderStageCreateInfo &
GraphicsPipelineCreateInfo::stage(vk::ShaderStageFlagBits stage)
{
    auto it = find_if(begin(mStages), end(mStages),
                      [&](PipelineShaderStageCreateInfo const &info) {
                          return info.stage() == stage;
                      });
    assert(it != mStages.end() &&
           "You have to add the stage before accessing it with stage()");
    return *it;
}

PipelineShaderStageCreateInfo &
GraphicsPipelineCreateInfo::stage(uint32_t index)
{
    assert(index < mStages.size() &&
           "You have to add the stage before accessing it with stage()");
    return mStages[index];
}

lava::GraphicsPipelineCreateInfo::operator vk::GraphicsPipelineCreateInfo()
{
    mVkStages.resize(mStages.size());
    copy(begin(mStages), end(mStages), begin(mVkStages));

    return mInfo.setStageCount(uint32_t(mVkStages.size()))
        .setPStages(mVkStages.data())
        .setPVertexInputState(vertexInputState.get())
        .setPInputAssemblyState(&inputAssemblyState)
        .setPTessellationState(tesselationState.patchControlPoints
                                   ? &tesselationState
                                   : nullptr)
        .setPViewportState(viewportState)
        .setPRasterizationState(&rasterizationState)
        .setPMultisampleState(&multisampleState)
        .setPDepthStencilState(&depthStencilState)
        .setPColorBlendState(colorBlendState)
        .setPDynamicState(
            dynamicState.empty()
                ? nullptr
                : static_cast<vk::PipelineDynamicStateCreateInfo *>(
                      dynamicState));
}

PipelineDynamicStateCreateInfo::PipelineDynamicStateCreateInfo(
    vk::PipelineDynamicStateCreateInfo info)
{
    for (size_t i = 0; i < info.dynamicStateCount; i++) {
        mStates.push_back(info.pDynamicStates[i]);
    }
}

void
PipelineDynamicStateCreateInfo::addState(vk::DynamicState state)
{
    mStates.push_back(state);
}

void
PipelineDynamicStateCreateInfo::removeState(vk::DynamicState state)
{
    auto it = std::remove(begin(mStates), end(mStates), state);
    mStates.erase(it, end(mStates));
}

PipelineColorBlendStateCreateInfo &
PipelineColorBlendStateCreateInfo::add(
    vk::PipelineColorBlendAttachmentState state)
{
    mAttachments.push_back(state);
    return *this;
}

PipelineColorBlendStateCreateInfo &
PipelineColorBlendStateCreateInfo::addNoBlend()
{
    vk::PipelineColorBlendAttachmentState result;
    result.colorWriteMask =
        vk::ColorComponentFlagBits::eA | vk::ColorComponentFlagBits::eR |
        vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB;
    add(result);
    return *this;
}

PipelineColorBlendStateCreateInfo &
PipelineColorBlendStateCreateInfo::addTransparencyBlend()
{
    add(vk::PipelineColorBlendAttachmentState()
            .setBlendEnable(true)                                       //
            .setColorWriteMask(vk::ColorComponentFlagBits::eA |         //
                               vk::ColorComponentFlagBits::eR |         //
                               vk::ColorComponentFlagBits::eG |         //
                               vk::ColorComponentFlagBits::eB)          //
            .setAlphaBlendOp(vk::BlendOp::eAdd)                         //
            .setSrcAlphaBlendFactor(vk::BlendFactor::eOne)              //
            .setDstAlphaBlendFactor(vk::BlendFactor::eOneMinusSrcAlpha) //
            .setColorBlendOp(vk::BlendOp::eAdd)                         //
            .setSrcColorBlendFactor(vk::BlendFactor::eSrcAlpha)         //
            .setDstColorBlendFactor(vk::BlendFactor::eOneMinusSrcAlpha) //
    );
    return *this;
}

PipelineColorBlendStateCreateInfo::
operator vk::PipelineColorBlendStateCreateInfo *()
{
    return &(mInfo.setAttachmentCount(uint32_t(mAttachments.size()))
                 .setPAttachments(mAttachments.data()));
}
} // namespace lava
