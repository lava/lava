#pragma once
#include <lava/common/FormatInfo.hh>
#include <lava/common/vulkan.hh>

namespace lava
{

class PipelineVertexInputStateCreateInfo
{
  public:
    /// Add the pointed to member as vertex attribute.
    template <class VertexT, class DataT>
    void addAttribute(DataT VertexT::*member, unsigned int location,
                      unsigned int binding = 0)
    {
        auto offset = reinterpret_cast<long>(
            &(reinterpret_cast<VertexT const volatile *>(0)->*member));

        /* Fixing "undefined reference" on vkTypeOf here:
         * - if DataT is something from glm, change your CMake to provide
         *   glm_static as a target before you add lava
         * - otherwise look at lava/common/FormatInfo.hh
         */
        addAttribute<VertexT>(
            {location, binding, vkTypeOf<DataT>::format, (uint32_t)offset});
    }

    template <class VertexT>
    void addAttribute(vk::VertexInputAttributeDescription const &attribute)
    {
        mAttributes.push_back(attribute);

        auto sameBinding = [&](vk::VertexInputBindingDescription const &a) {
            return a.binding == attribute.binding;
        };

        auto it =
            std::find_if(begin(mBindings), end(mBindings), sameBinding);
        if (it == end(mBindings)) {
            mBindings.push_back({attribute.binding, sizeof(VertexT)});
        }
    }

    void setRate(unsigned int binding, vk::VertexInputRate rate)
    {
        auto sameBinding = [&](vk::VertexInputBindingDescription const &a) {
            return a.binding == binding;
        };

        auto it =
            std::find_if(begin(mBindings), end(mBindings), sameBinding);
        if (it != end(mBindings)) {
            it->setInputRate(rate);
        }
    }

    void binding(uint32_t) {} // Recursion endpoint only

    template <class VertexT, class DataT, class... Rest>
    void binding(uint32_t binding_id, DataT VertexT::*member, Rest... rest)
    {
        addAttribute(member,
                     mAttributes.empty() ? 0
                                         : mAttributes.back().location + 1,
                     binding_id);
        binding(binding_id, rest...);
    }

    template <class VertexT, class DataT, class... Rest>
    void binding(uint32_t binding_id, vk::VertexInputRate rate,
                 DataT VertexT::*member, Rest... rest)
    {
        binding(binding_id, member, rest...);
        setRate(binding_id, rate);
    }

    vk::PipelineVertexInputStateCreateInfo *get();

  protected:
    std::vector<vk::VertexInputAttributeDescription> mAttributes;
    std::vector<vk::VertexInputBindingDescription> mBindings;
    vk::PipelineVertexInputStateCreateInfo mInfo;
};
} // namespace lava
