#include "RayTracingPipelineCreateInfo.hh"
#include <lava/common/log.hh>
#include <lava/objects/PipelineLayout.hh>
#include <lava/objects/RayTracingPipeline.hh>
#include <lava/objects/ShaderModule.hh>

namespace lava
{
RayTracingPipelineCreateInfo::RayTracingPipelineCreateInfo(
    const SharedPipelineLayout &layout)
    : mLayout(layout)
{

    mInfo.setLayout(mLayout->handle());
}

SharedRayTracingPipeline
RayTracingPipelineCreateInfo::create() const
{
    return RayTracingPipeline::create(*this);
}

RayTracingPipelineCreateInfo::Self &
RayTracingPipelineCreateInfo::addRayGeneration(
    const SharedShaderModule &rgen)
{

    auto idx = insertModule(rgen);
    mGroups.emplace(begin(mGroups) + mNumRaygens)
        ->setType(vk::RayTracingShaderGroupTypeNV::eGeneral)
        .setGeneralShader(idx)
        .setAnyHitShader(VK_SHADER_UNUSED_NV)
        .setClosestHitShader(VK_SHADER_UNUSED_NV)
        .setIntersectionShader(VK_SHADER_UNUSED_NV);
    mNumRaygens++;

    return *this;
}

RayTracingPipelineCreateInfo::Self &
RayTracingPipelineCreateInfo::addMiss(const SharedShaderModule &rmiss)
{

    auto idx = insertModule(rmiss);
    mGroups.emplace(end(mGroups))
        ->setType(vk::RayTracingShaderGroupTypeNV::eGeneral)
        .setGeneralShader(idx)
        .setAnyHitShader(VK_SHADER_UNUSED_NV)
        .setClosestHitShader(VK_SHADER_UNUSED_NV)
        .setIntersectionShader(VK_SHADER_UNUSED_NV);

    return *this;
}

RayTracingPipelineCreateInfo::Self &
RayTracingPipelineCreateInfo::addTriangleHitGroup(
    const SharedShaderModule &closestHit, const SharedShaderModule &anyHit)
{
    assert((closestHit || anyHit) &&
           "At least one hit shader is needed per hit group");

    mGroups.emplace(begin(mGroups) + mNumRaygens + mNumHitGroups)
        ->setType(vk::RayTracingShaderGroupTypeNV::eTrianglesHitGroup)
        .setGeneralShader(VK_SHADER_UNUSED_NV)
        .setClosestHitShader(closestHit ? insertModule(closestHit)
                                        : VK_SHADER_UNUSED_NV)
        .setAnyHitShader(anyHit ? insertModule(anyHit) //
                                : VK_SHADER_UNUSED_NV)
        .setIntersectionShader(VK_SHADER_UNUSED_NV);
    mNumHitGroups++;

    return *this;
}

RayTracingPipelineCreateInfo::Self &
RayTracingPipelineCreateInfo::addProceduralHitGroup(
    const SharedShaderModule &intersection,
    const SharedShaderModule &closestHit, const SharedShaderModule &anyHit)
{
    assert((closestHit || anyHit) &&
           "At least one hit shader is needed per hit group");
    assert(
        intersection &&
        "Cannot use procedural hit groups without an intersection shader");

    mGroups.emplace(begin(mGroups) + mNumRaygens + mNumHitGroups)
        ->setType(vk::RayTracingShaderGroupTypeNV::eProceduralHitGroup)
        .setGeneralShader(VK_SHADER_UNUSED_NV)
        .setIntersectionShader(insertModule(intersection))
        .setClosestHitShader(closestHit ? insertModule(closestHit)
                                        : VK_SHADER_UNUSED_NV)
        .setAnyHitShader(anyHit ? insertModule(anyHit) //
                                : VK_SHADER_UNUSED_NV);
    mNumHitGroups++;

    return *this;
}

uint32_t
RayTracingPipelineCreateInfo::insertModule(const SharedShaderModule &module)
{
    auto it = std::find(begin(mModules), end(mModules), module);
    if (it == end(mModules)) {
        mModules.push_back(module);
        it = end(mModules) - 1;

        auto &info = (mStages.emplace_back(), mStages.back());
        info.setName("main");
        info.setStage(module->stage());
        info.setModule(module);
    }
    return std::distance(begin(mModules), it);
}

const vk::RayTracingPipelineCreateInfoNV &
RayTracingPipelineCreateInfo::info()
{
    mStagesData.resize(mStages.size());
    std::copy(begin(mStages), end(mStages), begin(mStagesData));

    return mInfo.setPGroups(mGroups.data())
        .setGroupCount(mGroups.size())
        .setPStages(mStagesData.data())
        .setStageCount(mStagesData.size());
}

} // namespace lava
