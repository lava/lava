#pragma once
#include "fwd.hh"
#include <lava/common/GlLikeFormats.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <lava/objects/Buffer.hh>
#include <set>

namespace lava
{

struct BottomLevelAccelerationStructureCreateInfo {
    using Self = BottomLevelAccelerationStructureCreateInfo;

    /// Expects vbuffer to be a densely packed buffer of 3 floats per vertex
    /// and uses he whole of vbuffer and ibuffer for the geometry
    Self &addTriangleGeometry(SharedBuffer const &vbuffer,
                              SharedBuffer const &ibuffer,
                              vk::IndexType itype = vk::IndexType::eUint32);

    /// Adds a triangle geometry with the given buffers, offsets, counts,
    /// strides. vbuffer == ibuffer is allowed.
    Self &addTriangleGeometry(SharedBuffer const &vbuffer, uint32_t voffset,
                              uint32_t vcount, uint32_t vstride,
                              GenericFormat vformat,
                              SharedBuffer const &ibuffer, uint32_t ioffset,
                              uint32_t icount, vk::IndexType itype);

    // TODO
    Self &addAabbGeometry(SharedBuffer const &aabbbuffer, uint32_t count,
                          uint32_t offset = 0,
                          uint32_t stride = 6 * sizeof(float));

    SharedBottomLevelAccelerationStructure create(SharedDevice const &on);

    vk::AccelerationStructureCreateInfoNV info() const;
    int geometryCount() const { return mGeometries.size(); }

  protected:
    std::set<SharedBuffer> mBuffers;
    std::vector<vk::GeometryNV> mGeometries;
    uint32_t addTriangleGeometry(void *vertices, size_t offset,
                                 vk::Format format);
};

} // namespace lava
