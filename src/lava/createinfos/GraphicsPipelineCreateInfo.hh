#pragma once
#include "PipelineVertexInputStateCreateInfo.hh"
#include <lava/common/FormatInfo.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/PipelineShaderStageCreateInfo.hh>
#include <lava/createinfos/Scissor.hh>
#include <lava/createinfos/Viewport.hh>
#include <lava/createinfos/fwd.hh>
#include <lava/fwd.hh>

namespace lava
{

class PipelineColorBlendStateCreateInfo
{
  public:
    /// Remove the blend states for all attachments
    PipelineColorBlendStateCreateInfo &clear()
    {
        mAttachments.clear();
        return *this;
    }
    PipelineColorBlendStateCreateInfo &
    add(vk::PipelineColorBlendAttachmentState state);

    /// Add a blend attachment that writes to color without blending
    PipelineColorBlendStateCreateInfo &addNoBlend();

    /// Add a blend attachment that does regular transparency blending
    PipelineColorBlendStateCreateInfo &addTransparencyBlend();

    operator vk::PipelineColorBlendStateCreateInfo *();

  protected:
    vk::PipelineColorBlendStateCreateInfo mInfo;
    std::vector<vk::PipelineColorBlendAttachmentState> mAttachments;
};

class PipelineDynamicStateCreateInfo
{
  public:
    PipelineDynamicStateCreateInfo() = default;
    PipelineDynamicStateCreateInfo(vk::PipelineDynamicStateCreateInfo info);

    void addState(vk::DynamicState state);
    void removeState(vk::DynamicState state);
    bool empty() const { return mStates.empty(); }

    operator vk::PipelineDynamicStateCreateInfo *()
    {
        return &(mInfo.setDynamicStateCount(uint32_t(mStates.size()))
                     .setPDynamicStates(mStates.data()));
    }

  protected:
    vk::PipelineDynamicStateCreateInfo mInfo;
    std::vector<vk::DynamicState> mStates;
};

class PipelineViewportStateCreateInfo
{
  public:
    PipelineViewportStateCreateInfo() = default;
    PipelineViewportStateCreateInfo(vk::PipelineViewportStateCreateInfo o)
    {
        mInfo = o;
        for (size_t i = 0; i < mInfo.viewportCount; i++)
            mViewports.push_back(mInfo.pViewports[i]);
        for (size_t i = 0; i < mInfo.scissorCount; i++)
            mScissors.push_back(mInfo.pScissors[i]);
    }

    operator vk::PipelineViewportStateCreateInfo *()
    {
        return &(mInfo.setScissorCount(uint32_t(mScissors.size()))
                     .setPScissors(mScissors.data())
                     .setViewportCount(uint32_t(mViewports.size()))
                     .setPViewports(mViewports.data()));
    }

    void clear()
    {
        mViewports.clear();
        mScissors.clear();
    }

    void addViewport(Viewport vp) { mViewports.push_back(vp); }
    void addScissor(Scissor sc) { mScissors.push_back(sc); }

    vk::Rect2D &scissor(size_t i) { return mScissors.at(i); }
    vk::Viewport &viewport(size_t i) { return mViewports.at(i); }

  protected:
    vk::PipelineViewportStateCreateInfo mInfo;
    std::vector<vk::Viewport> mViewports;
    std::vector<vk::Rect2D> mScissors;
};

class GraphicsPipelineCreateInfo
{
  public:
    GraphicsPipelineCreateInfo();

    /**
     * @brief sets up CreateInfos for a graphics pipeline roughly like the
     * default state of OpenGL, with the exception that it assumes a
     * reverse-depth Z-Buffer.
     * (https://developer.nvidia.com/content/depth-precision-visualized)
     *
     * You'll have to add your own shaders and set the PipelineLayout
     * Depth-test/-write is disabled by default (as it is in OpenGL)
     * The viewport is a dynamic parameter, i.e. has to be set in the
     * command buffer
     */
    static GraphicsPipelineCreateInfo defaults();

    void deriveFrom(vk::Pipeline other);
    void allowDerivatives(bool val = true);
    void disableOptimization(bool val = true);

    void setLayout(SharedPipelineLayout const &layout);

    /// SPIR-V modules can contain multiple functions for different stages.
    /// If you compile from GLSL, however, that won't affect you for now.
    void
    addStage(SharedShaderModule const &module,
             const std::string &name = "main",
             vk::ShaderStageFlagBits stage = vk::ShaderStageFlagBits::eAll);

    /// assumes that the entry point name and StageFlags stay the same.
    void replaceStage(size_t index, SharedShaderModule const &module);

    PipelineShaderStageCreateInfo &stage(vk::ShaderStageFlagBits stage);
    PipelineShaderStageCreateInfo &stage(uint32_t index);
    operator vk::GraphicsPipelineCreateInfo();

    PipelineVertexInputStateCreateInfo vertexInputState;
    vk::PipelineInputAssemblyStateCreateInfo inputAssemblyState;
    vk::PipelineTessellationStateCreateInfo tesselationState;
    PipelineViewportStateCreateInfo viewportState;
    vk::PipelineRasterizationStateCreateInfo rasterizationState;
    vk::PipelineMultisampleStateCreateInfo multisampleState;
    vk::PipelineDepthStencilStateCreateInfo depthStencilState;
    PipelineColorBlendStateCreateInfo colorBlendState;
    PipelineDynamicStateCreateInfo dynamicState;

    size_t numStages() const { return mStages.size(); }

  protected:
    bool mUseTesselation = false;
    vk::GraphicsPipelineCreateInfo mInfo;
    std::vector<PipelineShaderStageCreateInfo> mStages;
    std::vector<vk::PipelineShaderStageCreateInfo> mVkStages;
    SharedPipelineLayout mLayout;
    SharedRenderPass mPass;

    friend class GraphicsPipeline;
    friend class RenderPass;
};
} // namespace lava
