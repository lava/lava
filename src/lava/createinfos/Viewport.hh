#pragma once
#ifdef  LAVA_GLM_AVAILABLE
#include <glm/vec2.hpp>
#endif //  LAVA_GLM_AVAILABLE
#include <lava/common/vulkan.hh>

namespace lava
{

/** In contrast to OpenGL, the viewport in Vulkan has a third dimension for
 * the depth values that the range [0,1] is scaled to. In almost all cases,
 * you'll want to leave those values at [0,1]. Vulkan.hpp however
 * initializes both values to 0.f, which breaks the depth test if not
 * explicitly overwritten.
 *
 *  This class exists to get a sensible default instead.
 */
class Viewport : public vk::Viewport
{
  public:
    Viewport(float x_, float y_, float width_, float height_,
             float minDepth_ = 0.f, float maxDepth_ = 1.f)
        : vk::Viewport(x_, y_, width_, height_, minDepth_, maxDepth_)
    {
    }
    Viewport(vk::Viewport const &viewport) : vk::Viewport(viewport) {}
#ifdef LAVA_GLM_AVAILABLE
    Viewport(glm::uvec2 size)
        : vk::Viewport(0, 0, float(size.x), float(size.y), 0.f, 1.f)
    {
    }
#endif //  LAVA_GLM_AVAILABLE
    Viewport(float width, float height)
        : vk::Viewport(0, 0, width, height, 0.f, 1.f)
    {
    }
};

static_assert(sizeof(Viewport) == sizeof(vk::Viewport),
              "Wrapper has wrong size!");
static_assert(alignof(Viewport) == alignof(vk::Viewport),
              "Wrapper has wrong alignment!");
} // namespace lava
