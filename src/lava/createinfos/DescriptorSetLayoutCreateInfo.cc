#include "DescriptorSetLayoutCreateInfo.hh"
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>

namespace lava
{

SharedDescriptorSetLayout
DescriptorSetLayoutCreateInfo::create(const SharedDevice &on,
                                      uint32_t poolSize) const
{
    return std::make_shared<DescriptorSetLayout>(on, *this, poolSize);
}

} // namespace lava
