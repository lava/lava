#include "DescriptorPoolCreateInfo.hh"
#include <lava/objects/DescriptorPool.hh>

namespace lava
{

DescriptorPoolCreateInfo::DescriptorPoolCreateInfo(
    const vk::DescriptorPoolCreateInfo &from)
{
    mInfo = from;

    for (size_t i = 0; i < mInfo.poolSizeCount; i++) {
        mSizes.push_back(mInfo.pPoolSizes[i]);
    }
}

DescriptorPoolCreateInfo::Self &
DescriptorPoolCreateInfo::setFlags(vk::DescriptorPoolCreateFlags flags)
{
    mInfo.flags = flags;
    return *this;
}

DescriptorPoolCreateInfo::Self &
DescriptorPoolCreateInfo::allowFreeing(bool val)
{
    if (val) {
        mInfo.flags |= vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
    } else {
        mInfo.flags &=
            ~vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
    }
    return *this;
}

DescriptorPoolCreateInfo::Self &
DescriptorPoolCreateInfo::setMaxSets(uint32_t count)
{
    mInfo.maxSets = count;
    return *this;
}

SharedDescriptorPool
DescriptorPoolCreateInfo::create(const SharedDevice &device)
{
    return std::make_shared<DescriptorPool>(device, *this);
}

lava::DescriptorPoolCreateInfo::operator vk::DescriptorPoolCreateInfo()
{
    return mInfo.setPoolSizeCount(uint32_t(mSizes.size()))
        .setPPoolSizes(mSizes.data());
}

} // namespace lava
