#pragma once
#include <lava/common/optional.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class RenderPassBeginInfo
{
  public:
    RenderPassBeginInfo() = default;

    void setFramebuffer(SharedFramebuffer const &fb);

    void setRenderPass(SharedRenderPass const &pass) { mPass = pass; }

    void setRenderArea(vk::Rect2D ra) { mInfo.renderArea = ra; }

    /// Set clear values, copies the vector over into the
    /// RenderPassBeginInfo
    void setClearValues(std::vector<vk::ClearValue> const &values);

    /// Set clear values, but keep the storage of the list external.
    /// You need to make sure that the range of values stays valid yourself.
    void setClearValues(vk::ClearValue const *first, uint32_t count);

    SharedFramebuffer const &framebuffer() const { return mFramebuffer; }

    operator vk::RenderPassBeginInfo &();

  protected:
    vk::RenderPassBeginInfo mInfo;
    lava::optional<std::vector<vk::ClearValue>> mClearValues;
    SharedRenderPass mPass;
    SharedFramebuffer mFramebuffer;
};
} // namespace lava
