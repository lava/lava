#pragma once

#include <lava/common/GlLikeFormats.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

struct ImageCreateInfo {
    ImageCreateInfo();
    vk::ImageCreateInfo imageInfo;
    vk::ImageViewType viewType;

    SharedImage create(SharedDevice const &device) const;

    ImageCreateInfo &setFormat(GenericFormat format);
    ImageCreateInfo &setSize(uint32_t width, uint32_t height = 1,
                             uint32_t depth = 1);
    ImageCreateInfo &setArrayLayers(uint32_t layers);
    ImageCreateInfo &setMipLevels(uint32_t levels);
    ImageCreateInfo &setCombinedType(vk::ImageViewType type);

    ImageCreateInfo &setUsage(vk::ImageUsageFlags usage);
    ImageCreateInfo &addUsage(vk::ImageUsageFlags usage);

    ImageCreateInfo &setTiling(vk::ImageTiling tiling);

    operator vk::ImageCreateInfo &() { return imageInfo; }
    operator vk::ImageCreateInfo const &() const { return imageInfo; }
};

/// CreateInfo for a 2D image used for texturing.
/// Determine number of mipmap levels automatically
ImageCreateInfo texture2D(uint32_t width, uint32_t height,
                          GenericFormat format);
/// CreateInfo for a 2D array image used for texturing.
/// Determine number of mipmap levels automatically
ImageCreateInfo texture2DArray(uint32_t width, uint32_t height,
                               uint32_t layers, GenericFormat format);

/// CreateInfo for a 2D image used as color/depth attachment.
ImageCreateInfo attachment2D(uint32_t width, uint32_t height,
                             GenericFormat format);
/// CreateInfo for a 2D array image used as color/depth attachment.
ImageCreateInfo attachment2DArray(uint32_t width, uint32_t height,
                                  uint32_t layers, GenericFormat format);

/// CreateInfo for a 2D image used as `image2D` in shader (shader storage
/// image)
ImageCreateInfo storageImage2D(uint32_t width, uint32_t height,
                               GenericFormat format);
/// CreateInfo for a 2D Array image used as `image2DArray` in shader (shader
/// storage image)
ImageCreateInfo storageImage2DArray(uint32_t width, uint32_t height,
                                    uint32_t layers, GenericFormat format);

} // namespace lava
