#include "RenderPassBeginInfo.hh"
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/RenderPass.hh>

namespace lava
{

lava::RenderPassBeginInfo::operator vk::RenderPassBeginInfo &()
{
    if (mClearValues) {
        return mInfo.setClearValueCount(uint32_t(mClearValues->size()))
            .setPClearValues(mClearValues->data())
            .setRenderPass(mPass->handle());
    } else {
        return mInfo.setRenderPass(mPass->handle());
    }
}

void
RenderPassBeginInfo::setFramebuffer(const SharedFramebuffer &fb)
{
    mFramebuffer = fb;
    mInfo.framebuffer = fb->handle();
}

void
RenderPassBeginInfo::setClearValues(
    const std::vector<vk::ClearValue> &values)
{
    mClearValues = values;
}

void
RenderPassBeginInfo::setClearValues(const vk::ClearValue *first,
                                    uint32_t count)
{
    mClearValues = lava::nullopt;
    mInfo.pClearValues = first;
    mInfo.clearValueCount = count;
}

} // namespace lava
