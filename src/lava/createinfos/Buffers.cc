#include "Buffers.hh"
#include <lava/objects/Buffer.hh>

namespace lava
{

SharedBuffer
BufferCreateInfo::create(const SharedDevice &device) const
{
    return std::make_shared<Buffer>(device, *this);
}

} // namespace lava
