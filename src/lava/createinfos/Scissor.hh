#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{
/**
 *  The Extend of a Rect2D uses unsigned integers as the dimensions, but the
 *  (nvidia) driver interprets them as signed integers in the context of the
 *  scissor test.
 *  Setting the scissor to {{0,0},{UINT32_MAX,UINT32_MAX}} with the
 * intention to make all fragments pass the test will thus cause no
 * fragments to pass.
 *
 *  This class exists to avoid that trap.
 */
class Scissor : public vk::Rect2D
{
  public:
    Scissor(vk::Rect2D const &rect) : vk::Rect2D(rect) {}
    Scissor(int32_t x, int32_t y, int32_t width, int32_t height)
        : vk::Rect2D({x, y}, {uint32_t(width), uint32_t(height)})
    {
    }
    Scissor(vk::Offset2D offset, vk::Extent2D extent)
        : vk::Rect2D(offset, extent)
    {
        assert(extent.width <= INT32_MAX && extent.height <= INT32_MAX &&
               "A scissor with a size greater than INT32_MAX discards all "
               "fragments.");
    }
};

static_assert(sizeof(Scissor) == sizeof(vk::Rect2D),
              "Wrapper has wrong size!");
static_assert(alignof(Scissor) == alignof(vk::Rect2D),
              "Wrapper has wrong alignment!");
} // namespace lava
