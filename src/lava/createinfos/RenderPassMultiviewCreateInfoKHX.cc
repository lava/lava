#include "RenderPassMultiviewCreateInfoKHX.hh"
#include <bitset>
namespace lava
{

RenderPassMultiviewCreateInfoKHX::RenderPassMultiviewCreateInfoKHX(
    const RenderPassCreateInfo &pass, uint32_t num_layers)
{
    auto &&subpasses = pass.getSubpasses();
    for (size_t i = 0; i < subpasses.size(); i++) {
        std::bitset<32> set{0};

        // Vulkan Doc is a bit unclear here...
        /*if (sub.depthAttachment != vk::AttachmentReference{}) {
            set[sub.depthAttachment.attachment] = true;
        }
        for (auto &&att : sub.colorAttachments) {
            set[att.attachment] = true;
        }*/
        for (size_t j = 0; j < num_layers; j++)
            set[j] = true;
        mViewMask.push_back(uint32_t(set.to_ulong()));
    }
    mInfo.subpassCount = subpasses.size();
}

vk::StructureType
RenderPassMultiviewCreateInfoKHX::type() const
{
    return vk::StructureType::eRenderPassMultiviewCreateInfoKHR;
}

const void *
RenderPassMultiviewCreateInfoKHX::get()
{
    mInfo.pNext = mNext ? mNext->get() : nullptr;

    mInfo.subpassCount = mViewMask.size();
    mInfo.pViewMasks = mViewMask.empty() ? nullptr : mViewMask.data();

    mInfo.dependencyCount = mViewOffsets.size();
    mInfo.pViewOffsets =
        mViewOffsets.empty() ? nullptr : mViewOffsets.data();

    mInfo.correlationMaskCount = mCorrelationMasks.size();
    mInfo.pCorrelationMasks =
        mCorrelationMasks.empty() ? nullptr : mCorrelationMasks.data();

    return &mInfo;
}
} // namespace lava
