#include "PipelineLayoutCreateInfo.hh"

namespace lava
{

PipelineLayoutCreateInfo::PipelineLayoutCreateInfo(
    const vk::PipelineLayoutCreateInfo &info)
{
    mInfo = info;
    for (size_t i = 0; i < mInfo.pushConstantRangeCount; i++) {
        mRanges.push_back(mInfo.pPushConstantRanges[i]);
    }
    for (size_t i = 0; i < mInfo.setLayoutCount; i++) {
        mLayouts.push_back(mInfo.pSetLayouts[i]);
    }
}

PipelineLayoutCreateInfo::PipelineLayoutCreateInfo(
    std::vector<vk::DescriptorSetLayout> const &layouts,
    std::vector<vk::PushConstantRange> const &ranges)
    : mLayouts(layouts), mRanges(ranges)
{
}

PipelineLayoutCreateInfo::operator vk::PipelineLayoutCreateInfo()
{
    return mInfo.setPushConstantRangeCount(uint32_t(mRanges.size()))
        .setPPushConstantRanges(mRanges.data())
        .setSetLayoutCount(uint32_t(mLayouts.size()))
        .setPSetLayouts(mLayouts.data());
}
} // namespace lava
