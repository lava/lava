#include "Images.hh"
#include <lava/common/FormatInfo.hh>
#include <lava/objects/Image.hh>

namespace lava
{

static int
mipLevels(int dim)
{
    uint32_t count = 1;
    while (dim >>= 1)
        ++count;
    return count;
}

ImageCreateInfo::ImageCreateInfo()
{
    imageInfo.setInitialLayout(vk::ImageLayout::eUndefined)
        .setMipLevels(1)
        .setArrayLayers(1)
        .setSamples(vk::SampleCountFlagBits::e1)
        .setSharingMode(vk::SharingMode::eExclusive)
        .setTiling(vk::ImageTiling::eOptimal)
        .setUsage(vk::ImageUsageFlagBits::eTransferDst |
                  vk::ImageUsageFlagBits::eTransferSrc);
}

SharedImage
ImageCreateInfo::create(const SharedDevice &device) const
{
    return std::make_shared<Image>(device, imageInfo, viewType);
}

ImageCreateInfo
texture2D(uint32_t width, uint32_t height, GenericFormat format)
{
    return ImageCreateInfo()
        .setCombinedType(vk::ImageViewType::e2D)
        .setSize(width, height)
        .setFormat(format)
        .setMipLevels(mipLevels(std::min(width, height)))
        .addUsage(vk::ImageUsageFlagBits::eSampled);
}

ImageCreateInfo
texture2DArray(uint32_t width, uint32_t height, uint32_t layers,
               GenericFormat format)
{
    return ImageCreateInfo()
        .setCombinedType(vk::ImageViewType::e2DArray)
        .setSize(width, height)
        .setFormat(format)
        .setMipLevels(mipLevels(std::min(width, height)))
        .setArrayLayers(layers)
        .addUsage(vk::ImageUsageFlagBits::eSampled);
}

ImageCreateInfo
storageImage2D(uint32_t width, uint32_t height, GenericFormat format)
{
    return ImageCreateInfo()
        .setCombinedType(vk::ImageViewType::e2D)
        .setFormat(format.vkhpp())
        .setSize(width, height)
        .addUsage(vk::ImageUsageFlagBits::eStorage);
}

ImageCreateInfo
storageImage2DArray(uint32_t width, uint32_t height, uint32_t layers,
                    GenericFormat format)
{
    return ImageCreateInfo()
        .setCombinedType(vk::ImageViewType::e2DArray)
        .setArrayLayers(layers)
        .setFormat(format.vkhpp())
        .setSize(width, height)
        .addUsage(vk::ImageUsageFlagBits::eStorage);
}

ImageCreateInfo
attachment2D(uint32_t width, uint32_t height, GenericFormat format)
{
    auto info = ImageCreateInfo()
                    .setCombinedType(vk::ImageViewType::e2D)
                    .setFormat(format.vkhpp())
                    .setSize(width, height)
                    .addUsage(vk::ImageUsageFlagBits::eSampled);

    auto aspects = aspectsOf(format.vkhpp());
    if (aspects & vk::ImageAspectFlagBits::eDepth ||
        aspects & vk::ImageAspectFlagBits::eStencil) {
        info.addUsage(vk::ImageUsageFlagBits::eDepthStencilAttachment);
    }

    if (aspects & vk::ImageAspectFlagBits::eColor) {
        info.addUsage(vk::ImageUsageFlagBits::eColorAttachment);
    }

    return info;
}

ImageCreateInfo
attachment2DArray(uint32_t width, uint32_t height, uint32_t layers,
                  GenericFormat format)
{
    auto info = ImageCreateInfo()
                    .setCombinedType(vk::ImageViewType::e2DArray)
                    .setFormat(format.vkhpp())
                    .setSize(width, height)
                    .setArrayLayers(layers)
                    .addUsage(vk::ImageUsageFlagBits::eSampled);

    auto aspects = aspectsOf(format.vkhpp());
    if (aspects & vk::ImageAspectFlagBits::eDepth ||
        aspects & vk::ImageAspectFlagBits::eStencil) {
        info.addUsage(vk::ImageUsageFlagBits::eDepthStencilAttachment);
    }

    if (aspects & vk::ImageAspectFlagBits::eColor) {
        info.addUsage(vk::ImageUsageFlagBits::eColorAttachment);
    }

    return info;
}

ImageCreateInfo &
ImageCreateInfo::setFormat(GenericFormat format)
{
    imageInfo.format = format.vkhpp();
    return *this;
}

ImageCreateInfo &
ImageCreateInfo::setSize(uint32_t width, uint32_t height, uint32_t depth)
{
    imageInfo.extent.setWidth(width).setHeight(height).setDepth(depth);
    return *this;
}

ImageCreateInfo &
ImageCreateInfo::setArrayLayers(uint32_t layers)
{
    imageInfo.setArrayLayers(layers);
    return *this;
}

ImageCreateInfo &
ImageCreateInfo::setMipLevels(uint32_t levels)
{
    imageInfo.setMipLevels(levels);
    return *this;
}

ImageCreateInfo &
ImageCreateInfo::setCombinedType(vk::ImageViewType type)
{
    viewType = type;

    switch (type) {
    case vk::ImageViewType::e1D:
    case vk::ImageViewType::e1DArray:
        imageInfo.setImageType(vk::ImageType::e1D);
        break;
    case vk::ImageViewType::eCubeArray:
    case vk::ImageViewType::eCube:
        imageInfo.flags |= vk::ImageCreateFlagBits::eCubeCompatible;
        imageInfo.setImageType(vk::ImageType::e2D);
        break;
    case vk::ImageViewType::e2D:
    case vk::ImageViewType::e2DArray:
        imageInfo.setImageType(vk::ImageType::e2D);
        break;
    case vk::ImageViewType::e3D:
        imageInfo.setImageType(vk::ImageType::e3D);
        break;
    }

    return *this;
}

ImageCreateInfo &
ImageCreateInfo::addUsage(vk::ImageUsageFlags usage)
{
    imageInfo.usage |= usage;
    return *this;
}

ImageCreateInfo &
ImageCreateInfo::setTiling(vk::ImageTiling tiling)
{
    imageInfo.tiling = tiling;
    return *this;
}

ImageCreateInfo &
ImageCreateInfo::setUsage(vk::ImageUsageFlags usage)
{
    imageInfo.usage = usage;
    return *this;
}

} // namespace lava
