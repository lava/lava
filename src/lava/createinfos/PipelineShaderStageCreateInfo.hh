#pragma once
#include <lava/common/optional.hh>
#include <lava/common/vulkan.hh>
#include <lava/objects/ShaderModule.hh>

namespace lava
{
struct PipelineShaderStageCreateInfo {
    template <class T>
    PipelineShaderStageCreateInfo &specialize(uint32_t constantId, T value)
    {
        auto it = find_if(begin(mSpecMap), end(mSpecMap),
                          [&](vk::SpecializationMapEntry const &entry) {
                              return entry.constantID == constantId;
                          });
        if (it == end(mSpecMap)) {
            mSpecMap.push_back(
                {constantId, uint32_t(mSpecData.size()), sizeof(T)});
            auto tptr = reinterpret_cast<char const *>(&value);
            std::copy(tptr, tptr + sizeof(T), back_inserter(mSpecData));
        } else {
            vk::SpecializationMapEntry &entry = *it;
            assert(sizeof(T) == entry.size &&
                   "Different specializations shouldn't have different "
                   "types.");
            auto tptr = reinterpret_cast<char const *>(&value);
            std::copy(tptr, tptr + sizeof(T),
                      mSpecData.data() + entry.offset);
        }
        mSpecInfo.dataSize = mSpecData.size();
        mSpecInfo.mapEntryCount = mSpecMap.size();
        mSpecInfo.pData = mSpecData.data();
        mSpecInfo.pMapEntries = mSpecMap.data();
        return *this;
    }

    operator vk::PipelineShaderStageCreateInfo &()
    {
        if (!mSpecData.empty())
            mInfo.pSpecializationInfo = &mSpecInfo;
        mSpecInfo.pData = mSpecData.data();
        mSpecInfo.dataSize = mSpecData.size();
        mSpecInfo.pMapEntries = mSpecMap.data();
        mSpecInfo.mapEntryCount = uint32_t(mSpecMap.size());
        mInfo.setPName(mName.c_str());
        return mInfo;
    }

    PipelineShaderStageCreateInfo &setStage(vk::ShaderStageFlagBits stage)
    {
        mInfo.setStage(stage);
        return *this;
    }

    PipelineShaderStageCreateInfo &setModule(SharedShaderModule module)
    {
        mModule = module;
        mInfo.setModule(module->handle());
        return *this;
    }

    PipelineShaderStageCreateInfo &setName(std::string const &name)
    {
        mName = name;
        return *this;
    }

    vk::ShaderStageFlagBits stage() const { return mInfo.stage; }

  private:
    std::string mName;
    vk::PipelineShaderStageCreateInfo mInfo;
    vk::SpecializationInfo mSpecInfo;
    std::vector<vk::SpecializationMapEntry> mSpecMap;
    std::vector<char> mSpecData;
    SharedShaderModule mModule;
};
} // namespace lava
