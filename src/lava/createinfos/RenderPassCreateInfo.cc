#include "RenderPassCreateInfo.hh"
#include <lava/common/FormatInfo.hh>
#include <lava/common/GlLikeFormats.hh>
#include <lava/objects/RenderPass.hh>

namespace lava
{

RenderPassCreateInfo::RenderPassCreateInfo() {}

RenderPassCreateInfo
RenderPassCreateInfo::simpleForward(GenericFormat colorFormat)
{
    RenderPassCreateInfo info;

    auto depth = AttachmentDescription::depth32float();
    depth.clear();
    depth.discard();
    info.addAttachment(depth);

    auto color = AttachmentDescription::color(colorFormat.vkhpp());
    color.clear();
    color.finalLayout(vk::ImageLayout::ePresentSrcKHR);
    info.addAttachment(color);

    info.addSubpass(info.fullSubpass());
    info.autoAddDependencies();

    return info;
}

SubpassDescription
RenderPassCreateInfo::fullSubpass()
{
    SubpassDescription sub;
    for (size_t i = 0; i < mAttachments.size(); i++) {
        vk::AttachmentReference att;
        att.attachment = uint32_t(i);

        if (aspectsOf(mAttachments[i].format()) &
            vk::ImageAspectFlagBits::eColor) {
            att.layout = vk::ImageLayout::eColorAttachmentOptimal;
            sub.colorAttachments.push_back(att);
        } else {
            att.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
            sub.depthAttachment = att;
        }
    }
    return sub;
}

void
RenderPassCreateInfo::autoAddDependencies()
{
    auto const n = mSubpasses.size();
    for (size_t i = 0; i <= n; i++) {
        vk::SubpassDependency dep;
        dep.srcSubpass = (i == 0) ? VK_SUBPASS_EXTERNAL : uint32_t(i - 1);
        dep.dstSubpass = (i == n) ? VK_SUBPASS_EXTERNAL : uint32_t(i);

        if (i > 0) {
            dep.srcSubpass = uint32_t(i - 1);
            dep.srcStageMask =
                vk::PipelineStageFlagBits::eColorAttachmentOutput;
            dep.srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
        } else {
            dep.srcSubpass = VK_SUBPASS_EXTERNAL;
            dep.srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
        }

        if (i < n) {
            dep.dstSubpass = uint32_t(i);
            dep.dstStageMask = vk::PipelineStageFlagBits::eFragmentShader;
            dep.dstAccessMask = vk::AccessFlagBits::eShaderRead;
        } else {
            dep.dstSubpass = VK_SUBPASS_EXTERNAL;
            dep.dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
        }
        dep.dependencyFlags = vk::DependencyFlagBits::eByRegion;

        mDependencies.push_back(dep);
    }
}

SharedRenderPass
RenderPassCreateInfo::create(SharedDevice const &device) const
{
    return std::make_shared<RenderPass>(device, *this);
}

lava::RenderPassCreateInfo::operator vk::RenderPassCreateInfo()
{
    mVulkanSubpasses.resize(mSubpasses.size());
    std::copy(begin(mSubpasses), end(mSubpasses), begin(mVulkanSubpasses));

    return mInfo.setDependencyCount(uint32_t(mDependencies.size()))
        .setPNext(mNext ? mNext->get() : nullptr)
        .setPDependencies(mDependencies.data())
        .setSubpassCount(uint32_t(mVulkanSubpasses.size()))
        .setPSubpasses(mVulkanSubpasses.data())
        .setAttachmentCount(uint32_t(mAttachments.size()))
        .setPAttachments(reinterpret_cast<vk::AttachmentDescription *>(
            mAttachments.data()));

    static_assert(sizeof(AttachmentDescription) ==
                      sizeof(vk::AttachmentDescription),
                  "AttachmentDescription between lava and vulkan have "
                  "different sizes.");
    static_assert(alignof(AttachmentDescription) ==
                      alignof(vk::AttachmentDescription),
                  "AttachmentDescription between lava and vulkan have "
                  "different aligns.");
}

AttachmentDescription
AttachmentDescription::depth24Stencil8()
{
    AttachmentDescription result;
    result.mInfo.format = vk::Format::eD24UnormS8Uint;
    result.mInfo.samples = vk::SampleCountFlagBits::e1;
    result.mInfo.loadOp = vk::AttachmentLoadOp::eLoad;
    result.mInfo.storeOp = vk::AttachmentStoreOp::eStore;
    result.mInfo.stencilLoadOp = vk::AttachmentLoadOp::eLoad;
    result.mInfo.stencilStoreOp = vk::AttachmentStoreOp::eStore;
    result.mInfo.initialLayout =
        vk::ImageLayout::eDepthStencilAttachmentOptimal;
    result.mInfo.finalLayout =
        vk::ImageLayout::eDepthStencilAttachmentOptimal;
    return result;
}

AttachmentDescription
AttachmentDescription::depth16()
{
    AttachmentDescription result;
    result.mInfo.format = vk::Format::eD16Unorm;
    result.mInfo.samples = vk::SampleCountFlagBits::e1;
    result.mInfo.loadOp = vk::AttachmentLoadOp::eClear;
    result.mInfo.storeOp = vk::AttachmentStoreOp::eStore;
    result.mInfo.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    result.mInfo.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    result.mInfo.initialLayout = vk::ImageLayout::eUndefined;
    result.mInfo.finalLayout =
        vk::ImageLayout::eDepthReadOnlyStencilAttachmentOptimal;
    return result;
}

AttachmentDescription
AttachmentDescription::depth32float()
{
    AttachmentDescription result;
    result.mInfo.format = vk::Format::eD32Sfloat;
    result.mInfo.samples = vk::SampleCountFlagBits::e1;
    result.mInfo.loadOp = vk::AttachmentLoadOp::eLoad;
    result.mInfo.storeOp = vk::AttachmentStoreOp::eStore;
    result.mInfo.stencilLoadOp = vk::AttachmentLoadOp::eLoad;
    result.mInfo.stencilStoreOp = vk::AttachmentStoreOp::eStore;
    result.mInfo.initialLayout =
        vk::ImageLayout::eDepthStencilAttachmentOptimal;
    result.mInfo.finalLayout =
        vk::ImageLayout::eDepthStencilAttachmentOptimal;
    return result;
}

AttachmentDescription
AttachmentDescription::color(GenericFormat format)
{
    AttachmentDescription result;
    result.mInfo.format = format.vkhpp();
    result.mInfo.samples = vk::SampleCountFlagBits::e1;
    result.mInfo.loadOp = vk::AttachmentLoadOp::eLoad;
    result.mInfo.storeOp = vk::AttachmentStoreOp::eStore;
    result.mInfo.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    result.mInfo.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    result.mInfo.initialLayout = vk::ImageLayout::eColorAttachmentOptimal;
    result.mInfo.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;
    return result;
}

AttachmentDescription &
AttachmentDescription::clear()
{
    mInfo.loadOp = vk::AttachmentLoadOp::eClear;
    mInfo.stencilLoadOp = vk::AttachmentLoadOp::eClear;
    mInfo.initialLayout = vk::ImageLayout::eUndefined;
    return *this;
}

AttachmentDescription &
AttachmentDescription::discard()
{
    mInfo.storeOp = vk::AttachmentStoreOp::eDontCare;
    mInfo.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    if (aspectsOf(mInfo.format) & vk::ImageAspectFlagBits::eColor) {
        mInfo.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;
    } else if (aspectsOf(mInfo.format) & vk::ImageAspectFlagBits::eDepth) {
        mInfo.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
    }
    return *this;
}

AttachmentDescription &
AttachmentDescription::finalLayout(vk::ImageLayout layout)
{
    mInfo.setFinalLayout(layout);
    return *this;
}

AttachmentDescription &
AttachmentDescription::finalLayout_Attachment()
{
    if (aspectsOf(mInfo.format) & vk::ImageAspectFlagBits::eColor) {
        return finalLayout(vk::ImageLayout::eColorAttachmentOptimal);
    }
    return finalLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);
}

AttachmentDescription &
AttachmentDescription::finalLayout_ShaderRead()
{
    return finalLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
}

AttachmentDescription &
AttachmentDescription::finalLayout_TransferSrc()
{
    return finalLayout(vk::ImageLayout::eTransferSrcOptimal);
}

AttachmentDescription &
AttachmentDescription::finalLayout_DepthStencilRead()
{
    return finalLayout(vk::ImageLayout::eDepthStencilReadOnlyOptimal);
}

AttachmentDescription &
AttachmentDescription::finalLayout_PresentSrc()
{
    return finalLayout(vk::ImageLayout::ePresentSrcKHR);
}

lava::SubpassDescription::operator vk::SubpassDescription()
{
    vk::SubpassDescription result;
    result.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics)
        .setColorAttachmentCount((uint32_t)colorAttachments.size())
        .setPColorAttachments(colorAttachments.data())
        .setInputAttachmentCount((uint32_t)inputAttachments.size())
        .setPInputAttachments(inputAttachments.data());
    if (depthAttachment != vk::AttachmentReference{})
        result.setPDepthStencilAttachment(&depthAttachment);
    result.flags = flags;

    return result;
}
} // namespace lava
