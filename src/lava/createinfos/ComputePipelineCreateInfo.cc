#include "ComputePipelineCreateInfo.hh"
#include <lava/objects/ComputePipeline.hh>

namespace lava
{

SharedComputePipeline
lava::ComputePipelineCreateInfo::create()
{
    return std::make_shared<ComputePipeline>(*this);
}

} // namespace lava
