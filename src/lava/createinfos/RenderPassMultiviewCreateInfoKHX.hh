#pragma once
#include "ExtensionStructure.hh"
#include "RenderPassCreateInfo.hh"
#include <bitset>
#include <lava/common/vulkan.hh>

namespace lava
{

class RenderPassMultiviewCreateInfoKHX : public ExtensionStructure
{
  public:
    RenderPassMultiviewCreateInfoKHX();

    /// Expect every attachment to be a layered texture and forward every
    /// subpass to accordingly
    RenderPassMultiviewCreateInfoKHX(RenderPassCreateInfo const &pass,
                                     uint32_t num_layers);

    // ExtensionStructure interface
    vk::StructureType type() const override;
    const void *get() override;

    std::vector<uint32_t> const &viewMasks() const { return mViewMask; }
    void addCorrelation(std::bitset<16> mask)
    {
        mCorrelationMasks.push_back(uint32_t(mask.to_ulong()));
    }

  protected:
    vk::RenderPassMultiviewCreateInfoKHR mInfo;
    std::vector<uint32_t> mViewMask;
    std::vector<int32_t> mViewOffsets;
    std::vector<uint32_t> mCorrelationMasks;
};
} // namespace lava
