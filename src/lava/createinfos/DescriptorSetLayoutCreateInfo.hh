#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class DescriptorSetLayoutCreateInfo
{
  public:
    using Self = DescriptorSetLayoutCreateInfo;
    DescriptorSetLayoutCreateInfo() = default;
    DescriptorSetLayoutCreateInfo(vk::DescriptorSetLayoutCreateInfo info)
    {
        for (size_t i = 0; i < info.bindingCount; i++) {
            mBindings.push_back(info.pBindings[i]);
        }
    }

    operator vk::DescriptorSetLayoutCreateInfo()
    {
        return mInfo.setBindingCount(uint32_t(mBindings.size()))
            .setPBindings(mBindings.data());
    }

    Self &addSampler(
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAllGraphics)
    {
        addBinding(vk::DescriptorType::eSampler, stage, 1);
        return *this;
    }

    Self &addCombinedImageSampler(
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAllGraphics)
    {
        addBinding(vk::DescriptorType::eCombinedImageSampler, stage, 1);
        return *this;
    }

    Self &addCombinedImageSamplers(
        uint32_t count,
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAllGraphics)
    {
        addBinding(vk::DescriptorType::eCombinedImageSampler, stage, count);
        return *this;
    }

    Self &addUniformBuffer(
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAll)
    {
        addBinding(vk::DescriptorType::eUniformBuffer, stage, 1);
        return *this;
    }

    Self &addSampledImage(
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAllGraphics)
    {
        addBinding(vk::DescriptorType::eSampledImage, stage, 1);
        return *this;
    }

    Self &addStorageBuffer(
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAll,
        uint32_t count = 1)
    {
        addBinding(vk::DescriptorType::eStorageBuffer, stage, count);
        return *this;
    }

    Self &addStorageImage(
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAll,
        uint32_t count = 1)
    {
        addBinding(vk::DescriptorType::eStorageImage, stage, count);
        return *this;
    }

    Self &
    addAccelerationStructure(vk::ShaderStageFlags stage =
                                 vk::ShaderStageFlagBits::eRaygenNV |
                                 vk::ShaderStageFlagBits::eAnyHitNV |
                                 vk::ShaderStageFlagBits::eClosestHitNV |
                                 vk::ShaderStageFlagBits::eMissNV,
                             uint32_t count = 1)
    {

        addBinding(vk::DescriptorType::eAccelerationStructureNV, stage,
                   count);
        return *this;
    }

    Self &addBinding(
        vk::DescriptorType type,
        vk::ShaderStageFlags stage = vk::ShaderStageFlagBits::eAllGraphics,
        uint32_t count = 1)
    {
        mBindings.push_back(vk::DescriptorSetLayoutBinding(
            uint32_t(mBindings.size()), type, count, stage));
        return *this;
    }

    SharedDescriptorSetLayout create(SharedDevice const &on,
                                     uint32_t poolsize = 4) const;

  protected:
    vk::DescriptorSetLayoutCreateInfo mInfo;
    std::vector<vk::DescriptorSetLayoutBinding> mBindings;
    friend class DescriptorSetLayout;
};
} // namespace lava
