#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{
class ExtensionStructure
{
  public:
    virtual vk::StructureType type() const = 0;
    void setNext(std::shared_ptr<ExtensionStructure> const &next)
    {
        mNext = next;
    }
    virtual void const *get() = 0;

    std::shared_ptr<ExtensionStructure> const &next() const
    {
        return mNext;
    }

  protected:
    std::shared_ptr<ExtensionStructure> mNext = nullptr;
};

using SharedExtensionStructure = std::shared_ptr<ExtensionStructure>;
} // namespace lava
