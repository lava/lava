#include "Framebuffer.hh"

#include <iterator>

#include <lava/common/FormatInfo.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/createinfos/RenderPassMultiviewCreateInfoKHX.hh>

namespace lava
{

Framebuffer::Framebuffer(const SharedRenderPass &pass,
                         const std::vector<SharedImageView> &views)
    : mPass(pass), mViews(views)
{

    init();
}

Framebuffer::Framebuffer(const SharedRenderPass &pass,
                         const std::vector<SharedImage> &images)
    : mPass(pass)
{
    for (auto &&i : images) {
        vk::ImageSubresourceRange range;
        range.aspectMask = aspectsOf(i->createInfo().format);
        range.baseArrayLayer = 0;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.layerCount = i->createInfo().arrayLayers;

        mViews.push_back(i->createView(range));
    }
    init();
}

Framebuffer::~Framebuffer()
{
    mPass->device()->handle().destroyFramebuffer(mHandle);
}

void
Framebuffer::init()
{
    auto viewHandleOf = [](SharedImageView const &i) {
        return i->handle();
    };
    std::transform(begin(mViews), end(mViews),
                   std::back_inserter(mCreateInfoViews), viewHandleOf);

    mCreateInfo.attachmentCount = uint32_t(mCreateInfoViews.size());
    mCreateInfo.pAttachments = mCreateInfoViews.data();

    mCreateInfo.width = INT32_MAX;
    mCreateInfo.height = INT32_MAX;
    mCreateInfo.layers = INT32_MAX;

    for (auto &&v : mViews) {
        if (v->image()) {
            mCreateInfo.width =
                std::min(mCreateInfo.width, v->image()->width());
            mCreateInfo.height =
                std::min(mCreateInfo.height, v->image()->height());
        }
        mCreateInfo.layers = std::min(mCreateInfo.layers, v->layers());
    }

    auto passCreateInfoNext = mPass->createInfo().next();
    if (passCreateInfoNext != nullptr)
    {
        auto structType = passCreateInfoNext->type();
        if (structType == vk::StructureType::eRenderPassMultiviewCreateInfoKHR)
        {
            auto multiViewCreateInfo = std::static_pointer_cast<lava::RenderPassMultiviewCreateInfoKHX>(passCreateInfoNext);
            assert(multiViewCreateInfo);
            if (multiViewCreateInfo->viewMasks().size() > 0 && multiViewCreateInfo->viewMasks()[0] > 0)
            {
                // VUID-VkFramebufferCreateInfo-renderPass-02531:
                // If renderPass was specified with non - zero view masks, layers must be 1
                mCreateInfo.layers = 1;
            }
        }
    }

    mCreateInfo.setRenderPass(mPass->handle());

    mHandle = mPass->device()->handle().createFramebuffer(mCreateInfo);
}
} // namespace lava
