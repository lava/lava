#include "RenderPass.hh"
#include <algorithm>
#include <lava/common/FormatInfo.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/createinfos/RenderPassCreateInfo.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/GraphicsPipeline.hh>

namespace lava
{

SharedGraphicsPipeline
Subpass::createPipeline(const GraphicsPipelineCreateInfo &info) const
{
    return pass->createPipeline(subpassIndex, info);
}

RenderPass::RenderPass(const SharedDevice &device,
                       const RenderPassCreateInfo &info)
    : mInfo(info), mDevice(device)
{
    mHandle = mDevice->handle().createRenderPass(mInfo);

    auto &&attachments = mInfo.attachments();
    for (size_t i = 0; i < attachments.size(); i++) {
        if (aspectsOf(attachments[i].format()) &
            vk::ImageAspectFlagBits::eColor) {
            mClearValues.push_back(vk::ClearColorValue{});
        } else if (aspectsOf(attachments[i].format()) &
                   (vk::ImageAspectFlagBits::eDepth |
                    vk::ImageAspectFlagBits::eStencil)) {
            mClearValues.push_back(vk::ClearDepthStencilValue{});
        } else {
            assert(false &&
                   "RenderPass attachments should have a color or a "
                   "depth/stencil aspect.");
        }
    }
}

RenderPass::~RenderPass() { mDevice->handle().destroyRenderPass(mHandle); }

SharedGraphicsPipeline
RenderPass::createPipeline(uint32_t subpass,
                           GraphicsPipelineCreateInfo info)
{
    info.mInfo.renderPass = handle();
    info.mInfo.subpass = subpass;
    info.mPass = shared_from_this();
    return std::make_shared<GraphicsPipeline>(info);
}

SharedFramebuffer
RenderPass::createFramebuffer(
    const std::vector<SharedImageView> &attachments)
{
    return std::make_shared<Framebuffer>(shared_from_this(), attachments);
}

SharedFramebuffer
RenderPass::createFramebuffer(const std::vector<SharedImage> &attachments)
{
    return std::make_shared<Framebuffer>(shared_from_this(), attachments);
}

void
RenderPass::setClearColor(vk::ClearColorValue color)
{
    for (size_t i = 0; i < mClearValues.size(); i++) {
        if (aspectsOf(mInfo.attachments()[i].format()) &
            vk::ImageAspectFlagBits::eColor) {
            mClearValues[i] = color;
        }
    }
}

void
RenderPass::setClearColors(const std::vector<vk::ClearColorValue> &colors)
{
    size_t colorsIdx = 0;
    for (size_t i = 0; i < mClearValues.size(); i++) {
        if (aspectsOf(mInfo.attachments()[i].format()) &
            vk::ImageAspectFlagBits::eColor) {
            assert(colorsIdx < colors.size() &&
                   "You need to provide as many clear colors as there are "
                   "color attachments in your RenderPass.");
            mClearValues[i] = colors[colorsIdx++];
        }
    }
}

void
RenderPass::setClearDepthStencil(vk::ClearDepthStencilValue depthStencil)
{
    for (size_t i = 0; i < mClearValues.size(); i++) {
        if (aspectsOf(mInfo.attachments()[i].format()) &
            (vk::ImageAspectFlagBits::eDepth |
             vk::ImageAspectFlagBits::eStencil)) {
            mClearValues[i] = depthStencil;
        }
    }
}

} // namespace lava
