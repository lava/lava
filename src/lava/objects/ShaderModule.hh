#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class ShaderModule
{
  public:
    LAVA_NON_MOVABLE(ShaderModule);

    ShaderModule(SharedDevice const &device, char const *begin,
                 char const *end);
    ~ShaderModule();

    vk::ShaderModule handle() const { return mHandle; }
    vk::ShaderStageFlagBits stage() const { return mStage; }

    void setStage(vk::ShaderStageFlagBits stage) { mStage = stage; }

  protected:
    SharedDevice mDevice;
    vk::ShaderModule mHandle;
    vk::ShaderStageFlagBits mStage = vk::ShaderStageFlagBits::eAll;
};
} // namespace lava
