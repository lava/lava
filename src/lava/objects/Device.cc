#include "Device.hh"
#include <mutex>

#include "Instance.hh"
#include <bitset>
#include <fstream>
#include <lava/common/ShaderExtensions.hh>
#include <lava/common/log.hh>
#include <lava/createinfos/Sampler.hh>
#include <lava/gpuselection/ISelectionStrategy.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorPool.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/PipelineLayout.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/objects/Sampler.hh>
#include <lava/objects/ShaderModule.hh>
#include <lava/objects/Suballocator.hh>

namespace lava
{

Device::Device(SharedInstance instance,
               const std::vector<SharedFeature> &features,
               const ISelectionStrategy &gpuSelectionStrategy,
               const std::vector<QueueRequest> &queues)
    : mInstance(instance), mFeatures(features)
{

    pickPhysicalDevice(gpuSelectionStrategy, features);
    createLogicalDevice({mPhysicalDevice}, queues);
    mIsOwnDeviceHandle = true;
    mSuballocator.reset(new Suballocator(
        *this, mPhyProperties.limits.bufferImageGranularity));
}

Device::Device(SharedInstance instance,
               const std::vector<Device::SharedFeature> &features,
               const IGroupAssemblyStrategy &groupAssembly,
               const std::vector<QueueRequest> &queues)
    : mInstance(instance), mFeatures(features)
{

    auto groups = instance->handle().enumeratePhysicalDeviceGroups();
    auto group = groupAssembly.assembleFrom(groups);
    mPhysicalDevice = group.front();

    for (auto &&feat : mFeatures) {
        feat->onPhysicalDeviceSelected(mPhysicalDevice);
    }

    mPhyProperties = mPhysicalDevice.getProperties();
    createLogicalDevice(group, queues);
    mIsOwnDeviceHandle = true;
    mSuballocator.reset(new Suballocator(
        *this, mPhyProperties.limits.bufferImageGranularity));
}

Device::Device(SharedInstance instance,
               std::vector<SharedFeature> const &features,
               VkPhysicalDevice phyDeviceHandle,
               VkDevice logicalDeviceHandle,
               const std::vector<QueueFamilyInfo> &familyInfos)
    : mInstance(instance), mFeatures(features)
{
    mPhysicalDevice = vk::PhysicalDevice(phyDeviceHandle);

    for (auto &&feat : mFeatures) {
        feat->onPhysicalDeviceSelected(mPhysicalDevice);
    }
    mPhyProperties = mPhysicalDevice.getProperties();

    mDevice = vk::UniqueDevice(logicalDeviceHandle);
    mIsOwnDeviceHandle = false;

    for (auto const &familyInfo : familyInfos) {
        if (mPools.find(familyInfo.familyIdx) == mPools.end()) {
            mPools[familyInfo.familyIdx] = mDevice->createCommandPoolUnique(
                {vk::CommandPoolCreateFlagBits::eResetCommandBuffer |
                     vk::CommandPoolCreateFlagBits::eTransient,
                 familyInfo.familyIdx});
        }
        auto pool = mPools[familyInfo.familyIdx].get();
        mQueues.emplace(
            familyInfo.name,
            Queue(familyInfo.familyIdx, familyInfo.queue, pool, this));
    }

    mSuballocator.reset(new Suballocator(
        *this, mPhyProperties.limits.bufferImageGranularity));
}

Device::~Device()
{
    for (auto&& feat : mFeatures)
        feat->beforeDeviceDestruction();
    for (auto& queue : mQueues) {
        queue.second.catchUp(0);
    }
    mQueues.clear();
    mPools.clear(); // pools will be destroyed in UniqueHandle

    if (mIsOwnDeviceHandle) {
        mDevice->destroy();
    }
    mDevice.release();
}

Queue &
Device::namedQueue(const std::string &name)
{
    auto it = mQueues.find(name);
    assert(it != mQueues.end() && "No Queue with this name exists.");
    return it->second;
}

Queue &
Device::graphicsQueue()
{
    return namedQueue("graphics");
}

Queue &
Device::transferQueue()
{
    auto it = mQueues.find("transfer");
    if (it != mQueues.end())
        return it->second;
    return graphicsQueue();
}

SharedBuffer
Device::createBuffer(const vk::BufferCreateInfo &info)
{
    return std::make_shared<Buffer>(shared_from_this(), info);
}

SharedRenderPass
Device::createRenderPass(const RenderPassCreateInfo &info)
{
    return std::make_shared<RenderPass>(shared_from_this(), info);
}

SharedDescriptorPool
Device::createDescriptorPool(const DescriptorPoolCreateInfo &info)
{
    return std::make_shared<DescriptorPool>(shared_from_this(), info);
}

SharedDescriptorSetLayout
Device::createDescriptorSetLayout(
    const vk::DescriptorSetLayoutCreateInfo &info, size_t poolSize)
{
    return std::make_shared<DescriptorSetLayout>(shared_from_this(), info,
                                                 (uint32_t)poolSize);
}

SharedPipelineLayout
Device::createPipelineLayout(
    const std::vector<vk::PushConstantRange> &constantRanges,
    const std::vector<SharedDescriptorSetLayout> &descriptorSets)
{
    return std::make_shared<PipelineLayout>(shared_from_this(),
                                            descriptorSets, constantRanges);
}

SharedShaderModule
Device::createShader(const char *begin, const char *end)
{
    return std::make_shared<ShaderModule>(shared_from_this(), begin, end);
}

SharedShaderModule
Device::createShaderFromFile(const std::string &filename)
{
    auto file = std::ifstream(filename, std::ios::binary);
    assert(file.good());
    file.unsetf(std::ios::skipws);
    std::vector<char> code(std::istreambuf_iterator<char>(file),
                           std::istreambuf_iterator<char>{});

    auto shader = createShader(code);
    auto stage = identifyShader(filename);
    if (stage) {
        shader->setStage(stage.value());
    }

    return shader;
}

void
Device::pickPhysicalDevice(const ISelectionStrategy &gpuSelectionStrategy,
                           const std::vector<SharedFeature> &features)
{
    auto devices = mInstance->enumeratePhysicalDevices();
    auto deviceBad = [&](vk::PhysicalDevice device) {
        auto unsupported = [&](SharedFeature const &feature) {
            return !(feature->supportsDevice(device));
        };

        return std::any_of(begin(features), end(features), unsupported);
    };

    std::remove_if(begin(devices), end(devices), deviceBad);

    mPhysicalDevice = gpuSelectionStrategy.selectFrom(devices);

    for (auto &&feat : mFeatures)
        feat->onPhysicalDeviceSelected(mPhysicalDevice);

    mPhyProperties = mPhysicalDevice.getProperties();
}

namespace
{
uint8_t
bitsSet(uint32_t of)
{
    return uint8_t(std::bitset<32>{of}.count());
}

void
resolveQueueRequest(QueueRequest &req,
                    std::vector<vk::QueueFamilyProperties> const &families)
{
    if (req.index != QueueRequest::no_index)
        return;
    uint8_t bestBitcount = uint8_t(-1);
    for (uint32_t i = 0; i < families.size(); i++) {
        auto &&family = families[i];
        if (family.queueFlags == req.flags) {
            req.index = i;
            return;
        }
        if (family.queueFlags & req.flags) {
            auto bitcount = bitsSet(uint32_t(family.queueFlags));
            if (bitcount < bestBitcount) {
                req.index = i;
                bestBitcount = bitcount;
            }
        }
    }
}
} // namespace

void
Device::createLogicalDevice(std::vector<vk::PhysicalDevice> const &phys,
                            std::vector<QueueRequest> queues)
{

    auto available_exts =
        mPhysicalDevice.enumerateDeviceExtensionProperties();

    // Throw in the queues requested by Features
    auto families = mPhysicalDevice.getQueueFamilyProperties();
    for (auto &&feat : mFeatures) {
        auto fams = feat->queueRequests(families);
        copy(begin(fams), end(fams), back_inserter(queues));
    }

    for (auto &q : queues)
        resolveQueueRequest(q, families);

    // Group requested queues by family index
    struct FamilyInfo {
        uint32_t index;
        std::vector<std::string> names;
        std::vector<float> priorities;
    };
    std::unordered_map<uint32_t, FamilyInfo> familyInfos;
    for (auto q : queues) {
        auto &info = familyInfos[q.index];
        info.index = q.index;
        info.names.push_back(q.name);
        info.priorities.push_back(q.priority);
    }

    std::vector<vk::DeviceQueueCreateInfo> qinfo;
    for (auto const &pair : familyInfos) {
        auto const &info = pair.second;

        auto queueCount = std::min(families[info.index].queueCount,
                                   uint32_t(info.priorities.size()));
        qinfo.push_back(
            {{}, info.index, queueCount, info.priorities.data()});
    }

    std::vector<const char *> extensions;
    for (auto &&feat : mFeatures) {
        auto add = feat->deviceExtensions();
        copy(begin(add), end(add), back_inserter(extensions));
    }

    vk::PhysicalDeviceFeatures features;
    features.setSamplerAnisotropy(true);
    for (auto &&feat : mFeatures)
        feat->addPhysicalDeviceFeatures(features);

    features::IFeature::NextPtr features2 = nullptr;
    for (auto &&feat : mFeatures)
        feat->addPhysicalDeviceFeatures2(features2);

    vk::DeviceCreateInfo info;
    info.setQueueCreateInfoCount((uint32_t)qinfo.size());
    info.setPQueueCreateInfos(qinfo.data());
    info.setEnabledExtensionCount((uint32_t)extensions.size());
    info.setPpEnabledExtensionNames(extensions.data());

    vk::DeviceGroupDeviceCreateInfo groupInfo;
    if (phys.size() > 1) {
        groupInfo.physicalDeviceCount = phys.size();
        groupInfo.pPhysicalDevices = phys.data();
        info.pNext = &groupInfo;
    }

    if (features2) {
        // Some extension needs features from the *Features2KHR extension.
        // The specific features go in the next-field of the extension
        // struct.

        info.setPEnabledFeatures(nullptr);

        vk::PhysicalDeviceFeatures2KHR wrapper{features};
        wrapper.pNext = features2;
        info.pNext = &wrapper;
        mDevice = mPhysicalDevice.createDeviceUnique(info);
    } else {
        // Only level 1 extensions are used, no weird magic here.
        info.setPEnabledFeatures(&features);
        mDevice = mPhysicalDevice.createDeviceUnique(info);
    }

    for (auto const &pair : familyInfos) {
        mPools[pair.second.index] = mDevice->createCommandPoolUnique(
            {vk::CommandPoolCreateFlagBits::eResetCommandBuffer |
                 vk::CommandPoolCreateFlagBits::eTransient,
             pair.second.index});

        auto pool = mPools[pair.second.index].get();
        for (uint32_t i = 0; i < pair.second.names.size(); i++) {
            auto const &name = pair.second.names[i];
            auto const &family = pair.second.index;
            auto queueIndex = i % families[family].queueCount;
            auto queue = mDevice->getQueue(family, queueIndex);

            mQueues.emplace(name, Queue(family, queue, pool, this));
        }
    }
}

SharedSampler
Device::createSampler(SamplerCreateInfo const &info)
{
    return std::make_shared<Sampler>(shared_from_this(), info);
}
} // namespace lava
