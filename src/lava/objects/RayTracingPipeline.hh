#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/createinfos/RayTracingPipelineCreateInfo.hh>
#include <lava/fwd.hh>

namespace lava
{

class RayTracingPipeline
{
  public:
    LAVA_NON_MOVABLE(RayTracingPipeline);

    static SharedRayTracingPipeline
    create(RayTracingPipelineCreateInfo const &info)
    {
        return std::make_shared<RayTracingPipeline>(info);
    }

    RayTracingPipeline(RayTracingPipelineCreateInfo const &info);
    ~RayTracingPipeline();

    vk::Pipeline handle() const { return mHandle; }

    void bind(RecordingCommandBuffer &cmd);
    SharedBuffer const &shaderBindingTable() const
    {
        return mShaderBindingTable;
    }

    SharedPipelineLayout const &layout() const
    {
        return mCreateInfo.layout();
    }

    struct OffsetStride {
        OffsetStride() {}
        int offset = 0;
        int stride = 1;
    };

    void trace(RecordingCommandBuffer &cmd, uint32_t width, uint32_t height,
               uint32_t depth = 1, int raygen = 0, OffsetStride miss = {},
               OffsetStride hit = {});

  protected:
    vk::Pipeline mHandle;
    vk::PhysicalDeviceRayTracingPropertiesNV mRaytracingProperties;
    SharedDevice mDevice;
    SharedBuffer mShaderBindingTable;
    RayTracingPipelineCreateInfo mCreateInfo;
};

} // namespace lava
