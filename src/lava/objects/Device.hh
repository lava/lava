#pragma once
#include <lava/common/vulkan.hh>
#include <memory>
#include <unordered_map>

#include <lava/common/MemoryType.hh>
#include <lava/common/NoCopy.hh>
#include <lava/createinfos/fwd.hh>
#include <lava/features/IFeature.hh>
#include <lava/fwd.hh>
#include <lava/objects/Queue.hh>

namespace lava
{

class ISelectionStrategy;
class IGroupAssemblyStrategy;

struct QueueFamilyInfo {
    uint32_t familyIdx;
    std::string name;
    VkQueue queue;

    QueueFamilyInfo(uint32_t familyIdx, std::string name, VkQueue queue)
        : familyIdx(familyIdx), name(name), queue(queue)
    {
    }
};

class Device : public std::enable_shared_from_this<Device>
{
    using SharedFeature = features::SharedFeature;

  public:
    LAVA_NON_MOVABLE(Device);

    /// use Instance::createDevice instead...
    Device(SharedInstance instance,
           std::vector<SharedFeature> const &features,
           ISelectionStrategy const &gpuSelectionStrategy,
           std::vector<QueueRequest> const &queues);
    Device(SharedInstance instance,
           std::vector<SharedFeature> const &features,
           IGroupAssemblyStrategy const &gpuSelectionStrategy,
           std::vector<QueueRequest> const &queues);
    Device(SharedInstance instance,
           std::vector<SharedFeature> const &features,
           VkPhysicalDevice phyDeviceHandle, VkDevice logicalDeviceHandle,
           const std::vector<QueueFamilyInfo> &familyInfos);
    ~Device();

    Queue &namedQueue(std::string const &name);

    /// get the Queue named "graphics"
    Queue &graphicsQueue();
    /// get the Queue named "transfer", or "graphics" if the former doesn't
    /// exist
    Queue &transferQueue();

    vk::Device handle() const { return mDevice.get(); }
    vk::PhysicalDevice physical() const { return mPhysicalDevice; }
    SharedInstance const &instance() const { return mInstance; }

    /// Creates a buffer with the given properties bound to the device
    SharedBuffer createBuffer(vk::BufferCreateInfo const &info);

    SharedRenderPass createRenderPass(RenderPassCreateInfo const &info);
    SharedDescriptorPool
    createDescriptorPool(DescriptorPoolCreateInfo const &info);

    /** Creates a descriptor set layout that comes with a pool big enough to
     * fit poolSize descriptor sets with this layout. Setting poolSize = 0
     * disables the automatic generation of the descriptor pool **/
    SharedDescriptorSetLayout
    createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo const &info,
                              size_t poolSize = 4);

    SharedPipelineLayout createPipelineLayout(
        std::vector<vk::PushConstantRange> const &constantRanges = {},
        std::vector<SharedDescriptorSetLayout> const &descriptorSets = {});

    /// Create a pipeline layout with a global push constant range based on
    /// the given type
    template <typename PushConstants>
    SharedPipelineLayout createPipelineLayout(
        std::vector<SharedDescriptorSetLayout> const &descriptorSets = {});

    SharedShaderModule createShader(const std::vector<char> &code)
    {
        return createShader(code.data(), code.data() + code.size());
    }
    SharedShaderModule createShader(char const *begin, char const *end);
    SharedShaderModule createShaderFromFile(std::string const &filename);

    SharedSampler createSampler(SamplerCreateInfo const &info);

    SharedFramebuffer createFramebuffer(FramebufferCreateInfo const &info);

    Suballocator *suballocator() { return mSuballocator.get(); }

    std::vector<SharedFeature> const &features() const { return mFeatures; }

    template <typename Feature> std::shared_ptr<Feature> get() const;

    vk::PhysicalDeviceProperties const &getPhysicalDeviceProperties() const
    {
        return mPhyProperties;
    }

  protected:
    SharedInstance mInstance;
    vk::PhysicalDevice mPhysicalDevice;
    vk::UniqueDevice mDevice;

    std::vector<SharedFeature> mFeatures;
    std::unordered_map<std::string, Queue> mQueues;
    std::unordered_map<uint32_t, vk::UniqueCommandPool> mPools;

    UniqueSuballocator mSuballocator;

    vk::PhysicalDeviceProperties mPhyProperties;

    void pickPhysicalDevice(const ISelectionStrategy &gpuSelectionStrategy,
                            const std::vector<SharedFeature> &features);
    void createLogicalDevice(const std::vector<vk::PhysicalDevice> &phys,
                             std::vector<QueueRequest> queues);

  private:
    bool mIsOwnDeviceHandle = false;
};

template <typename PushConstants>
SharedPipelineLayout
Device::createPipelineLayout(
    std::vector<SharedDescriptorSetLayout> const &descriptorSets)
{
    vk::PushConstantRange range;
    range.size = sizeof(PushConstants);
    range.offset = 0;
    range.stageFlags = vk::ShaderStageFlagBits::eAll;
    return createPipelineLayout({range}, descriptorSets);
}

template <typename Feature>
std::shared_ptr<Feature>
Device::get() const
{
    for (auto const &feat : mFeatures) {
        auto ptr = std::dynamic_pointer_cast<Feature>(feat);
        if (ptr)
            return ptr;
    }
    return nullptr;
}

} // namespace lava
