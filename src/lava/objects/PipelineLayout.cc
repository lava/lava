#include "PipelineLayout.hh"

#include <iterator>

#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/GraphicsPipeline.hh>

namespace lava
{

PipelineLayout::PipelineLayout(
    const SharedDevice &device,
    const std::vector<SharedDescriptorSetLayout> &descriptors,
    const std::vector<vk::PushConstantRange> &pushConstants)

    : mDevice(device), mDescriptors(descriptors),
      mPushConstants(pushConstants)
{

    std::transform(
        begin(mDescriptors), end(mDescriptors),
        std::back_inserter(mDescriptorHandles),
        [](SharedDescriptorSetLayout const &d) { return d->handle(); });

    for (auto &&d : descriptors)
        mCreateInfo.addSetLayout(d->handle());
    for (auto &&p : pushConstants)
        mCreateInfo.addPushConstantRange(p);

    mHandle = mDevice->handle().createPipelineLayout(mCreateInfo);
}

PipelineLayout::~PipelineLayout()
{
    mDevice->handle().destroyPipelineLayout(mHandle);
}

} // namespace lava
