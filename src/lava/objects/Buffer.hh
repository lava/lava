#pragma once
#include <vector>

#include <lava/common/MemoryType.hh>
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <lava/raii/Barriers.hh>

namespace lava
{

enum class MemoryType;

class Buffer
{
  public:
    LAVA_NON_MOVABLE(Buffer);

    Buffer(SharedDevice device, vk::BufferCreateInfo info);

    /// set the Buffer to the given data, automatically uses the
    /// graphics-queue of its device if the Memory is not host visible
    template <typename T> void setDataVRAM(std::vector<T> const &data)
    {
        setDataVRAM(data.data(), sizeof(T) * data.size());
    }

    /// set the Buffer to the given data, uses cmd for transfer if the
    /// Memory is not host visible
    template <typename T>
    BufferBarrier setDataVRAM(std::vector<T> const &data,
                              RecordingCommandBuffer &cmd)
    {
        return setDataVRAM(data.data(), sizeof(T) * data.size(), cmd);
    }

    /// set the Buffer to the given data, uses Memory that is host visible,
    /// so no CommandBuffers/Queues are involved
    template <typename T> void setDataRAM(std::vector<T> const &data)
    {
        setDataRAM(data.data(), sizeof(T) * data.size());
    }

    /// set the Buffer to the given data, uses cmd for transfer if the
    /// Memory is not host visible
    BufferBarrier setDataVRAM(const void *data, size_t length,
                              RecordingCommandBuffer &cmd);
    /// set the Buffer to the given data, automatically uses the
    /// graphics-queue of its device if the Memory is not host visible
    void setDataVRAM(const void *data, size_t length);
    /// set the Buffer to the given data, uses Memory that is host visible,
    /// so no CommandBuffers/Queues are involved
    void setDataRAM(const void *data, size_t length);

    /// set the data by storing the new data inside a CommandBuffer, use
    /// only for small updates
    template <typename T> void pushData(std::vector<T> const &data)
    {
        pushData(data.data(), sizeof(T) * data.size());
    }
    template <typename T>
    BufferBarrier pushData(std::vector<T> const &data,
                           RecordingCommandBuffer &cmd)
    {
        return pushData(data.data(), sizeof(T) * data.size(), cmd);
    }
    void pushData(const void *data, size_t length);
    BufferBarrier pushData(const void *data, size_t length,
                           RecordingCommandBuffer &cmd);

    /// copy the data into a vector of T
    template <typename T> std::vector<T> getData();
    /// copy the data to the chunk of memory behind data, must already be
    /// allocated and sufficiently large
    void getData(void *data);

    /// Allocates a MemoryChunk from RAM and binds it to the Buffer
    void realizeRAM();
    /// Allocates a MemoryChunk from VRAM and binds it to the Buffer
    void realizeVRAM();

    /// copy content of another Buffer to this Buffer, uses the
    /// graphics-queue of its device
    void copyFrom(SharedBuffer const &other);
    /// copy content of another Buffer to this Buffer, uses the provided
    /// CommandBuffer
    BufferBarrier copyFrom(SharedBuffer const &other,
                           RecordingCommandBuffer &cmd);
    /// allows to copy multiple regions of a buffer, no matter they are consecutive or not, to the
    /// specific offsets of the destination buffer.
    BufferBarrier copyFrom(SharedBuffer const& other, const std::vector<vk::BufferCopy>& regions,
                           RecordingCommandBuffer& cmd);

    /// copy all layers of the image to this buffer.
    BufferBarrier copyFrom(SharedImage const& image,
                           RecordingCommandBuffer& cmd);
    /// copy the specific layer of the image this buffer
    BufferBarrier copyFrom(SharedImage const& image, uint32_t baseArrayLayer, uint32_t layerCount,
                           RecordingCommandBuffer& cmd);

    vk::Buffer handle() const
    {
        assert(mHandle != vk::Buffer{} &&
               "Cannot get the Buffer handle before "
               "the size of the Buffer is "
               "determined.");
        return mHandle;
    }
    vk::BufferView view() const { return mView; }

    ~Buffer();

    void keepStagingBuffer(bool val = true) { mKeepStagingBuffer = val; }

    SharedMemoryChunk const &memoryChunk() const { return mMemory; }

    vk::DeviceSize size() const { return mCreateInfo.size; }

  protected:
    void initHandle(size_t dataLen);
    void initMemory(MemoryType type);

    SharedBuffer stagingBuffer();

    vk::Buffer mHandle;
    vk::BufferView mView;
    vk::BufferCreateInfo mCreateInfo;

    SharedMemoryChunk mMemory;
    SharedDevice mDevice;
    SharedBuffer mStagingBuffer;
    bool mKeepStagingBuffer = false; // useful for very frequent updates
};

template <typename T>
std::vector<T>
Buffer::getData()
{
    std::vector<T> result;
    result.resize(mCreateInfo.size / sizeof(T));
    getData(result.data());
    return result;
}
} // namespace lava
