#include "Queue.hh"
#include <lava/common/log.hh>
#include <lava/objects/CommandBuffer.hh>
#include <lava/objects/Device.hh>

namespace lava
{

SharedCommandBuffer
Queue::createCommandBuffer(vk::CommandBufferLevel level)
{
    return std::make_shared<CommandBuffer>(this, level);
}

RecordingCommandBuffer
Queue::beginCommandBuffer()
{
    auto buf = createCommandBuffer(vk::CommandBufferLevel::ePrimary);
    auto rec = buf->begin();
    rec.autoSubmit();
    return rec;
}

void
Queue::submit(SharedCommandBuffer cmd,
              const std::vector<vk::Semaphore> &waitSemaphores,
              const std::vector<vk::PipelineStageFlags> &waitStages,
              const std::vector<vk::Semaphore> &signalSemaphores)
{
    auto handle = cmd->handle();
    auto fence = findFreeFence();
    mSubmissionFences.emplace_back(fence);
    mSubmissionBuffers.emplace_back(move(cmd));

    vk::SubmitInfo info;
    info.commandBufferCount = 1;
    info.pCommandBuffers = &handle;
    info.signalSemaphoreCount = (uint32_t)signalSemaphores.size();
    info.pSignalSemaphores = signalSemaphores.data();
    info.waitSemaphoreCount = (uint32_t)waitSemaphores.size();
    info.pWaitSemaphores = waitSemaphores.data();
    info.pWaitDstStageMask = waitStages.data();

    mQueue.submit({info}, fence);
}

void
Queue::gc()
{
    // Garbage collection
    for (auto i = 0u; i < mSubmissionFences.size();) {
        auto fence = mSubmissionFences[i];
        if (mDevice->handle().getFenceStatus(fence) ==
            vk::Result::eSuccess) {
            mFencePool.emplace_back(fence);
            mSubmissionFences.erase(begin(mSubmissionFences) + i);
            mSubmissionBuffers.erase(begin(mSubmissionBuffers) + i);
        } else {
            i++;
        }
    }
}

void
Queue::catchUp(int inflightBuffers)
{
    while (int(mSubmissionFences.size()) > inflightBuffers) {
        mDevice->handle().waitForFences(mSubmissionFences, VK_FALSE, -1);
        gc();
    }
}

size_t
Queue::activeBuffers()
{
    gc();
    return mSubmissionFences.size();
}

vk::Fence
Queue::findFreeFence()
{
    if (!mFencePool.empty()) {
        auto result = mFencePool.back();
        mFencePool.pop_back();
        mDevice->handle().resetFences(result);
        return result;
    } else {
        try {
            return mDevice->handle().createFence({});
        } catch (vk::OutOfHostMemoryError const &e) {
            lava::error()
                << "Queue::findFreeFence(): Ran out of memory trying "
                   "to allocate a fence for CommandBuffer "
                   "submission. \n Use Queue::catchUp to prevent "
                   "oversized backlogs of submissions.";
            throw e;
        }
    }
}

} // namespace lava
