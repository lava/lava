#pragma once
#include "lava/common/NoCopy.hh"
#include <functional>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class MappedMemory
{
  public:
    MappedMemory(MappedMemory const &) = delete;
    MappedMemory &operator=(MappedMemory const &) = delete;

    MappedMemory(MappedMemory &&o);
    MappedMemory &operator=(MappedMemory &&o);

    MappedMemory(MemoryChunk *mem, vk::DeviceSize dataOffset,
                 vk::DeviceSize size);
    ~MappedMemory();

    void *data() { return mData; }

  protected:
    vk::DeviceSize mOffset, mSize;
    vk::Device mDevice;
    vk::DeviceMemory mMemory;
    void *mData;
};

class MemoryChunk
{
  public:
    using Deallocator = std::function<void(MemoryChunk &)>;

    MemoryChunk(vk::DeviceMemory memory, SharedDevice const &device,
                vk::DeviceSize allocationOffset, vk::DeviceSize offset,
                vk::DeviceSize size, uint32_t type, bool mappable,
                Deallocator const &dealloc);

    MemoryChunk(MemoryChunk const &) = delete;
    MemoryChunk(MemoryChunk &&rhs);
    MemoryChunk &operator=(MemoryChunk const &) = delete;
    MemoryChunk &operator=(MemoryChunk &&rhs);
    ~MemoryChunk();

    void bindToBuffer(vk::Buffer buffer);
    void bindToImage(vk::Image image);

    vk::DeviceMemory handle() const { return mMemory; }
    SharedDevice const &device() const { return mDevice; }
    vk::DeviceSize offset() const { return mOffset; }
    vk::DeviceSize size() const { return mSize; }
    vk::DeviceSize allocationOffset() const { return mAllocationOffset; }
    uint32_t type() const { return mType; }

    bool mappable() const { return mMappable; }

    MappedMemory map(vk::DeviceSize dataOffset = 0,
                     vk::DeviceSize size = VK_WHOLE_SIZE)
    {
        return {this, dataOffset, size};
    }

  private:
    vk::DeviceMemory mMemory;
    SharedDevice mDevice;
    vk::DeviceSize mOffset;
    vk::DeviceSize mSize;
    vk::DeviceSize mAllocationOffset;
    uint32_t mType;
    bool mMappable;

    // The MemoryChunk will call this function at destruction. If no
    // deallocator was set, the MemoryChunk will instead destroy the
    // DeviceMemory object directly.
    Deallocator mDeallocate;
};
} // namespace lava
