#pragma once

#include "lava/common/NoCopy.hh"
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

enum class MemoryType { VRAM, RAM };

class Suballocator
{
  public:
    Suballocator(Device &device, vk::DeviceSize biGranularity) noexcept;

    SharedMemoryChunk allocate(vk::MemoryRequirements req,
                               vk::MemoryPropertyFlags flags);
    SharedMemoryChunk allocateDedicated(vk::MemoryRequirements req,
                                        vk::MemoryPropertyFlags flags);
    SharedMemoryChunk allocateDedicated(vk::MemoryRequirements req,
                                        vk::MemoryPropertyFlags flags,
                                        vk::Buffer buffer);
    SharedMemoryChunk allocateDedicated(vk::MemoryRequirements req,
                                        vk::MemoryPropertyFlags flags,
                                        vk::Image image);

    SharedMemoryChunk allocate(vk::MemoryRequirements req, MemoryType type);
    SharedMemoryChunk allocateDedicated(vk::MemoryRequirements req,
                                        MemoryType type);
    SharedMemoryChunk allocateDedicated(vk::MemoryRequirements req,
                                        MemoryType type, vk::Buffer buffer);
    SharedMemoryChunk allocateDedicated(vk::MemoryRequirements req,
                                        MemoryType type, vk::Image image);

    size_t heldBlockCount() const;

  private:
    SharedMemoryChunk internalAllocateDedicated(
        vk::MemoryRequirements req, vk::MemoryPropertyFlags flags,
        vk::Buffer buffer = vk::Buffer(), vk::Image image = vk::Image());
    void deallocate(MemoryChunk &chunk);
    uint32_t typeIndexFor(vk::MemoryRequirements req,
                          vk::MemoryPropertyFlags flags);

    struct Hole {
        vk::DeviceSize begin;
        vk::DeviceSize end;
    };

    struct MemoryBlock {
        SharedMemoryChunk memory;
        vk::DeviceSize size;

        std::vector<Hole> holes;
    };

    std::array<std::vector<MemoryBlock>, VK_MAX_MEMORY_TYPES> mTypeBlocks;

    Device &mDevice;

    vk::DeviceSize mLargeHeapBlockSize = 256 * 1024 * 1024;
    vk::DeviceSize mSmallHeapBlockSize = 64 * 1024 * 1024;

    vk::DeviceSize mSmallHeapThreshold = 512 * 1024 * 1024;

    vk::DeviceSize mBufferImageGranularity;

    vk::PhysicalDeviceMemoryProperties mMemoryProperties;
};
} // namespace lava
