#include "DescriptorSet.hh"
#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorPool.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/Sampler.hh>
#include <lava/objects/TopLevelAccelerationStructure.hh>

namespace lava
{

DescriptorSet::DescriptorSet(const SharedDevice &device,
                             const SharedDescriptorPool &pool,
                             const SharedDescriptorSetLayout &layout)
    : mDevice(device), mPool(pool), mLayout(layout)
{
    vk::DescriptorSetLayout setLayout = layout->handle();
    vk::DescriptorSetAllocateInfo info;
    info.descriptorPool = pool->handle();
    info.descriptorSetCount = 1;
    info.pSetLayouts = &setLayout;
    mHandle = mDevice->handle().allocateDescriptorSets(info)[0];
}

DescriptorSet::~DescriptorSet()
{
    mDevice->handle().freeDescriptorSets(mPool->handle(), {mHandle});
}

void
DescriptorSet::writeCombinedImageSamplers(
    std::vector<std::pair<SharedSampler, SharedImageView>> const &combos,
    uint32_t binding)
{
    vk::WriteDescriptorSet writeInfo;
    writeInfo.descriptorCount = uint32_t(combos.size());
    writeInfo.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    writeInfo.dstSet = mHandle;
    writeInfo.dstBinding = binding;

    std::vector<vk::DescriptorImageInfo> infos;
    for (auto const &pair : combos) {
        vk::DescriptorImageInfo info;
        info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        info.imageView = pair.second->handle();
        info.sampler = pair.first->handle();
        infos.push_back(info);

        auto &resources = mBindingResources[binding];
        resources.clear();
        resources.push_back(pair.first);
        resources.push_back(pair.second);
    }
    writeInfo.pImageInfo = infos.data();

    mDevice->handle().updateDescriptorSets(writeInfo, {});
}

void
DescriptorSet::writeCombinedImageSampler(
    std::pair<SharedSampler, SharedImageView> const &combo,
    uint32_t binding)
{
    writeCombinedImageSamplers({combo}, binding);
}

void
DescriptorSet::writeUniformBuffer(const SharedBuffer &buffer,
                                  uint32_t binding)
{
    vk::WriteDescriptorSet writeInfo;
    writeInfo.descriptorCount = 1;
    writeInfo.descriptorType = vk::DescriptorType::eUniformBuffer;
    writeInfo.dstSet = mHandle;
    writeInfo.dstBinding = binding;

    vk::DescriptorBufferInfo info;
    info.buffer = buffer->handle();
    info.range = VK_WHOLE_SIZE;
    info.offset = 0;

    writeInfo.pBufferInfo = &info;

    auto &resources = mBindingResources[binding];
    resources.clear();
    resources.push_back(buffer);

    mDevice->handle().updateDescriptorSets(writeInfo, {});
}

void
DescriptorSet::writeSampledImage(const SharedImageView &image,
                                 uint32_t binding)
{
    vk::WriteDescriptorSet write;
    write.descriptorCount = 1;
    write.descriptorType = vk::DescriptorType::eSampledImage;
    write.dstSet = mHandle;
    write.dstBinding = binding;

    vk::DescriptorImageInfo info;
    info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    info.imageView = image->handle();

    auto &resources = mBindingResources[binding];
    resources.clear();
    resources.push_back(image);

    write.pImageInfo = &info;
    mDevice->handle().updateDescriptorSets(write, {});
}

DescriptorSetWriter &
DescriptorSetWriter::sampledImage(SharedImageView view)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(view);

    auto *info = infos.alloc<vk::DescriptorImageInfo>();
    info->setImageView(view->handle());
    info->setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eSampledImage)
            .setPImageInfo(info));

    currentBinding++;
    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::combinedImageSampler(SharedSampler sampler,
                                          SharedImageView view)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(view);
    res.push_back(sampler);

    auto *info = infos.alloc<vk::DescriptorImageInfo>();
    info->setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
    info->setImageView(view->handle());
    info->setSampler(sampler->handle());

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
            .setPImageInfo(info));

    currentBinding++;
    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::combinedImageSamplers(
    SharedSampler sampler, const std::vector<SharedImageView> &views)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();

    res.push_back(sampler);
    if (!views.empty()) {
        auto *info = infos.alloc<vk::DescriptorImageInfo>(views.size());

        for (auto i = 0u; i < views.size(); i++) {
            auto const &view = views[i];
            res.push_back(view);

            info[i].setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
            info[i].setImageView(view->handle());
            info[i].setSampler(sampler->handle());
        }

        writes.push_back(vk::WriteDescriptorSet()
                             .setDstSet(set->handle())
                             .setDstBinding(currentBinding)
                             .setDescriptorCount(views.size())
                             .setDescriptorType(
                                 vk::DescriptorType::eCombinedImageSampler)
                             .setPImageInfo(info));
    }
    currentBinding++;

    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::storageBuffer(SharedBuffer buffer)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(buffer);

    auto *info = infos.alloc<vk::DescriptorBufferInfo>();
    info->setBuffer(buffer->handle());
    info->setOffset(0);
    info->setRange(VK_WHOLE_SIZE);

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eStorageBuffer)
            .setPBufferInfo(info));

    currentBinding++;
    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::uniformBuffer(SharedBuffer buffer)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(buffer);

    auto *info = infos.alloc<vk::DescriptorBufferInfo>();
    info->setBuffer(buffer->handle());
    info->setOffset(0);
    info->setRange(VK_WHOLE_SIZE);

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eUniformBuffer)
            .setPBufferInfo(info));

    currentBinding++;
    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::storageBuffers(
    const std::vector<SharedBuffer> &buffers)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();

    if (!buffers.empty()) {
        auto *info = infos.alloc<vk::DescriptorBufferInfo>(buffers.size());

        for (auto i = 0u; i < buffers.size(); i++) {
            auto const &buf = buffers[i];
            res.push_back(buf);

            info[i].setBuffer(buf->handle());
            info[i].setOffset(0);
            info[i].setRange(VK_WHOLE_SIZE);
        }

        writes.push_back(
            vk::WriteDescriptorSet()
                .setDstSet(set->handle())
                .setDstBinding(currentBinding)
                .setDescriptorCount(buffers.size())
                .setDescriptorType(vk::DescriptorType::eStorageBuffer)
                .setPBufferInfo(info));
    }
    currentBinding++;

    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::uniformBuffers(
    const std::vector<SharedBuffer> &buffers)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();

    if (!buffers.empty()) {
        auto *info = infos.alloc<vk::DescriptorBufferInfo>(buffers.size());

        for (auto i = 0u; i < buffers.size(); i++) {
            auto const &buf = buffers[i];
            res.push_back(buf);

            info[i].setBuffer(buf->handle());
            info[i].setOffset(0);
            info[i].setRange(VK_WHOLE_SIZE);
        }

        writes.push_back(
            vk::WriteDescriptorSet()
                .setDstSet(set->handle())
                .setDstBinding(currentBinding)
                .setDescriptorCount(buffers.size())
                .setDescriptorType(vk::DescriptorType::eUniformBuffer)
                .setPBufferInfo(info));
    }
    currentBinding++;

    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::storageImage(SharedImageView view)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(view);

    auto *info = infos.alloc<vk::DescriptorImageInfo>();
    info->imageView = view->handle();
    info->imageLayout = vk::ImageLayout::eGeneral;

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eStorageImage)
            .setPImageInfo(info));

    currentBinding++;
    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::accelerationStructure(
    SharedTopLevelAccelerationStructure tlas)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(tlas);

    auto *handles = infos.alloc<vk::AccelerationStructureNV>();
    handles[0] = tlas->handle();

    auto *info =
        infos.alloc<vk::WriteDescriptorSetAccelerationStructureNV>();
    info->setAccelerationStructureCount(1);
    info->setPAccelerationStructures(handles);

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eAccelerationStructureNV)
            .setPNext(info));

    currentBinding++;
    return *this;
}

DescriptorSetWriter &
DescriptorSetWriter::inputAttachment(SharedImageView view,
                                     vk::ImageLayout layout)
{
    auto &res = set->mBindingResources[currentBinding];
    res.clear();
    res.push_back(view);

    auto *info = infos.alloc<vk::DescriptorImageInfo>();
    info->setImageView(view->handle());
    info->setImageLayout(layout);

    writes.push_back(
        vk::WriteDescriptorSet()
            .setDstSet(set->handle())
            .setDstBinding(currentBinding)
            .setDescriptorCount(1)
            .setDescriptorType(vk::DescriptorType::eInputAttachment)
            .setPImageInfo(info));

    currentBinding++;
    return *this;
}
DescriptorSetWriter &
DescriptorSetWriter::inputAttachmentColor(SharedImageView view)
{
    return inputAttachment(view, vk::ImageLayout::eShaderReadOnlyOptimal);
}

DescriptorSetWriter &
DescriptorSetWriter::inputAttachmentDepth(SharedImageView view)
{
    return inputAttachment(view,
                           vk::ImageLayout::eDepthStencilReadOnlyOptimal);
}

DescriptorSetWriter::~DescriptorSetWriter()
{
    set->mDevice->handle().updateDescriptorSets(writes, {});
}

} // namespace lava
