#include "GraphicsPipeline.hh"
#include <lava/objects/Device.hh>
#include <lava/objects/PipelineLayout.hh>

namespace lava
{

GraphicsPipeline::GraphicsPipeline(const GraphicsPipelineCreateInfo &info)
    : mCreateInfo(info)
{
    assert(mCreateInfo.mLayout &&
           "GraphicsPipeline requires a PipelineLayout "
           "to be set in its CreateInfo.");
    mDevice = mCreateInfo.mLayout->device();
    mHandle = static_cast<vk::Pipeline&&>(mDevice->handle().createGraphicsPipeline({}, mCreateInfo));
}

GraphicsPipeline::~GraphicsPipeline()
{
    mDevice->handle().destroyPipeline(mHandle);
}
} // namespace lava
