#include "ShaderModule.hh"
#include <lava/objects/Device.hh>

namespace lava
{

ShaderModule::ShaderModule(const SharedDevice &device, const char *begin,
                           const char *end)
    : mDevice(device)

{
    vk::ShaderModuleCreateInfo info;
    info.pCode = reinterpret_cast<uint32_t const *>(begin);
    info.codeSize = size_t(end - begin);

    mHandle = mDevice->handle().createShaderModule(info);
}

ShaderModule::~ShaderModule()
{
    mDevice->handle().destroyShaderModule(mHandle);
}
} // namespace lava
