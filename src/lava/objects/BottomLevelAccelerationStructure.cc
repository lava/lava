#include "BottomLevelAccelerationStructure.hh"
#include <lava/createinfos/Buffers.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/CommandBuffer.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/MemoryChunk.hh>
#include <lava/objects/Queue.hh>
#include <lava/objects/Suballocator.hh>

namespace lava
{

BottomLevelAccelerationStructure::BottomLevelAccelerationStructure(
    BottomLevelAccelerationStructureCreateInfo info,
    const lava::SharedDevice &device)
    : mCreateInfo(std::move(info)), mDevice(device)
{

    mHandle =
        mDevice->handle().createAccelerationStructureNV(mCreateInfo.info());

    vk::AccelerationStructureMemoryRequirementsInfoNV memInfo;
    memInfo.setAccelerationStructure(mHandle);

    auto reqs =
        mDevice->handle().getAccelerationStructureMemoryRequirementsNV(
            memInfo);
    mMemory = device->suballocator()->allocate(reqs.memoryRequirements,
                                               lava::MemoryType::VRAM);
    auto bind = vk::BindAccelerationStructureMemoryInfoNV()
                    .setAccelerationStructure(mHandle)
                    .setMemory(mMemory->handle())
                    .setMemoryOffset(mMemory->offset());

    device->handle().bindAccelerationStructureMemoryNV(bind);

    device->handle().getAccelerationStructureHandleNV(
        handle(), sizeof(mDeviceHandle), &mDeviceHandle);
}

BottomLevelAccelerationStructure::~BottomLevelAccelerationStructure()
{
    mDevice->handle().destroyAccelerationStructureNV(mHandle);
}

void
BottomLevelAccelerationStructure::build()
{
    auto buf = mDevice->createBuffer(raytracingBuffer(scratchSize()));
    buf->realizeVRAM();

    build(buf);
}

void
BottomLevelAccelerationStructure::build(RecordingCommandBuffer &cmd)
{
    auto buf = mDevice->createBuffer(raytracingBuffer(scratchSize()));
    buf->realizeVRAM();

    build(buf, cmd);
}

void
BottomLevelAccelerationStructure::build(const SharedBuffer &scratchBuffer)
{
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    build(scratchBuffer, cmd);
}

void
BottomLevelAccelerationStructure::build(const SharedBuffer &scratchBuffer,
                                        RecordingCommandBuffer &cmd)
{
    cmd->buildAccelerationStructureNV(mCreateInfo.info().info, {}, 0, false,
                                      mHandle, {}, scratchBuffer->handle(),
                                      0);
}

uint32_t
BottomLevelAccelerationStructure::scratchSize() const
{
    vk::AccelerationStructureMemoryRequirementsInfoNV info;
    info.setAccelerationStructure(mHandle);
    info.setType(
        vk::AccelerationStructureMemoryRequirementsTypeNV::eBuildScratch);
    return mDevice->handle()
        .getAccelerationStructureMemoryRequirementsNV(info)
        .memoryRequirements.size;
}

} // namespace lava
