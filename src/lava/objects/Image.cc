#include "Image.hh"

#include <lava/common/FormatInfo.hh>
#include <lava/common/LayoutInfo.hh>
#include <lava/common/log.hh>
#include <lava/createinfos/Buffers.hh>
#include <lava/createinfos/Images.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/MemoryChunk.hh>
#include <lava/objects/Suballocator.hh>

namespace lava
{

namespace
{

vk::FormatFeatureFlags
usageToFeatures(vk::ImageUsageFlags usage)
{
    vk::FormatFeatureFlags result;
    if (usage & vk::ImageUsageFlagBits::eColorAttachment) {
        result |= vk::FormatFeatureFlagBits::eColorAttachment |
                  vk::FormatFeatureFlagBits::eColorAttachmentBlend;
    }
    if (usage & vk::ImageUsageFlagBits::eDepthStencilAttachment) {
        result |= vk::FormatFeatureFlagBits::eDepthStencilAttachment;
    }
    if (usage & vk::ImageUsageFlagBits::eInputAttachment) {
        result |= vk::FormatFeatureFlagBits::eColorAttachment;
    }
    if (usage & vk::ImageUsageFlagBits::eSampled) {
        result |= vk::FormatFeatureFlagBits::eSampledImage;
    }
    if (usage & vk::ImageUsageFlagBits::eStorage) {
        result |= vk::FormatFeatureFlagBits::eStorageImage;
    }
    if (usage & vk::ImageUsageFlagBits::eTransferDst) {
         // this flag is included in core Vulkan 1.1
         result |= vk::FormatFeatureFlagBits::eTransferDst;
    }
    if (usage & vk::ImageUsageFlagBits::eTransferSrc) {
         // this flag is included in core Vulkan 1.1
         result |= vk::FormatFeatureFlagBits::eTransferSrc;
    }
    return result;
}
} // namespace

Image::Image(SharedDevice const &device, vk::ImageCreateInfo info,
             vk::ImageViewType type)
    : mType(type), mCreateInfo(info), mDevice(device)
{
    auto features = usageToFeatures(info.usage);

    auto props = mDevice->physical().getFormatProperties(info.format);
    auto canUseOptimal =
        ((props.optimalTilingFeatures & features) == features);
    auto canUseLinear =
        ((props.linearTilingFeatures & features) == features);

    if (!canUseOptimal && info.tiling == vk::ImageTiling::eOptimal) {
        if (canUseLinear) {
            lava::debug() << //
                "The format selected for this image does not support "
                "optimal "
                "tiling for the required features, falling back to linear "
                "tiling.\n"
                "In order to optimize performance, use a format that "
                "supports "
                "optimal tiling (e.g. use RGBA instead of RGB textures for "
                "attachments)\n"
                "You can supress this warning by setting the tiling to "
                "linear "
                "yourself.";
            info.tiling = vk::ImageTiling::eLinear;
        } else {
            lava::error()
                << "The format selected can't provide the requested "
                   "usages. Try another format (e.g. use RGBA "
                   "instead of RGB for attachments)";
        }
    } else if (!canUseLinear && info.tiling == vk::ImageTiling::eLinear) {
        if (canUseOptimal) {
            lava::debug() << //
                "The format selected for this image does not support "
                "linear "
                "tiling for the required features, falling back to optimal "
                "tiling.\n"
                "You can supress this warning by setting the tiling to "
                "optimal "
                "yourself.";
            info.tiling = vk::ImageTiling::eOptimal;
        } else {
            lava::error()
                << "The format selected can't provide the requested "
                   "usages. Try another format (e.g. use RGBA "
                   "instead of RGB for attachments)";
        }
    }

    mHandle = mDevice->handle().createImage(mCreateInfo);
}

Image::Image(const SharedDevice &device, vk::ImageCreateInfo approx,
             vk::Image unowned, vk::ImageViewType type)
    : mType(type), mCreateInfo(approx), mUnowned(true), mDevice(device)
{
    mHandle = unowned;
}

void
Image::setDataVRAM(const void *data, RecordingCommandBuffer &cmd)
{
    realizeVRAM();

    auto num_bytes =
        width() * height() * depth() * bytePerPixel(mCreateInfo.format);
    if (mMemory->mappable()) {
        changeLayout(vk::ImageLayout::eGeneral, cmd);
        auto mapped = mMemory->map();
        memcpy(mapped.data(), data, num_bytes);
    } else {
        auto staging = prepStagingBuffer(num_bytes);
        staging->setDataRAM(data, num_bytes);
        copyFrom(staging, cmd);
    }
}

void
Image::setDataVRAM(void const *data)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::setDataVRAM()");
    if (!mMemory)
        realizeVRAM();

    if (mMemory->mappable()) {
        auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
        changeLayout(vk::ImageLayout::eGeneral, cmd);

        auto mapped = mMemory->map();
        memcpy(mapped.data(), data, levelBytes());
    } else {
        auto staging = prepStagingBuffer(levelBytes());
        staging->setDataRAM(data, levelBytes());

        auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
        copyFrom(staging, cmd);
    }
}

void
Image::setDataRAM(const void *data)
{
    if (!mMemory)
        realizeRAM();

    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    changeLayout(vk::ImageLayout::eGeneral, cmd);

    auto num_bytes =
        width() * height() * depth() * bytePerPixel(mCreateInfo.format);
    auto mapped = mMemory->map();
    memcpy(mapped.data(), data, num_bytes);
}

void
Image::getData(void *data, int level)
{
    assert(mMemory && "Image needs to be realized to get data.");

    auto staging = prepStagingBuffer(levelBytes(level));

    copyTo(staging, level);
    mDevice->graphicsQueue().catchUp(0);
    staging->getData(data);
}

void
Image::realizeVRAM()
{
    initMemory(MemoryType::VRAM);
}

void
Image::realizeRAM()
{
    initMemory(MemoryType::RAM);
}

void
Image::realizeAttachment()
{
    realizeVRAM();
}

void
Image::copyFrom(const SharedBuffer &buffer)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::copyFrom()");
    auto cmd = mDevice->transferQueue().beginCommandBuffer();
    copyFrom(buffer, cmd);
}

void
Image::copyFrom(const SharedBuffer &buffer, RecordingCommandBuffer &cmd)
{
    copyFrom(buffer, 0, cmd);
}

void
Image::copyFrom(SharedBuffer const& buffer, uint64_t bufferOffset , RecordingCommandBuffer& cmd)
{
    changeLayout(vk::ImageLayout::eTransferDstOptimal, cmd);

    vk::BufferImageCopy copy;
    copy.bufferOffset = bufferOffset;
    copy.bufferRowLength = width();
    copy.bufferImageHeight = height();
    copy.imageExtent = mCreateInfo.extent;

    copy.imageSubresource.aspectMask = aspectsOf(format());
    copy.imageSubresource.layerCount = mCreateInfo.arrayLayers;
    copy.imageSubresource.mipLevel = 0;

    cmd->copyBufferToImage(buffer->handle(), handle(),
                           vk::ImageLayout::eTransferDstOptimal, {copy});
    cmd.attachResource(buffer);
}

void
Image::copyTo(const SharedBuffer &buffer, int layer)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::copyTo()");
    auto cmd = mDevice->transferQueue().beginCommandBuffer();
    copyTo(buffer, cmd, layer);
}

void
Image::copyTo(const SharedBuffer &buffer, RecordingCommandBuffer &cmd,
              int level)
{
    assert(mMemory &&
           "The Image needs to be realized to copy the data from it.");

    vk::BufferImageCopy copy;
    copy.setBufferRowLength(
        std::max(1u, mCreateInfo.extent.width >> level));
    copy.setBufferImageHeight(
        std::max(1u, mCreateInfo.extent.height >> level));
    copy.setImageExtent(mCreateInfo.extent);

    auto &sub = copy.imageSubresource;
    sub.aspectMask = aspectsOf(mCreateInfo.format);
    sub.baseArrayLayer = 0;
    sub.layerCount = mCreateInfo.arrayLayers;
    sub.mipLevel = uint32_t(level);

    cmd->copyImageToBuffer(handle(), vk::ImageLayout::eTransferSrcOptimal,
                           buffer->handle(), {copy});

    cmd.attachResource(shared_from_this());
}

void
Image::blitTo(const SharedImage &other)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::blitTo()");
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    blitTo(other, cmd);
}

void
Image::blitTo(const SharedImage &other, RecordingCommandBuffer &cmd)
{
    blitTo(other, 0, mCreateInfo.arrayLayers,
           vk::ImageAspectFlagBits::eColor, vk::ImageAspectFlagBits::eColor,
           cmd);
}

void
Image::blitTo(const SharedImage &other, uint32_t baseLayer,
              uint32_t layerCount, vk::ImageAspectFlags srcAspects,
              vk::ImageAspectFlags dstAspects, RecordingCommandBuffer &cmd)
{
    other->changeLayout(vk::ImageLayout::eTransferDstOptimal, cmd);

    vk::ImageSubresourceLayers srcSub;
    srcSub.setAspectMask(srcAspects)
        .setBaseArrayLayer(baseLayer)
        .setLayerCount(layerCount)
        .setMipLevel(0);

    vk::ImageSubresourceLayers dstSub;
    dstSub.setAspectMask(dstAspects)
        .setBaseArrayLayer(baseLayer)
        .setLayerCount(layerCount)
        .setMipLevel(0);

    auto dstExt = other->mCreateInfo.extent;
    auto srcExt = mCreateInfo.extent;
    vk::ImageBlit region;
    region.setSrcSubresource(srcSub)
        .setDstSubresource(dstSub)
        .setSrcOffsets(
            {{vk::Offset3D{},
              vk::Offset3D(srcExt.width, srcExt.height, srcExt.depth)}})
        .setDstOffsets(
            {{vk::Offset3D{},
              vk::Offset3D(dstExt.width, dstExt.height, dstExt.depth)}});

    auto filter = vk::Filter::eLinear;
    // VUID-vkCmdBlitImage-srcImage-00232: 
    // If the format of srcImage is a depth, stencil, or depth stencil
    // then filter must be VK_FILTER_NEAREST
    if ((srcAspects & vk::ImageAspectFlagBits::eDepth) ||
        (srcAspects & vk::ImageAspectFlagBits::eStencil) ||
        (dstAspects & vk::ImageAspectFlagBits::eDepth) ||
        (dstAspects & vk::ImageAspectFlagBits::eStencil))
    {
        filter = vk::Filter::eNearest;
    }
    cmd.buffer()->handle().blitImage(
        handle(), vk::ImageLayout::eTransferSrcOptimal, other->handle(),
        vk::ImageLayout::eTransferDstOptimal, {region},
        filter);
    cmd.attachResource(shared_from_this());
}

void
Image::blitTo(const SharedImage& other, RecordingCommandBuffer& cmd,
              vk::ImageBlit region)
{
    other->changeLayout(vk::ImageLayout::eTransferDstOptimal, cmd);

    cmd.buffer()->handle().blitImage(
        handle(), vk::ImageLayout::eTransferSrcOptimal, other->handle(),
        vk::ImageLayout::eTransferDstOptimal, { region },
        vk::Filter::eLinear);
    cmd.attachResource(shared_from_this());
}

void
Image::blitTo(SharedImage const& other, vk::ImageAspectFlags srcAspects, vk::ImageAspectFlags dstAspects,
    const std::vector<vk::ImageBlit>& regions, RecordingCommandBuffer& cmd)
{
    other->changeLayout(vk::ImageLayout::eTransferDstOptimal, cmd);

    auto filter = vk::Filter::eLinear;
    // VUID-vkCmdBlitImage-srcImage-00232: 
    // If the format of srcImage is a depth, stencil, or depth stencil
    // then filter must be VK_FILTER_NEAREST
    if ((srcAspects & vk::ImageAspectFlagBits::eDepth) ||
        (srcAspects & vk::ImageAspectFlagBits::eStencil) ||
        (dstAspects & vk::ImageAspectFlagBits::eDepth) ||
        (dstAspects & vk::ImageAspectFlagBits::eStencil))
    {
        filter = vk::Filter::eNearest;
    }

    cmd.buffer()->handle().blitImage(
        handle(), vk::ImageLayout::eTransferSrcOptimal, other->handle(),
        vk::ImageLayout::eTransferDstOptimal, regions,
        filter);
    cmd.attachResource(shared_from_this());
}

void
Image::copyFrom(const SharedImage &other)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::copyFrom()");
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    copyFrom(other, cmd);
}

void
Image::copyFrom(const SharedImage &other, RecordingCommandBuffer &cmd)
{
    changeLayout(vk::ImageLayout::eTransferDstOptimal, cmd);

    vk::ImageCopy copy;
    copy.srcSubresource.aspectMask = aspectsOf(mCreateInfo.format);
    copy.srcSubresource.layerCount = mCreateInfo.arrayLayers;
    copy.dstSubresource = copy.srcSubresource;
    copy.extent = mCreateInfo.extent;

    cmd->copyImage(other->handle(), vk::ImageLayout::eTransferSrcOptimal,
                   handle(), vk::ImageLayout::eTransferDstOptimal, {copy});
}

void
Image::copyLayerFrom(SharedImage const &other, uint32_t layer, RecordingCommandBuffer &cmd)
{
	changeLayout(vk::ImageLayout::eTransferDstOptimal, cmd);

	vk::ImageCopy copy;
	copy.srcSubresource.aspectMask = aspectsOf(mCreateInfo.format);
	copy.srcSubresource.baseArrayLayer = layer;
	copy.srcSubresource.layerCount = 1;
	copy.dstSubresource = copy.srcSubresource;
	copy.extent = mCreateInfo.extent;

	cmd->copyImage(other->handle(), vk::ImageLayout::eTransferSrcOptimal,
		handle(), vk::ImageLayout::eTransferDstOptimal, { copy });
}

void
Image::changeLayout(vk::ImageLayout from, vk::ImageLayout to)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::changeLayout()");
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    changeLayout(from, to, cmd);
}

void
Image::changeLayout(vk::ImageLayout from, vk::ImageLayout to,
                    RecordingCommandBuffer &cmd)
{
    vk::ImageMemoryBarrier barrier;
    barrier.setOldLayout(from)
        .setNewLayout(to)
        .setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
        .setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
        .setSrcAccessMask(flagsForLayout(from))
        .setDstAccessMask(flagsForLayout(to))
        .setImage(mHandle)
        .subresourceRange.setAspectMask(aspectsOf(mCreateInfo.format))
        .setBaseMipLevel(0)
        .setBaseArrayLayer(0)
        .setLayerCount(mCreateInfo.arrayLayers)
        .setLevelCount(mCreateInfo.mipLevels);

    cmd->pipelineBarrier(stageForLayout(from), stageForLayout(to), {}, 0,
                         nullptr, 0, nullptr, 1, &barrier);
}

void
Image::changeLayout(vk::ImageLayout to, RecordingCommandBuffer &cmd)
{
    changeLayout(vk::ImageLayout::eUndefined, to, cmd);
}

void
Image::changeOwner(Queue *oldOwner, vk::ImageLayout oldLayout,
                   Queue *newOwner, vk::ImageLayout newLayout)
{
    RecordingCommandBuffer::convenienceBufferCheck("Image::changeOwner()");
    if (oldOwner->family() == newOwner->family())
        return changeLayout(oldLayout, newLayout);
}

void
Image::changeOwner(RecordingCommandBuffer &oldBuffer,
                   vk::ImageLayout oldLayout,
                   RecordingCommandBuffer &newBuffer,
                   vk::ImageLayout newLayout)
{
    auto oldQueue = oldBuffer.buffer()->queue();
    auto newQueue = newBuffer.buffer()->queue();
    if (oldQueue->family() == newQueue->family())
        return changeLayout(oldLayout, newLayout, newBuffer);
}

void
Image::generateMipmaps()
{
    RecordingCommandBuffer::convenienceBufferCheck(
        "Image::generateMipmaps()");
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    generateMipmaps(cmd);
}

void
Image::generateMipmaps(RecordingCommandBuffer &cmd)
{
    auto props =
        mDevice->physical().getFormatProperties(mCreateInfo.format);
    assert(props.optimalTilingFeatures &
               vk::FormatFeatureFlagBits::eBlitDst &&
           "Need to be able to blit to this image format.");
    assert(props.optimalTilingFeatures &
               vk::FormatFeatureFlagBits::eBlitSrc &&
           "Need to be able to blit to this image format.");
    (void)props;

    auto or1 = [](int32_t x) { return std::max(x, 1); };

    for (uint32_t level = 1; level < mCreateInfo.mipLevels; level++) {
        // change layout of target level
        vk::ImageMemoryBarrier barrier;
        barrier.oldLayout = vk::ImageLayout::eUndefined;
        barrier.newLayout = vk::ImageLayout::eTransferDstOptimal;
        barrier.srcAccessMask = flagsForLayout(barrier.oldLayout);
        barrier.dstAccessMask = flagsForLayout(barrier.newLayout);
        barrier.image = mHandle;
        barrier.subresourceRange.aspectMask = aspectsOf(mCreateInfo.format);
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.baseMipLevel = level;
        barrier.subresourceRange.layerCount = mCreateInfo.arrayLayers;
        barrier.subresourceRange.levelCount = 1;

        cmd->pipelineBarrier(vk::PipelineStageFlagBits::eAllCommands,
                             vk::PipelineStageFlagBits::eAllCommands, {}, 0,
                             nullptr, 0, nullptr, 1, &barrier);

        vk::ImageBlit blit;
        blit.srcSubresource.aspectMask = aspectsOf(mCreateInfo.format);
        blit.srcSubresource.layerCount = mCreateInfo.arrayLayers;
        blit.srcSubresource.mipLevel = level - 1;
        blit.srcOffsets[1].x = or1(width() >> (level - 1));
        blit.srcOffsets[1].y = or1(height() >> (level - 1));
        blit.srcOffsets[1].z = or1(depth() >> (level - 1));
        blit.dstSubresource.aspectMask = aspectsOf(mCreateInfo.format);
        blit.dstSubresource.layerCount = mCreateInfo.arrayLayers;
        blit.dstSubresource.mipLevel = level;
        blit.dstOffsets[1].x = or1(width() >> level);
        blit.dstOffsets[1].y = or1(height() >> level);
        blit.dstOffsets[1].z = or1(depth() >> level);

        cmd.buffer()->handle().blitImage(
            handle(), vk::ImageLayout::eTransferSrcOptimal, handle(),
            vk::ImageLayout::eTransferDstOptimal, 1, &blit,
            vk::Filter::eLinear);

        barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal)
            .setNewLayout(vk::ImageLayout::eTransferSrcOptimal);
        cmd->pipelineBarrier(vk::PipelineStageFlagBits::eAllCommands,
                             vk::PipelineStageFlagBits::eAllCommands, {}, 0,
                             nullptr, 0, nullptr, 1, &barrier);
    }
    cmd.attachResource(shared_from_this());
}

SharedImageView
Image::createView()
{
    return createView(vk::ImageSubresourceRange{});
}

SharedImageView
Image::createView(const vk::ImageSubresourceRange &range)
{
    return createView(mType, range);
}

SharedImageView
Image::createView(vk::ImageViewType type)
{
    return createView(type, vk::ImageSubresourceRange{});
}

SharedImageView
Image::createView(vk::ImageViewType type,
                  const vk::ImageSubresourceRange &range)
{
    if (range == vk::ImageSubresourceRange{}) {
        return std::make_shared<ImageView>(shared_from_this(), type);
    }
    return std::make_shared<ImageView>(shared_from_this(), type, range);
}

void
Image::initMemory(MemoryType type)
{
    if (mMemory)
        return;
    assert(mHandle != vk::Image{} &&
           "Cannot realize an Image that doesn't have a size yet.");

    vk::ImageMemoryRequirementsInfo2 info(mHandle);
    vk::MemoryDedicatedRequirements dedicatedReq;
    vk::MemoryRequirements2 req;
    req.pNext = &dedicatedReq;
    mDevice->handle().getImageMemoryRequirements2(&info, &req);

    if (dedicatedReq.prefersDedicatedAllocation ||
        dedicatedReq.requiresDedicatedAllocation) {
        mMemory = mDevice->suballocator()->allocateDedicated(
            req.memoryRequirements, type, mHandle);
    } else {
        mMemory =
            mDevice->suballocator()->allocate(req.memoryRequirements, type);
    }

    assert(mMemory->offset() % req.memoryRequirements.alignment == 0);
    mMemory->bindToImage(mHandle);
}

void
Image::changeOwnerImpl(Queue *oldOwner, RecordingCommandBuffer &oldBuffer,
                       vk::ImageLayout oldLayout, Queue *newOwner,
                       RecordingCommandBuffer &newBuffer,
                       vk::ImageLayout newLayout)
{
    vk::ImageMemoryBarrier barrier;
    barrier.setOldLayout(oldLayout)
        .setNewLayout(newLayout)
        .setSrcQueueFamilyIndex(oldOwner->family())
        .setDstQueueFamilyIndex(newOwner->family())
        .setSrcAccessMask(flagsForLayout(oldLayout))
        .setDstAccessMask(flagsForLayout(newLayout))
        .setImage(mHandle)
        .subresourceRange.setAspectMask(aspectsOf(mCreateInfo.format))
        .setBaseMipLevel(0)
        .setBaseArrayLayer(0)
        .setLayerCount(mCreateInfo.arrayLayers)
        .setLevelCount(mCreateInfo.mipLevels);

    oldBuffer->pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe,
                               vk::PipelineStageFlagBits::eTopOfPipe, {}, 0,
                               nullptr, 0, nullptr, 1, &barrier);
    newBuffer->pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe,
                               vk::PipelineStageFlagBits::eTopOfPipe, {}, 0,
                               nullptr, 0, nullptr, 1, &barrier);
}

void
Image::changeLayout(vk::ImageLayout to)
{
    changeLayout(vk::ImageLayout::eUndefined, to);
}

Image::~Image()
{
    if (mUnowned)
        return;

    auto dev = mDevice->handle();
    dev.destroyImage(mHandle);
}

size_t
Image::levelPixels(int layer)
{
    return std::max(1u, mCreateInfo.extent.width >> layer) *
           std::max(1u, mCreateInfo.extent.height >> layer) *
           std::max(1u, mCreateInfo.extent.depth >> layer) *
           std::max(1u, mCreateInfo.arrayLayers);
}

size_t
Image::levelBytes(int layer)
{
    return levelPixels(layer) * bytePerPixel(mCreateInfo.format);
}

SharedBuffer
Image::prepStagingBuffer(size_t size)
{
    if (mStagingBuffer && mStagingBuffer->size() >= size)
        return mStagingBuffer;

    auto buf = lava::stagingBuffer(size)
                   .addUsage(vk::BufferUsageFlagBits::eTransferDst)
                   .create(mDevice);
    buf->realizeRAM();

    mStagingBuffer = mKeepStagingBuffer ? buf : nullptr;
    return buf;
}
} // namespace lava
