#include "DescriptorSetLayout.hh"
#include <lava/createinfos/DescriptorPoolCreateInfo.hh>
#include <lava/objects/DescriptorPool.hh>
#include <lava/objects/Device.hh>

namespace lava
{

DescriptorSetLayout::DescriptorSetLayout(
    const SharedDevice &device, const DescriptorSetLayoutCreateInfo &info,
    uint32_t poolSize)
    : mDevice(device), mCreateInfo(info)
{

    mHandle = mDevice->handle().createDescriptorSetLayout(mCreateInfo);
    if (poolSize) {
        std::unordered_map<vk::DescriptorType, uint32_t> sizes;
        for (auto b : info.mBindings) {
            sizes[b.descriptorType] += b.descriptorCount;
        }

        DescriptorPoolCreateInfo pinfo;
        pinfo.allowFreeing();
        for (auto s : sizes) {
            pinfo.addSize(s.first, s.second * poolSize);
        }
        pinfo.setMaxSets(poolSize);

        mPool = mDevice->createDescriptorPool(pinfo);
    }
}

DescriptorSetLayout::~DescriptorSetLayout()
{
    mDevice->handle().destroyDescriptorSetLayout(mHandle);
}

SharedDescriptorSet
DescriptorSetLayout::createDescriptorSet()
{
    return mPool->createDescriptorSet(shared_from_this());
}

} // namespace lava
