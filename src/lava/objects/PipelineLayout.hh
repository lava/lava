#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/PipelineLayoutCreateInfo.hh>
#include <lava/createinfos/fwd.hh>
#include <lava/fwd.hh>

namespace lava
{

class PipelineLayout : public std::enable_shared_from_this<PipelineLayout>
{
  public:
    LAVA_NON_MOVABLE(PipelineLayout);

    PipelineLayout(
        SharedDevice const &device,
        std::vector<SharedDescriptorSetLayout> const &descriptors = {},
        std::vector<vk::PushConstantRange> const &pushConstants = {});
    ~PipelineLayout();

    vk::PipelineLayout handle() const { return mHandle; }
    SharedDevice const &device() const { return mDevice; }

    PipelineLayoutCreateInfo const &createInfo() const
    {
        return mCreateInfo;
    }

  protected:
    SharedDevice mDevice;
    std::vector<SharedDescriptorSetLayout> mDescriptors;
    std::vector<vk::PushConstantRange> mPushConstants;
    std::vector<vk::DescriptorSetLayout> mDescriptorHandles;
    PipelineLayoutCreateInfo mCreateInfo;
    vk::PipelineLayout mHandle;
};
} // namespace lava
