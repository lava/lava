#include "Sampler.hh"
#include <lava/objects/Device.hh>

namespace lava
{
Sampler::Sampler(SharedDevice const &device,
                 vk::SamplerCreateInfo const &info)
    : mDevice(device), mInfo(info)
{
    mHandle = mDevice->handle().createSampler(mInfo);
}

Sampler::~Sampler()
{
    if (mHandle)
        mDevice->handle().destroySampler(mHandle);
}
} // namespace lava
