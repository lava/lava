#include "RayTracingPipeline.hh"
#include <lava/createinfos/Buffers.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/PipelineLayout.hh>

namespace lava
{

RayTracingPipeline::RayTracingPipeline(
    const RayTracingPipelineCreateInfo &info)
    : mCreateInfo(info)
{

    assert(mCreateInfo.layout() &&
           "GraphicsPipeline requires a PipelineLayout "
           "to be set in its CreateInfo.");
    mDevice = mCreateInfo.layout()->device();
    mHandle = static_cast<vk::Pipeline&&>(mDevice->handle().createRayTracingPipelineNV(
        nullptr, mCreateInfo.info()));

    mShaderBindingTable = mDevice->createBuffer(raytracingBuffer());

    mRaytracingProperties =
        mDevice->physical()
            .getProperties2<vk::PhysicalDeviceProperties2,
                            vk::PhysicalDeviceRayTracingPropertiesNV>()
            .get<vk::PhysicalDeviceRayTracingPropertiesNV>();

    std::vector<char> groupData(
        mRaytracingProperties.shaderGroupHandleSize *
        mCreateInfo.groups().size());
    mDevice->handle().getRayTracingShaderGroupHandlesNV(
        mHandle, 0, mCreateInfo.groups().size(), groupData.size(),
        groupData.data());

    mShaderBindingTable->setDataVRAM(groupData);
}

RayTracingPipeline::~RayTracingPipeline()
{
    mDevice->handle().destroyPipeline(mHandle);
}

void
RayTracingPipeline::bind(RecordingCommandBuffer &cmd)
{
    cmd->bindPipeline(vk::PipelineBindPoint::eRayTracingNV, handle());
}

void
RayTracingPipeline::trace(RecordingCommandBuffer &cmd, uint32_t width,
                          uint32_t height, uint32_t depth, int raygen,
                          RayTracingPipeline::OffsetStride miss,
                          RayTracingPipeline::OffsetStride hit)
{
    auto sbt = shaderBindingTable()->handle();
    auto handlesize = mRaytracingProperties.shaderGroupHandleSize;

    int genoffset = raygen + mCreateInfo.firstRaygenIndex();
    int hitoffset = hit.offset + mCreateInfo.firstHitIndex();
    int missoffset = miss.offset + mCreateInfo.firstMissIndex();

    cmd->traceRaysNV(                                           //
        sbt, genoffset * handlesize,                            //
        sbt, missoffset * handlesize, miss.stride * handlesize, //
        sbt, hitoffset * handlesize, hit.stride * handlesize,   //
        {}, 0, 0,                                               //
        width, height, depth);
}

} // namespace lava
