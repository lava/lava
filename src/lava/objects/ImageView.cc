#include "ImageView.hh"
#include <lava/common/FormatInfo.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>

namespace lava
{

ImageView::ImageView(const SharedImage &of, vk::ImageViewType type)
    : mImage(of)
{
    auto imgInfo = of->mCreateInfo;

    vk::ImageSubresourceRange range;
    range.setAspectMask(aspectsOf(imgInfo.format))
        .setLayerCount(imgInfo.arrayLayers)
        .setLevelCount(imgInfo.mipLevels);

    mCreateInfo.setViewType(type)
        .setFormat(imgInfo.format)
        .setSubresourceRange(range)
        .setImage(of->handle());

    mDevice = of->device().get();
    mHandle = mDevice->handle().createImageView(mCreateInfo);
}

ImageView::ImageView(const SharedImage &of, vk::ImageViewType type,
                     vk::ImageSubresourceRange range)
    : mImage(of)
{
    auto imgInfo = of->mCreateInfo;

    mCreateInfo.setViewType(type)
        .setFormat(imgInfo.format)
        .setSubresourceRange(range)
        .setImage(of->handle());

    mDevice = of->device().get();
    mHandle = mDevice->handle().createImageView(mCreateInfo);
}

ImageView::ImageView(Device *device, vk::ImageViewCreateInfo info)
{
    mDevice = device;
    mCreateInfo = info;

    mHandle = mDevice->handle().createImageView(mCreateInfo);
}

ImageView::~ImageView() { mDevice->handle().destroyImageView(mHandle); }
} // namespace lava
