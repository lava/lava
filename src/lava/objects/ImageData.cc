#include "ImageData.hh"
#include <fstream>
#include <lava/common/GlLikeFormats.hh>
#include <lava/common/log.hh>
#include <lava/common/str_utils.hh>
#include <lava/createinfos/Images.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/Queue.hh>
#include <lodepng/lodepng.h>
#include <stb/stb_image.h>

namespace lava
{

ImageData::ImageData() {}

SharedImageData
ImageData::loadWithLodepng(const std::string &filename, const std::string &,
                           ImageData::ColorSpace colorspace)
{
    auto result = std::make_shared<ImageData>();

    std::vector<unsigned char> data;
    unsigned width, height;

    auto errCode = lodepng::decode(data, width, height, filename);
    if (errCode) {
        error() << "Error loading LodePNG texture data from " << filename;
        error() << "  decoder error " << errCode << ": "
                << lodepng_error_text(errCode);
        return nullptr;
    }
    result->mExtent = vk::Extent3D{width, height, 1};
    result->mLayers = 1;
    result->mMipLevels = 1;
    result->mGenMipmaps = true;

    result->mBytes.resize(data.size());
    copy(begin(data), end(data), result->mBytes.begin());

    result->mFormat = (colorspace == Linear) ? vk::Format::eR8G8B8A8Unorm
                                             : vk::Format::eR8G8B8A8Srgb;

    return result;
}

SharedImageData
ImageData::loadWithSTB(const std::string &filename, const std::string &,
                       ImageData::ColorSpace colorspace)
{
    auto result = std::make_shared<ImageData>();
    int width, height, channels;

    if (stbi_is_hdr(filename.c_str())) {
        float *data = stbi_loadf(filename.c_str(), &width, &height,
                                 &channels, STBI_rgb_alpha);
        auto n_bytes = 32 * width * height;
        result->mBytes.resize(n_bytes);
        copy(reinterpret_cast<char *>(data),
             reinterpret_cast<char *>(data) + n_bytes,
             result->mBytes.begin());
        result->mFormat = vk::Format::eR32G32B32A32Sfloat;
        stbi_image_free(data);
    } else {
        unsigned char *data = stbi_load(filename.c_str(), &width, &height,
                                        &channels, STBI_rgb_alpha);
        auto n_bytes = 4 * width * height;
        result->mBytes.resize(n_bytes);
        copy(reinterpret_cast<char *>(data),
             reinterpret_cast<char *>(data) + n_bytes,
             result->mBytes.begin());
        result->mFormat = (colorspace == Linear)
                              ? vk::Format::eR8G8B8A8Unorm
                              : vk::Format::eR8G8B8A8Srgb;
        stbi_image_free(data);
    }
    result->mExtent = vk::Extent3D(width, height, 1);
    result->mLayers = 1;
    result->mMipLevels = 1;
    result->mGenMipmaps = true;

    return result;
}

lava::SharedImageData
lava::ImageData::createFromFile(const std::string &filename,
                                lava::ImageData::ColorSpace colorspace)
{

    auto ending = util::toLower(util::fileEndingOf(filename));
    if (!std::ifstream(filename)) {
        error() << "File " << filename << " not found.";
        return {};
    }

    // lodepng
    if (ending == ".png")
        return loadWithLodepng(filename, ending, colorspace);

    // stb
    if (ending == ".png" ||  //
        ending == ".jpg" ||  //
        ending == ".jpeg" || //
        ending == ".tga" ||  //
        ending == ".bmp" ||  //
        ending == ".psd" ||  //
        ending == ".gif" ||  //
        ending == ".hdr" ||  //
        ending == ".pic" ||  //
        ending == ".ppm" ||  //
        ending == ".pgm")
        return loadWithSTB(filename, ending, colorspace);

    /*
    // GLI
    if (ending == ".ktx" || //
        ending == ".dds")
        return loadWithGLI(filename, ending, colorSpace);

    // binary
    if (ending == ".texdata")
        return loadWithBinary(filename, ending, colorSpace);*/

    error() << "file type of `" << filename << "' not recognized.";
    return nullptr;
}

SharedImageData
ImageData::createFromData(void const *data, uint32_t width, uint32_t height,
                          uint32_t depth, vk::Format format)
{
    using charptr = const char *;
    auto result = std::make_shared<ImageData>();
    result->mBytes = {charptr(data),
                      charptr(data) +
                          width * height * depth * bytePerPixel(format)};
    result->mExtent = vk::Extent3D{width, height, depth};
    result->mFormat = format;
    result->mGenMipmaps = true;
    result->mLayers = 1ul;

    return result;
}

void
ImageData::concatLayers(const void *data, uint32_t layers)
{
    using charptr = const char *;

    assert(mExtent.depth == 1 && "You can't concat layers to a 3d image");
    auto added_bytes = layers * mExtent.width * mExtent.height;

    mBytes.insert(mBytes.end(), charptr(data), charptr(data) + added_bytes);
    mLayers += layers;
}

SharedImage
ImageData::uploadTo(const SharedDevice &device) const
{
    auto cmd = device->graphicsQueue().beginCommandBuffer();
    return this->uploadTo(device, cmd);
}

SharedImage
ImageData::uploadTo(const SharedDevice &device,
                    RecordingCommandBuffer &cmd) const
{
    auto info = lava::texture2DArray(mExtent.width, mExtent.height, mLayers,
                                     mFormat);
    if (mExtent.depth > 1) {
        assert(mLayers == 1 &&
               "A 3D Image cannot have multiple array layers.");
        info.imageInfo.extent.depth = mExtent.depth;
        info.setCombinedType(vk::ImageViewType::e3D);
    } else if (mLayers == 1) {
        info.setCombinedType(vk::ImageViewType::e2D);
    }

    if (!mGenMipmaps)
        info.setMipLevels(mMipLevels);

    auto image = info.create(device);
    image->setDataVRAM(mBytes.data(), cmd);

    if (mGenMipmaps) {
        image->changeLayout(vk::ImageLayout::eTransferDstOptimal,
                            vk::ImageLayout::eTransferSrcOptimal, cmd);
        image->generateMipmaps(cmd);
    }

    return image;
}

} // namespace lava
