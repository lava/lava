#include "MemoryChunk.hh"
#include <lava/objects/Device.hh>
#include <lava/objects/Suballocator.hh>

namespace lava
{

MemoryChunk::MemoryChunk(MemoryChunk &&rhs)
    : mMemory(rhs.mMemory), mOffset(rhs.mOffset), mSize(rhs.mSize),
      mAllocationOffset(rhs.mAllocationOffset), mType(rhs.mType),
      mMappable(rhs.mMappable), mDeallocate(rhs.mDeallocate)
{

    mDevice = std::move(rhs.mDevice);
    rhs.mSize = 0;
}

MemoryChunk::MemoryChunk(vk::DeviceMemory memory,
                         const SharedDevice &device,
                         vk::DeviceSize allocationOffset,
                         vk::DeviceSize offset, vk::DeviceSize size,
                         uint32_t type, bool mappable,
                         const MemoryChunk::Deallocator &dealloc)
    : mMemory(memory), mDevice(device), mOffset(offset), mSize(size),
      mAllocationOffset(allocationOffset), mType(type), mMappable(mappable),
      mDeallocate(dealloc)
{
}

MemoryChunk &
MemoryChunk::operator=(MemoryChunk &&rhs)
{
    std::swap(mMemory, rhs.mMemory);
    std::swap(mDevice, rhs.mDevice);
    std::swap(mAllocationOffset, rhs.mAllocationOffset);
    std::swap(mOffset, rhs.mOffset);
    std::swap(mSize, rhs.mSize);
    std::swap(mType, rhs.mType);
    std::swap(mDeallocate, rhs.mDeallocate);
    std::swap(mMappable, rhs.mMappable);
    return *this;
}

MemoryChunk::~MemoryChunk()
{
    if (mSize == 0)
        return;
    if (mDeallocate) {
        mDeallocate(*this);
    } else {
        mDevice->handle().freeMemory(mMemory);
    }
}

void
MemoryChunk::bindToBuffer(vk::Buffer buffer)
{
    mDevice->handle().bindBufferMemory(buffer, mMemory, mOffset);
}

void
MemoryChunk::bindToImage(vk::Image image)
{
    mDevice->handle().bindImageMemory(image, mMemory, mOffset);
}

MappedMemory::MappedMemory(MappedMemory &&o)
{
    std::swap(mOffset, o.mOffset);
    std::swap(mSize, o.mSize);
    std::swap(mOffset, o.mOffset);
    std::swap(mSize, o.mSize);
    std::swap(mMemory, o.mMemory);
    std::swap(mData, o.mData);
}

MappedMemory &
MappedMemory::operator=(MappedMemory &&o)
{
    std::swap(mOffset, o.mOffset);
    std::swap(mSize, o.mSize);
    std::swap(mMemory, o.mMemory);
    std::swap(mData, o.mData);
    return *this;
}

MappedMemory::MappedMemory(MemoryChunk *mem, vk::DeviceSize dataOffset,
                           vk::DeviceSize size)
{
    mMemory = mem->handle();
    mDevice = mem->device()->handle();
    mOffset = mem->offset() + dataOffset;
    mSize = std::min(mem->size() - dataOffset, size);
    mData = mDevice.mapMemory(mem->handle(), mOffset, mSize, {});
}

MappedMemory::~MappedMemory()
{
    if (mMemory) {
        mDevice.unmapMemory(mMemory);
    }
}
} // namespace lava
