#include "Instance.hh"

#include <lava/common/Activatable_Impl.hh>
#include <lava/objects/Device.hh>
#include <mutex>

namespace lava
{

Instance::Instance(const std::vector<Instance::SharedFeature> &features)
{

    std::copy_if(begin(features), end(features), back_inserter(mFeatures),
                 [](SharedFeature const &f) { return !!f; });

#ifdef VULKAN_HPP_DEFAULT_DISPATCHER
    vk::DynamicLoader dl;
    PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr =
        dl.getProcAddress<PFN_vkGetInstanceProcAddr>(
            "vkGetInstanceProcAddr");
    VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);
#else
    volkInitialize();
#endif

    auto exts = vk::enumerateInstanceExtensionProperties();
    std::vector<char const *> available_extensions;
    for (auto &&e : exts)
        available_extensions.push_back(e.extensionName);

    std::vector<const char *> extensions;
    for (auto &&feat : mFeatures) {
        auto additional = feat->instanceExtensions(available_extensions);
        copy(begin(additional), end(additional), back_inserter(extensions));
    }

    auto ls = vk::enumerateInstanceLayerProperties();
    std::vector<char const *> available_layers;
    for (auto &&l : ls)
        available_layers.push_back(l.layerName);

    std::vector<const char *> layers;
    for (auto &&feat : mFeatures) {
        auto additional = feat->layers(available_layers);
        copy(begin(additional), end(additional), back_inserter(layers));
    }

    vk::ApplicationInfo applicationInfo("", 1, "lava", 1,
                                        VK_API_VERSION_1_1);

    vk::InstanceCreateInfo info;
    info.setEnabledLayerCount((uint32_t)layers.size());
    info.setPpEnabledLayerNames(layers.data());
    info.setEnabledExtensionCount((uint32_t)extensions.size());
    info.setPpEnabledExtensionNames(extensions.data());
    info.pApplicationInfo = &applicationInfo;
    mInstance = vk::createInstanceUnique(info);
    mIsOwnDeviceHandle = true;
    {
#ifdef VULKAN_HPP_DEFAULT_DISPATCHER
        VULKAN_HPP_DEFAULT_DISPATCHER.init(mInstance.get());
#else
        volkLoadInstance(VkInstance(mInstance.get()));
#endif
    }

    for (auto &&feat : mFeatures)
        feat->onInstanceCreated(this);
}

Instance::Instance(VkInstance vkInstance,
                   std::vector<SharedFeature> const &features)
{
    {
#ifdef VULKAN_HPP_DEFAULT_DISPATCHER
        vk::DynamicLoader dl;
        PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr =
            dl.getProcAddress<PFN_vkGetInstanceProcAddr>(
                "vkGetInstanceProcAddr");
        VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);
#else
        volkInitialize();
#endif
    }
    std::copy_if(begin(features), end(features), back_inserter(mFeatures),
                 [](SharedFeature const &f) { return !!f; });

    mInstance = vk::UniqueInstance(vkInstance);
    mIsOwnDeviceHandle = false;
#ifdef VULKAN_HPP_DEFAULT_DISPATCHER
    VULKAN_HPP_DEFAULT_DISPATCHER.init(mInstance.get());
#else
    volkLoadInstance(vkInstance);
#endif

    for (auto &&feat : mFeatures)
        feat->onInstanceCreated(this);
}

Instance::~Instance()
{
    for (auto &&feat : mFeatures)
        feat->beforeInstanceDestruction();
    if (mIsOwnDeviceHandle) {
        mInstance->destroy();
    }
    mInstance.release();
}

SharedInstance
Instance::create(const std::vector<Instance::SharedFeature> &features)
{
    return std::make_shared<Instance>(features);
}

SharedInstance
Instance::create(VkInstance vkInstance,
                 const std::vector<Instance::SharedFeature> &features)
{
    return std::make_shared<Instance>(vkInstance, features);
}

SharedDevice
Instance::createDevice(const std::vector<QueueRequest> &queues,
                       const ISelectionStrategy &gpuSelectionStrategy)
{
    auto ptr = std::make_shared<Device>(shared_from_this(), mFeatures,
                                        gpuSelectionStrategy, queues);
    for (auto const &feat : mFeatures) {
        feat->onLogicalDeviceCreated(ptr);
    }
    return ptr;
}

SharedDevice
Instance::createDevice(const std::vector<QueueRequest> &queues,
                       const IGroupAssemblyStrategy &groupAssembly)
{
    auto ptr = std::make_shared<Device>(shared_from_this(), mFeatures,
                                        groupAssembly, queues);
    for (auto const &feat : mFeatures) {
        feat->onLogicalDeviceCreated(ptr);
    }
    return ptr;
}

SharedDevice
Instance::createDevice(VkPhysicalDevice phyDeviceHandle,
                       VkDevice logicalDeviceHandle,
                       const std::vector<QueueFamilyInfo> &familyInfos)
{
    auto ptr = std::make_shared<Device>(shared_from_this(), mFeatures,
                                        phyDeviceHandle,
                                        logicalDeviceHandle, familyInfos);
    for (auto const &feat : mFeatures) {
        feat->onLogicalDeviceCreated(ptr);
    }
    return ptr;
}

} // namespace lava
