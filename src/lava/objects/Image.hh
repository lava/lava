#pragma once
#include <lava/common/FormatInfo.hh>
#include <lava/common/MemoryType.hh>
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <vector>

namespace lava
{

enum class MemoryType;

class Image : public std::enable_shared_from_this<Image>
{
  public:
    LAVA_NON_MOVABLE(Image);

    Image(const SharedDevice &device, vk::ImageCreateInfo info,
          vk::ImageViewType type);

    /// Assumes that the Image is managed somewhere else (e.g. in a
    /// swapchain) The ImageCreateInfo will not be used to actually create
    /// an image, but to derive properties of the image (format, extend,
    /// ...)
    Image(const SharedDevice &device, vk::ImageCreateInfo approx,
          vk::Image unowned, vk::ImageViewType type);

    /// set the Image to the given data, automatically uses the
    /// transfer-queue of its device if the Memory is not host visible
    template <typename T> void setDataVRAM(std::vector<T> const &data)
    {
        const size_t num_bytes =
            mCreateInfo.extent.width * mCreateInfo.extent.height *
            mCreateInfo.extent.depth * bytePerPixel(mCreateInfo.format);
        (void)num_bytes; // unused in release
        assert(data.size() * sizeof(T) == num_bytes &&
               "The size of the vector provided "
               "does not match the size of the image contents.");
        setDataVRAM(data.data());
    }

    /// set the Image to the given data, uses Memory that is host visible,
    /// so no CommandBuffers/Queues are involved
    template <typename T> void setDataRAM(std::vector<T> const &data)
    {
        const size_t num_bytes =
            mCreateInfo.extent.width * mCreateInfo.extent.height *
            mCreateInfo.extent.depth * bytePerPixel(mCreateInfo.format);
        (void)num_bytes; // unused in release
        assert(data.size() * sizeof(T) == num_bytes &&
               "The size of the vector provided "
               "does not match the size of the image contents.");
        setDataRAM(data.data());
    }

    /// set the Image to the given data, uses cmd for transfer if the Memory
    /// is not host visible and layout transition
    void setDataVRAM(void const *data, RecordingCommandBuffer &cmd);
    /// set the Image to the given data, automatically uses the
    /// transfer-queue of its device if the Memory is not host visible,
    /// leaves the Image in eGeneral Layout
    void setDataVRAM(void const *data);
    /// set the Image to the given data, uses graphics queue of its device
    /// for layout transition, leaves the Image in eGeneral Layout
    void setDataRAM(void const *data);

    /// copy the data into a vector of T, expects the Image to be in
    /// eGeneral Layout
    template <typename T> std::vector<T> getData(int level = 0)
    {
        std::vector<T> result;
        result.resize(levelBytes(level) / sizeof(T));
        getData(result.data(), level);
        return result;
    }

    /// copy the data to the chunk of memory behind data, must already be
    /// allocated and sufficiently large, expects the Image to bei in
    /// eGeneral Layout
    void getData(void *data, int level = 0);

    /// Allocates a MemoryChunk from VRAM and binds it to the Image
    void realizeVRAM();
    /// Allocates a MemoryChunk from RAM and binds it to the Image
    void realizeRAM();
    /// Allocates a MemoryChunk from VRAM and binds it to the Image.
    /// Uses a dedicated allocation, which is advantageous for render
    /// targets.
    void realizeAttachment();

    /// copy content of a Buffer to this Image, uses the transfer-queue
    /// of its device, leaves the Image in eGeneral Layout
    void copyFrom(SharedBuffer const &buffer);
    /// copy content of a Buffer to this Image, uses the provided
    /// CommandBuffer, leaves the Image in eGeneral Layout
    void copyFrom(SharedBuffer const &buffer, RecordingCommandBuffer &cmd);
    /// copy content of a Buffer with an offset to this Image.
    void copyFrom(SharedBuffer const& buffer, uint64_t bufferOffset, RecordingCommandBuffer& cmd);
    /// copy content of this Image to the buffer, uses the transfer-queue of
    /// its device, Image is expected in eTransferSrcOptimal layout
    void copyTo(SharedBuffer const &buffer, int level = 0);
    /// copy content of this Image to the buffer, Image is expected in
    /// eTransferSrcOptimal Layout
    void copyTo(SharedBuffer const &buffer, RecordingCommandBuffer &cmd,
                int level = 0);

    /// blit the contents to the other image, uses the transfer-queue of its
    /// device. Source image is expected to be in eTransferSrcOptimal layout
    void blitTo(SharedImage const &other);
    /// blit the contents to the other image.
    /// Source image is expected to be in eTransferSrcOptimal layout
    void blitTo(SharedImage const &other, RecordingCommandBuffer &cmd);
    void blitTo(SharedImage const &other, uint32_t baseLayer,
                uint32_t layerCount, vk::ImageAspectFlags srcAspects,
                vk::ImageAspectFlags dstAspects,
                RecordingCommandBuffer &cmd);
    /// blit the contents to the other image, using the offsets and
    /// positions specified in regions.
    /// Source image is expected to be in eTransferSrcOptimal layout
    void blitTo(SharedImage const &other, RecordingCommandBuffer &cmd,
                vk::ImageBlit region);
    void blitTo(SharedImage const& other,
                vk::ImageAspectFlags srcAspects, vk::ImageAspectFlags dstAspects,
                const std::vector<vk::ImageBlit>& regions,
                RecordingCommandBuffer& cmd);

    /// copy content of another Image to this image, uses the graphics-queue
    /// of its device, expects other Image in eTransferSrc Layout
    void copyFrom(SharedImage const &other);
    void copyFrom(SharedImage const &other, RecordingCommandBuffer &cmd);
    void copyLayerFrom(SharedImage const &other, uint32_t layer, RecordingCommandBuffer &cmd);

    /// transition between the given layouts, uses the graphics-queue of its
    /// device
    void changeLayout(vk::ImageLayout from, vk::ImageLayout to);
    /// transition to the given layout (potentially) discarding the old
    /// data, uses the graphics-queue of its device
    void changeLayout(vk::ImageLayout to);

    /// transition between the given layouts, uses the provided
    /// CommandBuffer
    void changeLayout(vk::ImageLayout from, vk::ImageLayout to,
                      RecordingCommandBuffer &cmd);
    /// transition to the given layout (potentially) discarding the old
    /// data, uses the provided CommandBuffer
    void changeLayout(vk::ImageLayout to, RecordingCommandBuffer &cmd);

    /// changes the owner of the Image and changes the layout
    void changeOwner(Queue *oldOwner, vk::ImageLayout oldLayout,
                     Queue *newOwner, vk::ImageLayout newLayout);
    /// changes the owner of the Image and changes the layout, determines
    /// the owners based on the given CommandBuffers
    void changeOwner(RecordingCommandBuffer &oldBuffer,
                     vk::ImageLayout oldLayout,
                     RecordingCommandBuffer &newBuffer,
                     vk::ImageLayout newLayout);

    /// fill all levels of the texture with scaled-down versions of the
    /// first level, expects the image to be in eTransferSrcOptimal layout
    /// and keeps it uses the transfer-queue of its device
    void generateMipmaps();

    /// fill all levels of the texture with scaled-down versions of the
    /// first level, expects the image to be in eTransferSrcOptimal layout
    /// and keeps it uses the provided CommandBuffer
    void generateMipmaps(RecordingCommandBuffer &cmd);

    uint32_t width() const { return mCreateInfo.extent.width; }
    uint32_t height() const { return mCreateInfo.extent.height; }
    uint32_t depth() const { return mCreateInfo.extent.depth; }
    uint32_t layers() const { return mCreateInfo.arrayLayers; }
    uint32_t mipLevels() const { return mCreateInfo.mipLevels; }
    vk::Format format() const { return mCreateInfo.format; }

    /// Create a view into the complete image, with the Image's view type
    SharedImageView createView();
    /// Create a view into the given range of the Image, with the Image's
    /// view type.
    SharedImageView createView(vk::ImageSubresourceRange const &range);
    /// Create a view into the complete image, with given view type.
    SharedImageView createView(vk::ImageViewType type);
    /// Create a view into the Image, with the provided view type.
    SharedImageView createView(vk::ImageViewType type,
                               vk::ImageSubresourceRange const &range);

    vk::Image handle() const { return mHandle; }
    SharedDevice const &device() const { return mDevice; }

    ~Image();

    size_t levelPixels(int level = 0);
    size_t levelBytes(int level = 0);

    vk::ImageCreateInfo const &createInfo() const { return mCreateInfo; }

    SharedMemoryChunk const &memoryChunk() const { return mMemory; }

    void keepStagingBuffer(bool val = true) { mKeepStagingBuffer = val; }

  protected:
    void initMemory(MemoryType type);
    lava::SharedBuffer prepStagingBuffer(size_t size);
    void changeOwnerImpl(Queue *oldOwner, RecordingCommandBuffer &oldBuffer,
                         vk::ImageLayout oldLayout, Queue *newOwner,
                         RecordingCommandBuffer &newBuffer,
                         vk::ImageLayout newLayout);

    vk::Image mHandle;
    vk::ImageViewType mType;
    vk::ImageCreateInfo mCreateInfo;
    bool mUnowned = false;

    SharedMemoryChunk mMemory;
    SharedDevice mDevice;

    SharedBuffer mStagingBuffer;
    bool mKeepStagingBuffer = false;

    friend class ImageView;
};
} // namespace lava
