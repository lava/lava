#include <lava/objects/Device.hh>
#include <lava/objects/MemoryChunk.hh>
#include <lava/objects/Suballocator.hh>
#include <lava/raii/Barriers.hh>
#include <lava/objects/Image.hh>
#include <lava/createinfos/Images.hh>

#include "Buffer.hh"

namespace
{
vk::PipelineStageFlags
stagesForUsage(vk::BufferUsageFlags usage)
{
    vk::PipelineStageFlags flags;
    auto check = [&](auto use, auto flag) {
        if (usage & use)
            flags |= flag;
    };

    check(vk::BufferUsageFlagBits::eTransferSrc,
          vk::PipelineStageFlagBits::eTransfer);
    check(vk::BufferUsageFlagBits::eTransferDst,
          vk::PipelineStageFlagBits::eTransfer);
    check(vk::BufferUsageFlagBits::eUniformTexelBuffer,
          vk::PipelineStageFlagBits::eAllCommands);
    check(vk::BufferUsageFlagBits::eStorageTexelBuffer,
          vk::PipelineStageFlagBits::eAllCommands);
    check(vk::BufferUsageFlagBits::eUniformBuffer,
          vk::PipelineStageFlagBits::eAllCommands);
    check(vk::BufferUsageFlagBits::eStorageBuffer,
          vk::PipelineStageFlagBits::eAllCommands);
    check(vk::BufferUsageFlagBits::eIndexBuffer,
          vk::PipelineStageFlagBits::eVertexInput);
    check(vk::BufferUsageFlagBits::eVertexBuffer,
          vk::PipelineStageFlagBits::eVertexInput);
    check(vk::BufferUsageFlagBits::eIndirectBuffer,
          vk::PipelineStageFlagBits::eDrawIndirect);
    check(vk::BufferUsageFlagBits::eTransformFeedbackBufferEXT,
          vk::PipelineStageFlagBits::eTransformFeedbackEXT);
    check(vk::BufferUsageFlagBits::eTransformFeedbackCounterBufferEXT,
          vk::PipelineStageFlagBits::eTransformFeedbackEXT);
    check(vk::BufferUsageFlagBits::eConditionalRenderingEXT,
          vk::PipelineStageFlagBits::eConditionalRenderingEXT);
    check(vk::BufferUsageFlagBits::eRayTracingNV,
          vk::PipelineStageFlagBits::eRayTracingShaderNV);

    return flags;
}

vk::AccessFlags
accessForUsage(vk::BufferUsageFlags usage)
{
    vk::AccessFlags flags;
    auto check = [&](auto use, auto flag) {
        if (usage & use)
            flags |= flag;
    };

    check(vk::BufferUsageFlagBits::eTransferSrc,
          vk::AccessFlagBits::eTransferRead);
    check(vk::BufferUsageFlagBits::eTransferDst,
          vk::AccessFlagBits::eTransferWrite);
    check(vk::BufferUsageFlagBits::eUniformTexelBuffer,
          vk::AccessFlagBits::eUniformRead);
    check(vk::BufferUsageFlagBits::eStorageTexelBuffer,
          vk::AccessFlagBits::eShaderRead |
              vk::AccessFlagBits::eShaderWrite);
    check(vk::BufferUsageFlagBits::eUniformBuffer,
          vk::AccessFlagBits::eUniformRead);
    check(vk::BufferUsageFlagBits::eStorageBuffer,
          vk::AccessFlagBits::eShaderRead |
              vk::AccessFlagBits::eShaderWrite);
    check(vk::BufferUsageFlagBits::eIndexBuffer,
          vk::AccessFlagBits::eIndexRead);
    check(vk::BufferUsageFlagBits::eVertexBuffer,
          vk::AccessFlagBits::eVertexAttributeRead);
    check(vk::BufferUsageFlagBits::eIndirectBuffer,
          vk::AccessFlagBits::eIndirectCommandRead);
    check(vk::BufferUsageFlagBits::eTransformFeedbackBufferEXT,
          vk::AccessFlagBits::eTransformFeedbackWriteEXT);
    check(vk::BufferUsageFlagBits::eTransformFeedbackCounterBufferEXT,
          vk::AccessFlagBits::eTransformFeedbackCounterReadEXT |
              vk::AccessFlagBits::eTransformFeedbackCounterWriteEXT);
    check(vk::BufferUsageFlagBits::eConditionalRenderingEXT,
          vk::AccessFlagBits::eConditionalRenderingReadEXT);
    check(vk::BufferUsageFlagBits::eRayTracingNV,
          vk::AccessFlagBits::eAccelerationStructureReadNV |
              vk::AccessFlagBits::eAccelerationStructureWriteNV);

    return flags;
}
} // namespace

namespace lava
{

Buffer::Buffer(SharedDevice device, vk::BufferCreateInfo info)
    : mCreateInfo(info), mDevice(device)
{
    if (info.size)
        mHandle = mDevice->handle().createBuffer(mCreateInfo);
}

void
Buffer::setDataVRAM(void const *data, size_t length)
{
    RecordingCommandBuffer::convenienceBufferCheck("Buffer::setDataVRAM()");
    initHandle(length);
    if (!mMemory)
        realizeVRAM();

    if (mMemory->mappable()) { // For APUs / integrated GPUs
        auto mapped = mMemory->map();
        memcpy(mapped.data(), data, length);
    } else {
        SharedBuffer staging;
        if (mStagingBuffer) {
            staging = mStagingBuffer;
        } else {
            auto info = mCreateInfo;
            info.setUsage(vk::BufferUsageFlagBits::eTransferSrc);
            staging = mDevice->createBuffer(info);
        }
        staging->setDataRAM(data, length);

        copyFrom(staging);
        if (mKeepStagingBuffer)
            mStagingBuffer = std::move(staging);
    }
}

BufferBarrier
Buffer::setDataVRAM(const void *data, size_t length,
                    RecordingCommandBuffer &cmd)
{
    initHandle(length);
    if (!mMemory)
        realizeVRAM();

    if (mMemory->mappable()) { // For APUs / integrated GPUs
        auto mapped = mMemory->map();
        memcpy(mapped.data(), data, length);
    } else {
        auto staging = stagingBuffer();
        staging->setDataRAM(data, length);
        return copyFrom(staging, cmd);
    }
    return {};
}

void
Buffer::setDataRAM(void const *data, size_t length)
{
    initHandle(length);
    if (!mMemory)
        realizeRAM();

    auto mapped = mMemory->map();
    memcpy(mapped.data(), data, length);
}

void
Buffer::pushData(const void *data, size_t length)
{
    RecordingCommandBuffer::convenienceBufferCheck("Buffer::pushData()");
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    pushData(data, length, cmd);
}

BufferBarrier
Buffer::pushData(const void *data, size_t length,
                 RecordingCommandBuffer &cmd)
{
    cmd->updateBuffer(mHandle, 0, length, data);
    return BufferBarrier(cmd, handle())
        .addSrcStage(vk::PipelineStageFlagBits::eTransfer)
        .addSrcAccess(vk::AccessFlagBits::eTransferWrite)
        .addDstStage(stagesForUsage(mCreateInfo.usage))
        .addDstAccess(accessForUsage(mCreateInfo.usage));
}

void
Buffer::getData(void *data)
{
    assert(mMemory && "Buffer needs to be realized to get data.");

    if (mMemory->mappable()) {
        auto mapped = mMemory->map();
        memcpy(data, mapped.data(), mCreateInfo.size);
    } else {
        auto staging = stagingBuffer();
        {
            auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
            vk::BufferCopy region{0, 0, mCreateInfo.size};
            cmd->copyBuffer(mHandle, staging->mHandle, {region});
        }
        staging->getData(data);
    }
}

void
Buffer::realizeRAM()
{
    initMemory(MemoryType::RAM);
}

void
Buffer::realizeVRAM()
{
    initMemory(MemoryType::VRAM);
}

void
Buffer::copyFrom(const SharedBuffer &other)
{
    RecordingCommandBuffer::convenienceBufferCheck("Buffer::copyFrom()");
    auto &&queue = mDevice->graphicsQueue();
    auto cmd = queue.beginCommandBuffer();
    copyFrom(other, cmd);
}

BufferBarrier
Buffer::copyFrom(const SharedBuffer &other, RecordingCommandBuffer &cmd)
{
    vk::BufferCopy region{
        0, 0, std::min(mCreateInfo.size, other->mCreateInfo.size)};
    return copyFrom(other, { region }, cmd);
}

BufferBarrier
Buffer::copyFrom(SharedBuffer const& other, const std::vector<vk::BufferCopy>& regions, RecordingCommandBuffer& cmd)
{
    cmd->copyBuffer(other->mHandle, mHandle, regions);
    cmd.attachResource(other);

    return BufferBarrier(cmd, handle())
        .addSrcStage(vk::PipelineStageFlagBits::eTransfer)
        .addSrcAccess(vk::AccessFlagBits::eTransferWrite)
        .addDstStage(stagesForUsage(mCreateInfo.usage))
        .addDstAccess(accessForUsage(mCreateInfo.usage));
}

BufferBarrier
Buffer::copyFrom(SharedImage const& image, RecordingCommandBuffer& cmd)
{
    return copyFrom(image, 0, image->layers(), cmd);
}

BufferBarrier
Buffer::copyFrom(SharedImage const& image, uint32_t baseArrayLayer, uint32_t layerCount, RecordingCommandBuffer& cmd)
{
    auto& imgCreateInfo = image->createInfo();
    vk::BufferImageCopy region;
    region.bufferOffset = 0;
    region.bufferRowLength = image->width();
    region.bufferImageHeight = image->height();
    region.imageSubresource.aspectMask = aspectsOf(imgCreateInfo.format);
    region.imageSubresource.baseArrayLayer = baseArrayLayer;
    region.imageSubresource.layerCount = layerCount;
    region.imageSubresource.mipLevel = 0;
    region.imageOffset = vk::Offset3D(0, 0, 0);
    region.imageExtent = image->createInfo().extent;

    cmd->copyImageToBuffer(image->handle(), vk::ImageLayout::eTransferSrcOptimal, handle(), { region });
    cmd.attachResource(image);

    return BufferBarrier(cmd, handle())
        .addSrcStage(vk::PipelineStageFlagBits::eTransfer)
        .addSrcAccess(vk::AccessFlagBits::eTransferWrite)
        .addDstStage(stagesForUsage(mCreateInfo.usage))
        .addDstAccess(accessForUsage(mCreateInfo.usage));
}

Buffer::~Buffer() { mDevice->handle().destroyBuffer(mHandle); }

void
Buffer::initHandle(size_t dataLen)
{
    if (mHandle) {
        assert(dataLen <= mCreateInfo.size &&
               "Buffers in Vulkan cannot be "
               "enlarged. Create a new one or "
               "start off with a bigger one.");
    } else {
        mCreateInfo.size = dataLen;
        mHandle = mDevice->handle().createBuffer(mCreateInfo);
    }
}

void
Buffer::initMemory(MemoryType type)
{
    if (mMemory)
        return;
    assert(mHandle != vk::Buffer{} &&
           "Cannot realize a Buffer that doesn't have a size yet.");

    vk::BufferMemoryRequirementsInfo2 info(mHandle);
    vk::MemoryDedicatedRequirements dedicatedReq;
    vk::MemoryRequirements2 req;
    req.pNext = &dedicatedReq;
    mDevice->handle().getBufferMemoryRequirements2(&info, &req);

    if (dedicatedReq.prefersDedicatedAllocation ||
        dedicatedReq.requiresDedicatedAllocation) {
        mMemory = mDevice->suballocator()->allocateDedicated(
            req.memoryRequirements, type, mHandle);
    } else {
        mMemory =
            mDevice->suballocator()->allocate(req.memoryRequirements, type);
    }

    assert(mMemory->offset() % req.memoryRequirements.alignment == 0);
    mMemory->bindToBuffer(mHandle);
}

SharedBuffer
Buffer::stagingBuffer()
{
    SharedBuffer staging;
    if (mStagingBuffer) {
        staging = mStagingBuffer;
    } else {
        auto info = mCreateInfo;
        info.setUsage(vk::BufferUsageFlagBits::eTransferSrc |
                      vk::BufferUsageFlagBits::eTransferDst);
        staging = mDevice->createBuffer(info);
        staging->realizeRAM();
    }
    if (mKeepStagingBuffer)
        mStagingBuffer = staging;

    return staging;
}
} // namespace lava
