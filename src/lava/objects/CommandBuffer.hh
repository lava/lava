#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/fwd.hh>
#include <lava/fwd.hh>

namespace lava
{

class CommandBuffer : public std::enable_shared_from_this<CommandBuffer>
{
  public:
    LAVA_NON_MOVABLE(CommandBuffer);
    CommandBuffer();

    /// submits the CommandBuffer and prevents its deletion until completion
    /// TODO: some kind of thread pool to make this call return instantly
    void submitAndWait();

    void submit();

    /// signals the given Semaphore once the CommandBuffer finished
    /// execution
    void signal(vk::Semaphore sem);

    /// waits for the given Semaphore before executing
    void wait(vk::Semaphore sem,
              vk::PipelineStageFlags stage =
                  vk::PipelineStageFlagBits::eAllCommands);

    RecordingCommandBuffer begin();
    CommandBuffer(Queue *queue, vk::CommandBufferLevel level);
    ~CommandBuffer();
    vk::CommandBuffer const &handle() { return mHandle; }

    Queue *queue() { return mQueue; }

    void attachResource(std::shared_ptr<void> res)
    {
        mResources.emplace_back(std::move(res));
    }

  protected:
    vk::CommandBufferLevel mLevel;
    vk::CommandBuffer mHandle;
    Queue *mQueue;
    std::vector<vk::Semaphore> mSignalSemaphores;
    std::vector<vk::Semaphore> mWaitSemaphores;
    std::vector<vk::PipelineStageFlags> mWaitStages;
    std::vector<std::shared_ptr<void>> mResources;

    friend class RecordingCommandBuffer;
};

class ActiveRenderPass;
class RecordingCommandBuffer
{
  public:
    RecordingCommandBuffer(RecordingCommandBuffer const &) = delete;
    RecordingCommandBuffer &
    operator=(RecordingCommandBuffer const &) = delete;
    RecordingCommandBuffer(RecordingCommandBuffer &&o);
    RecordingCommandBuffer &operator=(RecordingCommandBuffer &&o);

    ~RecordingCommandBuffer();
    void autoSubmit(bool val = true) { mAutoSubmit = val; }
    vk::CommandBuffer *operator->() { return &mBuffer->mHandle; }

    SharedCommandBuffer const &buffer() { return mBuffer; }

    ActiveRenderPass beginRenderpass(RenderPassBeginInfo const &info);
    ActiveRenderPass beginRenderpass(SharedFramebuffer const &fbo);

    /// signals the given Semaphore once the CommandBuffer finished
    /// execution
    void signal(vk::Semaphore sem) { mBuffer->signal(sem); }

    /// waits for the given Semaphore before executing
    void wait(vk::Semaphore sem) { mBuffer->wait(sem); }

    void attachResource(std::shared_ptr<void> res)
    {
        mBuffer->attachResource(std::move(res));
    }

    void bindDescriptorSets(
        const std::vector<SharedDescriptorSet> &descriptors,
        const std::vector<uint32_t> &offsets, uint32_t firstSet = 0,
        vk::PipelineBindPoint bindPoint = vk::PipelineBindPoint::eCompute,
        const SharedPipelineLayout &layout = nullptr);
    void bindDescriptorSets(
        const std::vector<SharedDescriptorSet> &descriptors,
        uint32_t firstSet = 0,
        vk::PipelineBindPoint bindPoint = vk::PipelineBindPoint::eCompute,
        const SharedPipelineLayout &layout = nullptr);

    void pushConstants(uint32_t dataSize, const void *data, uint32_t offset,
                       vk::ShaderStageFlags stageFlags,
                       const SharedPipelineLayout &layout);
    void pushConstantBlock(uint32_t size, const void *data);

    void bindPipeline(SharedComputePipeline const &pip);
    void dispatch(uint32_t x, uint32_t y = 1, uint32_t z = 1);

    /// Writes a warning to stderr when another CommandBuffer is currently
    /// recording on the same thread.
    static void convenienceBufferCheck(char const *method);

  protected:
    SharedCommandBuffer mBuffer;
    SharedPipelineLayout mLastLayout;
    RecordingCommandBuffer(SharedCommandBuffer const &buffer);

    bool mAutoSubmit = false;

    friend class CommandBuffer;
    friend class InlineSubpass;
};

inline void
CommandBuffer::signal(vk::Semaphore sem)
{
    mSignalSemaphores.push_back(sem);
}

inline void
CommandBuffer::wait(vk::Semaphore sem, vk::PipelineStageFlags stage)
{
    mWaitSemaphores.push_back(sem);
    mWaitStages.push_back(stage);
}
} // namespace lava
