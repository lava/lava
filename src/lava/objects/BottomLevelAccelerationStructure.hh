#pragma once
#include <lava/createinfos/BottomLevelAccelerationStructureCreateInfo.hh>
#include <lava/fwd.hh>

namespace lava
{

class BottomLevelAccelerationStructure
{
  public:
    BottomLevelAccelerationStructure(
        BottomLevelAccelerationStructureCreateInfo info,
        SharedDevice const &device);

    ~BottomLevelAccelerationStructure();

    vk::AccelerationStructureNV handle() const { return mHandle; }

    /// Build the acceleration structure, creates own scratch and
    /// commandbuffer
    void build();
    /// Build the acceleration structure, using the supplied scratch buffer
    /// and using a commandbuffer from the graphics queue of the device
    void build(SharedBuffer const &scratchBuffer);

    /// Build the acceleration structure with the provided scratch and
    /// command buffers
    void build(SharedBuffer const &scratchBuffer,
               RecordingCommandBuffer &cmd);

    /// Build the acceleration structure with the provided commandbuffer,
    /// creates own scratch
    void build(RecordingCommandBuffer &cmd);

    uint32_t scratchSize() const;

    uint64_t deviceHandle() const { return mDeviceHandle; }

  protected:
    vk::AccelerationStructureNV mHandle;
    uint64_t mDeviceHandle;
    BottomLevelAccelerationStructureCreateInfo mCreateInfo;
    SharedDevice mDevice;
    SharedMemoryChunk mMemory;
};

} // namespace lava
