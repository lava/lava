#include "TimeQueryPool.hh"
#include <lava/objects/Device.hh>
#include <lava/raii/ActiveRenderPass.hh>

namespace lava
{

TimeQueryPool::TimeQueryPool(const SharedDevice &device, uint32_t poolSize)
    : mDevice(device), mPoolSize(poolSize)
{
    vk::QueryPoolCreateInfo info;
    info.queryType = vk::QueryType::eTimestamp;
    info.queryCount = poolSize;

    mPool = mDevice->handle().createQueryPool(info);
}

TimeQueryPool::~TimeQueryPool()
{
    if (mPool)
        mDevice->handle().destroyQueryPool(mPool);
}

void
TimeQueryPool::record(RecordingCommandBuffer &cmd,
                      vk::PipelineStageFlagBits stage)
{
    cmd->writeTimestamp(stage, mPool, mRecordIndex);
    mRecordIndex = (mRecordIndex + 1) % mPoolSize;
}

TimeQueryPool::Delta
TimeQueryPool::readDelta(RecordingCommandBuffer &cmd)
{
    uint32_t stamps[2];
    auto res = mDevice->handle().getQueryPoolResults(mPool, mReadIndex, 2,
                                                     sizeof(stamps), stamps,
                                                     sizeof(stamps[0]), {});
    if (res == vk::Result::eSuccess) {
        cmd->resetQueryPool(mPool, mReadIndex, 2);
        mReadIndex = (mReadIndex + 2) % mPoolSize;
        return Delta{int(stamps[1] - stamps[0])};
    }
    return Delta{-1};
}
} // namespace lava
