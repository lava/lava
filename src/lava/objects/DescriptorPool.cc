#include "DescriptorPool.hh"
#include "DescriptorSet.hh"

namespace lava
{

DescriptorPool::DescriptorPool(const SharedDevice &device,
                               const DescriptorPoolCreateInfo &info)
    : mDevice(device), mCreateInfo(info)
{
    mHandle = mDevice->handle().createDescriptorPool(mCreateInfo);
}

DescriptorPool::~DescriptorPool()
{
    mDevice->handle().destroyDescriptorPool(mHandle);
}

SharedDescriptorSet
DescriptorPool::createDescriptorSet(const SharedDescriptorSetLayout &layout)
{
    return std::make_shared<DescriptorSet>(mDevice, shared_from_this(),
                                           layout);
}

} // namespace lava
