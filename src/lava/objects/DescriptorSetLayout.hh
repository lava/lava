#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>
#include <lava/fwd.hh>

namespace lava
{

class DescriptorSetLayout
    : public std::enable_shared_from_this<DescriptorSetLayout>
{
  public:
    LAVA_NON_MOVABLE(DescriptorSetLayout);

    DescriptorSetLayout(SharedDevice const &device,
                        DescriptorSetLayoutCreateInfo const &info,
                        uint32_t poolSize);
    ~DescriptorSetLayout();

    SharedDescriptorSet createDescriptorSet();

    vk::DescriptorSetLayout handle() { return mHandle; }

  protected:
    SharedDevice mDevice;
    SharedDescriptorPool mPool;
    DescriptorSetLayoutCreateInfo mCreateInfo;

    vk::DescriptorSetLayout mHandle;
};
} // namespace lava
