#include "ComputePipeline.hh"
#include <lava/objects/Device.hh>
#include <lava/objects/PipelineLayout.hh>

namespace lava
{

ComputePipeline::ComputePipeline(const ComputePipelineCreateInfo &info)
    : mCreateInfo(info)
{
    mDevice = mCreateInfo.mLayout->device();

    vk::ComputePipelineCreateInfo ci;
    ci.setStage(mCreateInfo.mStage);
    ci.setLayout(mCreateInfo.mLayout->handle());

    mHandle = static_cast<vk::Pipeline&&>(mDevice->handle().createComputePipeline({}, ci));
}

ComputePipeline::~ComputePipeline()
{
    mDevice->handle().destroyPipeline(mHandle);
}

} // namespace lava
