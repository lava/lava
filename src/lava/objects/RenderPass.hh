#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/RenderPassCreateInfo.hh>
#include <lava/createinfos/fwd.hh>
#include <lava/fwd.hh>

namespace lava
{

struct Subpass {
    SharedRenderPass pass;
    uint32_t subpassIndex;

    SharedGraphicsPipeline
    createPipeline(lava::GraphicsPipelineCreateInfo const &info) const;
};

class RenderPass : public std::enable_shared_from_this<RenderPass>
{
  public:
    LAVA_NON_MOVABLE(RenderPass);

    RenderPass(SharedDevice const &device,
               RenderPassCreateInfo const &info);
    ~RenderPass();

    vk::RenderPass handle() const { return mHandle; }

    SharedGraphicsPipeline createPipeline(uint32_t subpass,
                                          GraphicsPipelineCreateInfo info);

    /// create a Framebuffer with the given ImageViews attached, the size is
    /// automatically set to the minimum attachment sizes
    SharedFramebuffer
    createFramebuffer(std::vector<SharedImageView> const &attachments);
    /// create a Framebuffer for which the views are generated from the
    /// provided images and their default view type, the size is
    /// automatically set to the minimum of the attachment sizes
    SharedFramebuffer
    createFramebuffer(std::vector<SharedImage> const &attachments);

    SharedDevice const &device() { return mDevice; }

    /// Sets the clear color of all attachments to color
    void setClearColor(vk::ClearColorValue color);

    /// Set the clear colors of the attachments to the given colors
    void setClearColors(std::vector<vk::ClearColorValue> const &colors);

    void setClearDepthStencil(vk::ClearDepthStencilValue depthStencil);

    SharedDevice const &device() const { return mDevice; }

    RenderPassCreateInfo const &createInfo() const { return mInfo; }

  protected:
    RenderPassCreateInfo mInfo;
    vk::RenderPass mHandle;
    SharedDevice mDevice;

    std::vector<vk::ClearValue> mClearValues;

    friend class RecordingCommandBuffer;
};

} // namespace lava

/*auto mCommandBuffer = mQueue->createCommandBuffer(...);

{
    auto cmd = mCommandBuffer->begin("begin Info");
    auto cmd = mCommandBuffer->beginAndSubmit("begin Info");

    auto q = cmd.query(...);

    {
        auto rp = cmd.renderPass(...);
        {
            auto sp = rp.subPassInline();

            {
                auto p = sp.pipeline(...);

                p.bindDescriptorSets(...);
                p.pushConstants(...);
                p.bindVertex/IndexBuffer(...);
                p.draw...(...);
            }
        }
        {
            auto sp = rp.subPassSecondary();

            // no pipeline
            // only cmd buffer
        }
    }
    {
        auto rp = cmd.renderPassInline(...);
        // no subpasses
    }
    {
        auto rp = cmd.renderPassSecondary(...);
        // no subpasses
    }
}


{
    auto cmd = mQueue->beginCommandBuffer(...);


} // submits cmd
*/
