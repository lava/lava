#pragma once
#include <lava/common/Arena.hh>
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <unordered_map>

namespace lava
{

struct DescriptorSetWriter {
    LAVA_NON_MOVABLE(DescriptorSetWriter);

    ~DescriptorSetWriter();
    DescriptorSetWriter(DescriptorSet *_set) : set(_set) {}

    /// Use this if you want to skip a binding, e.g. if you don't want to
    /// start with binding 0
    DescriptorSetWriter &skipToBinding(uint32_t binding)
    {
        currentBinding = binding;
        return *this;
    }

    DescriptorSetWriter &sampledImage(SharedImageView view);
    DescriptorSetWriter &combinedImageSampler(SharedSampler sampler,
                                              SharedImageView view);
    DescriptorSetWriter &
    combinedImageSamplers(SharedSampler sampler,
                          std::vector<SharedImageView> const &views);
    DescriptorSetWriter &storageBuffer(SharedBuffer buffer);
    DescriptorSetWriter &
    storageBuffers(std::vector<SharedBuffer> const &buffers);
    DescriptorSetWriter &storageImage(SharedImageView view);
    DescriptorSetWriter &
    accelerationStructure(SharedTopLevelAccelerationStructure tlas);

    DescriptorSetWriter &uniformBuffer(SharedBuffer buffer);
    DescriptorSetWriter &
    uniformBuffers(std::vector<SharedBuffer> const &buffers);

    DescriptorSetWriter &inputAttachment(SharedImageView view,
                                         vk::ImageLayout layout);
    DescriptorSetWriter &inputAttachmentColor(SharedImageView view);
    DescriptorSetWriter &inputAttachmentDepth(SharedImageView view);

  protected:
    DescriptorSet *set;
    uint32_t currentBinding = 0;
    std::vector<vk::WriteDescriptorSet> writes;
    Arena infos;
};

class DescriptorSet
{
  public:
    LAVA_NON_MOVABLE(DescriptorSet);

    DescriptorSet(SharedDevice const &device,
                  SharedDescriptorPool const &pool,
                  const SharedDescriptorSetLayout &layout);
    ~DescriptorSet();

    vk::DescriptorSet handle() const { return mHandle; }

    /// Assumes that the images are in
    /// vk::ImageLayout::eShaderReadOnlyOptimal when rendering with this set
    /// bound
    void writeCombinedImageSamplers(
        std::vector<std::pair<SharedSampler, SharedImageView>> const
            &combos,
        uint32_t binding);

    void writeCombinedImageSampler(
        std::pair<SharedSampler, SharedImageView> const &combo,
        uint32_t binding);

    void writeUniformBuffer(SharedBuffer const &buffer, uint32_t binding);

    void writeSampledImage(SharedImageView const &image, uint32_t binding);

    DescriptorSetWriter write() { return {this}; }

    SharedDescriptorSetLayout const &layout() const { return mLayout; }

  protected:
    SharedDevice mDevice;
    SharedDescriptorPool mPool;
    SharedDescriptorSetLayout mLayout;
    vk::DescriptorSet mHandle;
    vk::DescriptorSetAllocateInfo mInfo;

    // Keep Samplers/Views etc. to keep them from being destroyed while in
    // use
    using ResourceList = std::vector<std::shared_ptr<void>>;
    std::unordered_map<uint32_t, ResourceList> mBindingResources;

    friend struct DescriptorSetWriter;
};
} // namespace lava
