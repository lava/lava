#pragma once

#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class Sampler
{
  public:
    Sampler(SharedDevice const &device, vk::SamplerCreateInfo const &info);
    ~Sampler();

    vk::Sampler handle() const { return mHandle; }

  private:
    SharedDevice mDevice;
    vk::SamplerCreateInfo mInfo;
    vk::Sampler mHandle;
};

} // namespace lava
