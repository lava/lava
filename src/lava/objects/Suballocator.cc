#include "Suballocator.hh"
#include <bitset>
#include <lava/objects/Device.hh>
#include <lava/objects/MemoryChunk.hh>

namespace lava
{

Suballocator::Suballocator(Device &device,
                           vk::DeviceSize biGranularity) noexcept
    : mDevice(device), mBufferImageGranularity(biGranularity)
{
    mMemoryProperties = mDevice.physical().getMemoryProperties();
}

uint32_t
Suballocator::typeIndexFor(vk::MemoryRequirements req,
                           vk::MemoryPropertyFlags flags)
{
    auto memtype = uint32_t(-1);
    {
        size_t best_popcount = 0;
        for (uint32_t i = 0; i < mMemoryProperties.memoryTypeCount; i++) {
            if (req.memoryTypeBits & (1 << i)) {
                auto current_flags =
                    mMemoryProperties.memoryTypes[i].propertyFlags;
                auto intersect =
                    std::bitset<32>(uint32_t(flags & current_flags));

                if (intersect.count() > best_popcount) {
                    best_popcount = intersect.count();
                    memtype = i;
                }
            }
        }

        if (memtype == uint32_t(-1)) {
            throw std::runtime_error(
                "Could not find appropriate memory class.");
        }
    }
    return memtype;
}

SharedMemoryChunk
Suballocator::allocate(vk::MemoryRequirements req,
                       vk::MemoryPropertyFlags flags)
{
    /// the bufferImageGranularity is the size of memory pages that must not
    /// be shared between linear resources (buffers) and non-linear
    /// resources (images). In order to limit complexity, we (for now)
    /// assume that every currently bound resource is of the "wrong" type
    /// => increase the alignment to the page size
    req.alignment = std::max(req.alignment, mBufferImageGranularity);

    uint32_t typeIdx = typeIndexFor(req, flags);
    auto type = mMemoryProperties.memoryTypes[typeIdx];
    auto heapIndex = type.heapIndex;
    auto heap = mMemoryProperties.memoryHeaps[heapIndex];
    auto blocksize = heap.size > mSmallHeapThreshold ? mLargeHeapBlockSize
                                                     : mSmallHeapBlockSize;

    if (req.size > blocksize)
        return allocateDedicated(req, flags);

    auto toAlign = [&](vk::DeviceSize offset) {
        auto remainder = offset % req.alignment;
        if (!remainder)
            return offset;
        return offset + req.alignment - remainder;
    };

    auto deallocator = [this](MemoryChunk &chunk) {
        this->deallocate(chunk);
    };
    auto mappable =
        !!(type.propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible);
    auto &blocks = mTypeBlocks[typeIdx];
    for (auto &block : blocks) {
        for (auto it = begin(block.holes); it != end(block.holes); ++it) {
            auto &hole = *it;
            auto diff = int64_t(hole.end) - int64_t(toAlign(hole.begin)) -
                        int64_t(req.size);

            if (diff == 0) {
                // Allocation fits hole exactly, throw out the hole
                block.holes.erase(it);
                return std::make_shared<MemoryChunk>(
                    block.memory->handle(), mDevice.shared_from_this(),
                    hole.begin, toAlign(hole.begin), req.size, typeIdx,
                    mappable, deallocator);
            } else if (diff > 0) {
                // Put the allocation at the beginning of the hole
                auto begin = hole.begin;
                hole.begin = toAlign(begin) + req.size;
                return std::make_shared<MemoryChunk>(
                    block.memory->handle(), mDevice.shared_from_this(),
                    begin, toAlign(begin), req.size, typeIdx, mappable,
                    deallocator);
            }
        }
    }

    // Still no hole found, add a new block
    vk::MemoryAllocateInfo info;
    info.memoryTypeIndex = typeIdx;
    info.allocationSize = blocksize;
    auto memory = mDevice.handle().allocateMemory(info);
    auto blockchunk = std::make_shared<MemoryChunk>(
        memory, mDevice.shared_from_this(), 0, 0, blocksize, typeIdx,
        mappable, MemoryChunk::Deallocator{});
    blocks.push_back(
        MemoryBlock{blockchunk, blocksize, {{req.size, blocksize}}});

    return std::make_shared<MemoryChunk>(
        blocks.back().memory->handle(), mDevice.shared_from_this(), 0, 0,
        req.size, typeIdx, mappable, deallocator);
}

SharedMemoryChunk
Suballocator::allocateDedicated(vk::MemoryRequirements req,
                                vk::MemoryPropertyFlags flags)
{
    return internalAllocateDedicated(req, flags);
}

SharedMemoryChunk
Suballocator::allocateDedicated(vk::MemoryRequirements req,
                                vk::MemoryPropertyFlags flags,
                                vk::Buffer buffer)
{
    return internalAllocateDedicated(req, flags, buffer);
}

SharedMemoryChunk
Suballocator::allocateDedicated(vk::MemoryRequirements req,
                                vk::MemoryPropertyFlags flags,
                                vk::Image image)
{
    return internalAllocateDedicated(req, flags, vk::Buffer(), image);
}

static inline vk::MemoryPropertyFlags
flagsForType(MemoryType type)
{
    vk::MemoryPropertyFlags flags;
    switch (type) {
    case MemoryType::VRAM:
        flags = vk::MemoryPropertyFlagBits::eDeviceLocal |
                vk::MemoryPropertyFlagBits::eLazilyAllocated;
        break;
    case MemoryType::RAM:
        flags = vk::MemoryPropertyFlagBits::eHostVisible |
                vk::MemoryPropertyFlagBits::eHostCoherent |
                vk::MemoryPropertyFlagBits::eHostCached;
        break;
    }
    return flags;
}

SharedMemoryChunk
Suballocator::allocate(vk::MemoryRequirements req, MemoryType type)
{
    return allocate(req, flagsForType(type));
}

SharedMemoryChunk
Suballocator::allocateDedicated(vk::MemoryRequirements req, MemoryType type)
{
    return allocateDedicated(req, flagsForType(type));
}

SharedMemoryChunk
Suballocator::allocateDedicated(vk::MemoryRequirements req, MemoryType type,
                                vk::Buffer buffer)
{
    return allocateDedicated(req, flagsForType(type), buffer);
}

SharedMemoryChunk
Suballocator::allocateDedicated(vk::MemoryRequirements req, MemoryType type,
                                vk::Image image)
{
    return allocateDedicated(req, flagsForType(type), image);
}

size_t
Suballocator::heldBlockCount() const
{
    size_t sum = 0;
    for (auto &&vec : mTypeBlocks)
        sum += vec.size();
    return sum;
}

SharedMemoryChunk
Suballocator::internalAllocateDedicated(vk::MemoryRequirements req,
                                        vk::MemoryPropertyFlags flags,
                                        vk::Buffer buffer, vk::Image image)
{
    auto typeIdx = typeIndexFor(req, flags);
    auto type = mMemoryProperties.memoryTypes[typeIdx];

    vk::MemoryAllocateInfo info;
    info.memoryTypeIndex = typeIdx;
    info.allocationSize = req.size;

    vk::MemoryDedicatedAllocateInfo dedicatedAllocInfo(image, buffer);
    if (type.propertyFlags & vk::MemoryPropertyFlagBits::eDeviceLocal &&
        (buffer || image)) {
        info.pNext = &dedicatedAllocInfo;
    }

    vk::DeviceMemory memory;
    try {
        memory = mDevice.handle().allocateMemory(info);
    } catch (vk::OutOfDeviceMemoryError &e) {
        if (!(type.propertyFlags &
              vk::MemoryPropertyFlagBits::eDeviceLocal)) {
            // The nvidia driver throws VK_ERROR_OUT_OF_DEVICE_MEMORY, even
            // when host memory is the issue. So when it occurs during an
            // allocation from Host memory, we assume the driver is wrong
            // and correct it here.
            throw vk::OutOfHostMemoryError("VK_ERROR_OUT_OF_DEVICE_MEMORY "
                                           "occurred while allocating from "
                                           "host memory.");
        }
        throw;
    }

    auto mappable =
        !!(type.propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible);

    return std::make_shared<MemoryChunk>(memory, mDevice.shared_from_this(),
                                         0, 0, req.size, typeIdx, mappable,
                                         MemoryChunk::Deallocator{});
}

void
Suballocator::deallocate(MemoryChunk &chunk)
{
    auto &blocks = mTypeBlocks[chunk.type()];
    auto it = std::find_if(
        begin(blocks), end(blocks), [&](MemoryBlock const &block) {
            return block.memory->handle() == chunk.handle();
        });
    assert(it != end(blocks) &&
           "Couldn't find the block this chunk belongs to.");

    auto &block = *it;
    auto &holes = block.holes;

    auto directlyBefore =
        std::find_if(begin(holes), end(holes), [&](Hole const &hole) {
            return hole.end == chunk.allocationOffset();
        });
    auto directlyAfter =
        std::find_if(begin(holes), end(holes), [&](Hole const &hole) {
            return hole.begin == chunk.offset() + chunk.size();
        });

    size_t realsize =
        chunk.size() + (chunk.offset() - chunk.allocationOffset());
    if (directlyBefore != end(holes) && directlyAfter != end(holes)) {
        // the chunk connects two holes to one
        // => resize the first, delete the second

        directlyBefore->end = directlyAfter->end;
        holes.erase(directlyAfter);
    } else if (directlyBefore != end(holes)) {
        directlyBefore->end += realsize;
    } else if (directlyAfter != end(holes)) {
        directlyAfter->begin = chunk.allocationOffset();
    } else {
        holes.push_back(
            {chunk.allocationOffset(), chunk.offset() + chunk.size()});
    }

    if (holes.size() == 1 &&   //
        holes[0].begin == 0 && //
        holes[0].end == block.size) {

        // give completely empty blocks back to the driver
        blocks.erase(it);
    }
}

} // namespace lava
