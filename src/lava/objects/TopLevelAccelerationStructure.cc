#include "TopLevelAccelerationStructure.hh"
#include "BottomLevelAccelerationStructure.hh"
#include <iostream>
#include <lava/createinfos/Buffers.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/MemoryChunk.hh>
#include <lava/objects/Suballocator.hh>

namespace lava
{

namespace
{
struct BufferInstance {
    float transform[12] = {
        // row-major
        1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    };
    uint32_t instanceId : 24;
    uint32_t mask : 8;
    uint32_t instanceOffset : 24;
    uint32_t flags : 8;
    uint64_t accelerationStructureHandle = -1;
};
} // namespace

TopLevelAccelerationStructure::TopLevelAccelerationStructure(
    SharedDevice const &device, uint32_t num_instances)
    : mDevice(device)
{
    mCreateInfo.info.setType(vk::AccelerationStructureTypeNV::eTopLevel)
        .setInstanceCount(num_instances)
        .setFlags(
            vk::BuildAccelerationStructureFlagBitsNV::ePreferFastTrace);

    mHandle = mDevice->handle().createAccelerationStructureNV(mCreateInfo);

    vk::AccelerationStructureMemoryRequirementsInfoNV memInfo;
    memInfo.setAccelerationStructure(mHandle);

    auto reqs =
        mDevice->handle().getAccelerationStructureMemoryRequirementsNV(
            memInfo);
    mMemory = mDevice->suballocator()->allocate(reqs.memoryRequirements,
                                                lava::MemoryType::VRAM);
    auto bind = vk::BindAccelerationStructureMemoryInfoNV()
                    .setAccelerationStructure(mHandle)
                    .setMemory(mMemory->handle())
                    .setMemoryOffset(mMemory->offset());

    device->handle().bindAccelerationStructureMemoryNV(bind);
}

TopLevelAccelerationStructure::~TopLevelAccelerationStructure()
{
    mDevice->handle().destroyAccelerationStructureNV(mHandle);
}

void
TopLevelAccelerationStructure::build(
    const std::vector<RayTracingInstance> &instances)
{
    auto cmd = mDevice->graphicsQueue().beginCommandBuffer();
    build(instances, cmd);
}

void
TopLevelAccelerationStructure::build(
    const std::vector<RayTracingInstance> &instances,
    RecordingCommandBuffer &cmd)
{
    mBottomLevels.clear();

    std::vector<BufferInstance> deviceInstances;
    deviceInstances.reserve(instances.size());
    for (auto const &i : instances) {
        mBottomLevels.insert(i.bottomLevelAS);

        BufferInstance inst;
        // RTX wants the matrix in row-major order
        for (int col = 0; col < 4; col++) {
            for (int row = 0; row < 3; row++) {
                inst.transform[4 * row + 3] =
                    i.transform.matrix[3 * col + row];
            }
        }
        inst.instanceId = i.instanceCustomIndex;
        inst.mask = i.mask;
        inst.instanceOffset = i.hitgroupOffset;
        inst.flags = i.flags;
        inst.accelerationStructureHandle = i.bottomLevelAS->deviceHandle();
        deviceInstances.push_back(inst);
    }
    auto bytes_needed = sizeof(BufferInstance) * deviceInstances.size();
    if (!mInstanceBuffer || mInstanceBuffer->size() < bytes_needed) {
        mInstanceBuffer = mDevice->createBuffer(raytracingBuffer());
    }
    mInstanceBuffer->setDataVRAM(deviceInstances, cmd);

    auto scratchBuffer =
        mDevice->createBuffer(raytracingBuffer(scratchSize()));
    scratchBuffer->realizeVRAM();

    auto barr = vk::MemoryBarrier()
                    .setSrcAccessMask(
                        vk::AccessFlagBits::eAccelerationStructureWriteNV)
                    .setDstAccessMask(
                        vk::AccessFlagBits::eAccelerationStructureReadNV |
                        vk::AccessFlagBits::eAccelerationStructureWriteNV);
    cmd->pipelineBarrier(
        vk::PipelineStageFlagBits::eAccelerationStructureBuildNV,
        vk::PipelineStageFlagBits::eAccelerationStructureBuildNV, {}, barr,
        {}, {});

    assert(instances.size() <= mCreateInfo.info.instanceCount &&
           "TLAS too small");
    auto info = mCreateInfo.info;
    info.setInstanceCount(instances.size());
    cmd->buildAccelerationStructureNV(info, mInstanceBuffer->handle(), 0,
                                      false, mHandle, {},
                                      scratchBuffer->handle(), 0);
    cmd.attachResource(std::move(scratchBuffer));
}

uint32_t
TopLevelAccelerationStructure::scratchSize() const
{
    vk::AccelerationStructureMemoryRequirementsInfoNV info;
    info.setAccelerationStructure(mHandle);
    info.setType(
        vk::AccelerationStructureMemoryRequirementsTypeNV::eBuildScratch);
    return mDevice->handle()
        .getAccelerationStructureMemoryRequirementsNV(info)
        .memoryRequirements.size;
}

} // namespace lava
