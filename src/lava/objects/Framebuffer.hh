#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class Framebuffer
{
  public:
    LAVA_NON_MOVABLE(Framebuffer);

    Framebuffer(SharedRenderPass const &pass,
                std::vector<SharedImageView> const &views);
    Framebuffer(SharedRenderPass const &pass,
                std::vector<SharedImage> const &images);
    ~Framebuffer();

    vk::Framebuffer handle() const { return mHandle; }
    std::vector<SharedImageView> const &views() const { return mViews; }

    uint32_t width() const { return mCreateInfo.width; }
    uint32_t height() const { return mCreateInfo.height; }
    uint32_t layers() const { return mCreateInfo.layers; }

    SharedRenderPass const &pass() const { return mPass; }

  protected:
    void init();

    vk::Framebuffer mHandle;
    SharedRenderPass mPass;

    vk::FramebufferCreateInfo mCreateInfo;
    std::vector<vk::ImageView> mCreateInfoViews;

    std::vector<SharedImageView> mViews;
};

} // namespace lava
