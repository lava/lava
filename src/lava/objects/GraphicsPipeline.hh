#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/fwd.hh>

namespace lava
{

class GraphicsPipeline
{
  public:
    LAVA_NON_MOVABLE(GraphicsPipeline);

    GraphicsPipeline(GraphicsPipelineCreateInfo const &info);
    ~GraphicsPipeline();

    vk::Pipeline handle() const { return mHandle; }
    SharedPipelineLayout const &layout() const
    {
        return mCreateInfo.mLayout;
    }

    GraphicsPipelineCreateInfo const &createInfo() const
    {
        return mCreateInfo;
    }

    SharedDevice const &device() const { return mDevice; }

  protected:
    SharedDevice mDevice;
    SharedPipelineLayout mLayout;
    vk::Pipeline mHandle;
    GraphicsPipelineCreateInfo mCreateInfo;
};
} // namespace lava
