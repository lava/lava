#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <lava/objects/CommandBuffer.hh>

namespace lava
{

struct QueueRequest {
    constexpr static uint32_t no_index = uint32_t(-1);

    static QueueRequest graphics(std::string const &_name,
                                 float _prio = 1.0f)
    {
        return {_name, _prio, vk::QueueFlagBits::eGraphics, no_index};
    }

    static QueueRequest transfer(std::string const &_name,
                                 float _prio = 1.0f)
    {
        return {_name, _prio, vk::QueueFlagBits::eTransfer, no_index};
    }

    static QueueRequest compute(std::string const &_name,
                                float _prio = 1.0f)
    {
        return {_name, _prio, vk::QueueFlagBits::eCompute, no_index};
    }

    static QueueRequest byFlags(std::string const &_name,
                                vk::QueueFlags _flags, float _prio)
    {
        return {_name, _prio, _flags, no_index};
    }
    static QueueRequest byFamily(std::string const &_name, uint32_t index,
                                 float _prio = 1.0f)
    {
        return {_name, _prio, {}, index};
    }

    std::string name;
    float priority;
    vk::QueueFlags flags;
    uint32_t index;
};

class Queue
{
  public:
    LAVA_NON_COPYABLE(Queue);

    vk::Queue handle() const { return mQueue; }
    vk::CommandPool pool() const { return mPool; }
    uint32_t family() const { return mFamilyIndex; }

    Queue(uint32_t family, vk::Queue queue, vk::CommandPool pool,
          Device *device)
        : mFamilyIndex(family), mPool(pool), mQueue(queue), mDevice(device)
    {
    }

    SharedCommandBuffer createCommandBuffer(
        vk::CommandBufferLevel level = vk::CommandBufferLevel::ePrimary);
    RecordingCommandBuffer beginCommandBuffer();

    Device *device() const { return mDevice; }

    void submit(SharedCommandBuffer cmd,
                const std::vector<vk::Semaphore> &waitSemaphores,
                const std::vector<vk::PipelineStageFlags> &waitStages,
                const std::vector<vk::Semaphore> &signalSemaphores);

    /** wait until at most `inflightBuffers` Submissions are incomplete
     * can/should be used at the start of the frame to prevent the CPU from
     * getting ahead of the GPU by more than `inflightBuffers` / <command
     * buffers per frame> frames. **/
    void catchUp(int inflightBuffers);

    size_t activeBuffers();

  protected:
    uint32_t mFamilyIndex;
    vk::CommandPool mPool;
    vk::Queue mQueue;
    Device *mDevice;

    vk::Fence findFreeFence();
    void gc();

    std::vector<vk::Fence> mFencePool;

    std::vector<vk::Fence> mSubmissionFences;
    std::vector<SharedCommandBuffer> mSubmissionBuffers;
};

} // namespace lava
