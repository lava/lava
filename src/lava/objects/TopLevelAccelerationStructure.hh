#pragma once

#include <set>
#include <vector>

#include <lava/common/RigidTransform.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

struct RayTracingInstance {
    SharedBottomLevelAccelerationStructure bottomLevelAS;
    RigidTransform transform;
    uint32_t instanceCustomIndex =
        0; // appears as gl_InstanceCustomIndexNV in shader
    uint32_t hitgroupOffset = 0;
    uint8_t flags = 0;
    uint8_t mask = 0xff;
};

class TopLevelAccelerationStructure
{
  public:
    static SharedTopLevelAccelerationStructure
    create(SharedDevice const &device, uint32_t num_instances)
    {
        return std::make_shared<TopLevelAccelerationStructure>(
            device, num_instances);
    }

    TopLevelAccelerationStructure(SharedDevice const &device,
                                  uint32_t num_instances);
    ~TopLevelAccelerationStructure();

    void setFlags(vk::BuildAccelerationStructureFlagsNV flags)
    {
        mCreateInfo.info.setFlags(flags);
    }

    void build(std::vector<RayTracingInstance> const &instances);
    void build(std::vector<RayTracingInstance> const &instances,
               RecordingCommandBuffer &cmd);

    uint32_t scratchSize() const;

    vk::AccelerationStructureNV handle() const { return mHandle; }

  protected:
    vk::AccelerationStructureNV mHandle;
    vk::AccelerationStructureCreateInfoNV mCreateInfo;
    SharedDevice mDevice;
    SharedMemoryChunk mMemory;

    SharedBuffer mInstanceBuffer;
    std::set<SharedBottomLevelAccelerationStructure> mBottomLevels;
};

} // namespace lava
