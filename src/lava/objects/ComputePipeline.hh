#pragma once

#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/ComputePipelineCreateInfo.hh>
#include <lava/fwd.hh>

namespace lava
{
class ComputePipeline
{
  public:
    LAVA_NON_MOVABLE(ComputePipeline);

    ComputePipeline(ComputePipelineCreateInfo const &info);
    ~ComputePipeline();

    vk::Pipeline handle() const { return mHandle; }
    SharedPipelineLayout const &layout() const
    {
        return mCreateInfo.mLayout;
    }

    ComputePipelineCreateInfo const &createInfo() const
    {
        return mCreateInfo;
    }

    SharedDevice const &device() const { return mDevice; }

  protected:
    SharedDevice mDevice;
    SharedPipelineLayout mLayout;
    vk::Pipeline mHandle;
    ComputePipelineCreateInfo mCreateInfo;
};
} // namespace lava
