#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class TimeQueryPool
{
  public:
    struct Delta {
        int val;
        bool available() const { return val >= 0; }
        int value() const
        {
            assert(available());
            return val;
        }
    };

    TimeQueryPool(SharedDevice const &device, uint32_t poolSize = 32);
    ~TimeQueryPool();

    void record(RecordingCommandBuffer &cmd,
                vk::PipelineStageFlagBits stage);

    /// Read the last two queries (provided they are available yet) from the
    /// pool and return the time that has passed. Also uses the provided
    /// commandBuffer to reset the queries afterwards.
    Delta readDelta(RecordingCommandBuffer &cmd);

  private:
    SharedDevice mDevice;
    vk::QueryPool mPool;
    uint32_t mPoolSize;
    uint32_t mRecordIndex = 0;
    uint32_t mReadIndex = 0;
};
} // namespace lava
