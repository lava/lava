#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <vector>

namespace lava
{

class ImageData
{
  public:
    ImageData();
    enum ColorSpace { Linear, sRGB, AutoDetect };

    static SharedImageData
    createFromFile(const std::string &filename,
                   ColorSpace colorspace = ColorSpace::AutoDetect);

    /// Creates ImageData (for a non-array image) from a pointer to the
    /// binary color values of the given format. Assumes the provided data
    /// to be the 0th mip level
    static SharedImageData
    createFromData(const void *data, uint32_t width, uint32_t height = 1,
                   uint32_t depth = 1,
                   vk::Format format = vk::Format::eR8G8B8A8Srgb);

    /// Upload the image data as new image onto the given device.
    /// Uses the transfer-queue of the device.
    SharedImage uploadTo(lava::SharedDevice const &device) const;
    /// Upload the image data as new image onto the given device using the
    /// given queue.
    SharedImage uploadTo(lava::SharedDevice const &device,
                         lava::RecordingCommandBuffer &cmd) const;

    void disableMipmapGen() { mGenMipmaps = false; }

    void concatLayers(const void *data, uint32_t layers = 1);

  private:
    static SharedImageData loadWithLodepng(const std::string &filename,
                                           const std::string &,
                                           ColorSpace colorspace);
    static SharedImageData loadWithSTB(const std::string &filename,
                                       const std::string &,
                                       ColorSpace colorspace);

    std::vector<char> mBytes;

    vk::Extent3D mExtent;
    vk::Format mFormat;

    uint32_t mMipLevels;
    uint32_t mLayers;

    bool mGenMipmaps = false;
};
} // namespace lava
