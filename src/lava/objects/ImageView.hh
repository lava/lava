#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

class ImageView
{
  public:
    LAVA_NON_MOVABLE(ImageView);

    ImageView(SharedImage const &of, vk::ImageViewType type);
    ImageView(SharedImage const &of, vk::ImageViewType type,
              vk::ImageSubresourceRange range);

    /** create a view into an image managed elsewhere, useful in combination
     *  with swapchain images.
     *  Otherwise, you'll probably want to use Image::createView() **/
    ImageView(Device *device, vk::ImageViewCreateInfo info);

    ~ImageView();

    vk::ImageView handle() const;
    SharedImage const &image() const;
    uint32_t layers() const;
    uint32_t levels() const;

    vk::Format format() const { return mCreateInfo.format; }

  protected:
    vk::ImageView mHandle;

    Device *mDevice;
    SharedImage mImage;

    vk::ImageViewCreateInfo mCreateInfo;
};

inline vk::ImageView
ImageView::handle() const
{
    return mHandle;
}

inline const SharedImage &
ImageView::image() const
{
    return mImage;
}

inline uint32_t
ImageView::layers() const
{
    return mCreateInfo.subresourceRange.layerCount;
}

inline uint32_t
ImageView::levels() const
{
    return mCreateInfo.subresourceRange.levelCount;
}

} // namespace lava
