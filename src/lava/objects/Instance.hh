#pragma once
#include <iostream>
#include <vector>

#include <lava/common/Activatable.hh>
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/features/IFeature.hh>
#include <lava/fwd.hh>
#include <lava/gpuselection/IGroupAssemblyStrategy.hh>
#include <lava/gpuselection/ISelectionStrategy.hh>

namespace lava
{

struct QueueFamilyInfo;
class Instance : public std::enable_shared_from_this<Instance>
{
    using SharedFeature = features::SharedFeature;

  public:
    LAVA_NON_MOVABLE(Instance);
    Instance(std::vector<SharedFeature> const &features);
    Instance(VkInstance vkInstance,
             std::vector<SharedFeature> const &features);

    ~Instance();
    static SharedInstance
    create(const std::vector<SharedFeature> &features);
    static SharedInstance
    create(VkInstance vkInstance,
           const std::vector<SharedFeature> &features);

    SharedDevice
    createDevice(std::vector<QueueRequest> const &queues,
                 ISelectionStrategy const &gpuSelectionStrategy);
    SharedDevice createDevice(std::vector<QueueRequest> const &queues,
                              IGroupAssemblyStrategy const &groupAssembly);
    SharedDevice
    createDevice(VkPhysicalDevice phyDeviceHandle,
                 VkDevice logicalDeviceHandle,
                 const std::vector<QueueFamilyInfo> &familyInfos);

    vk::Instance handle() { return mInstance.get(); }

    std::vector<vk::PhysicalDevice> enumeratePhysicalDevices()
    {
        return mInstance->enumeratePhysicalDevices();
    }

    std::vector<SharedFeature> const &features() const { return mFeatures; }

  protected:
    vk::UniqueInstance mInstance;
    std::vector<SharedFeature> mFeatures;

  private:
    bool mIsOwnDeviceHandle = false;
};

} // namespace lava
