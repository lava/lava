#include "CommandBuffer.hh"
#include <lava/common/FormatInfo.hh>
#include <lava/common/log.hh>
#include <lava/objects/ComputePipeline.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Framebuffer.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/PipelineLayout.hh>
#include <lava/objects/Queue.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/raii/ActiveRenderPass.hh>

namespace lava
{

namespace
{
// Count the number of command buffers currently recording, so we can warn
// if a convenience command buffer is created while another one is recording
thread_local uint32_t sRecordingBufferCount = 0;
} // namespace

CommandBuffer::CommandBuffer() {}

void
CommandBuffer::submitAndWait()
{
    vk::PipelineStageFlags waitStages =
        vk::PipelineStageFlagBits::eAllCommands;

    auto cmdbuff = mHandle;
    vk::SubmitInfo info;
    info.commandBufferCount = 1;
    info.pCommandBuffers = &cmdbuff;
    info.signalSemaphoreCount = (uint32_t)mSignalSemaphores.size();
    info.pSignalSemaphores = mSignalSemaphores.data();
    info.waitSemaphoreCount = (uint32_t)mWaitSemaphores.size();
    info.pWaitSemaphores = mWaitSemaphores.data();
    info.pWaitDstStageMask = &waitStages;

    mQueue->handle().submit({info}, {});

    try {
        mQueue->handle().waitIdle();
    } catch (vk::Error const &e) {
        std::cerr << "Exception in CommandBuffer::submitAndWait(): "
                  << e.what() << std::endl;
        throw;
    }
}

void
CommandBuffer::submit()
{
    mQueue->submit(shared_from_this(), mWaitSemaphores, mWaitStages,
                   mSignalSemaphores);
}

RecordingCommandBuffer
CommandBuffer::begin()
{
    return {shared_from_this()};
}

CommandBuffer::CommandBuffer(Queue *queue, vk::CommandBufferLevel level)
    : mLevel(level), mQueue(queue)
{
    vk::CommandBufferAllocateInfo info;
    info.setCommandBufferCount(1);
    info.setCommandPool(queue->pool());
    info.setLevel(level);

    mHandle = queue->device()->handle().allocateCommandBuffers(info)[0];
}

CommandBuffer::~CommandBuffer()
{
    if (!mHandle)
        return;
    mQueue->device()->handle().freeCommandBuffers(mQueue->pool(),
                                                  {mHandle});
}

RecordingCommandBuffer::RecordingCommandBuffer(RecordingCommandBuffer &&o)
{
    mBuffer = o.mBuffer;
    o.mBuffer.reset();
    mAutoSubmit = o.mAutoSubmit;
    o.mAutoSubmit = false;
}

RecordingCommandBuffer &
RecordingCommandBuffer::operator=(RecordingCommandBuffer &&o)
{
    std::swap(mBuffer, o.mBuffer);
    std::swap(mAutoSubmit, o.mAutoSubmit);
    return *this;
}

RecordingCommandBuffer::~RecordingCommandBuffer()
{
    // Buffer could be null in the "moved from" state.
    if (mBuffer) {
        sRecordingBufferCount--;
        mBuffer->handle().end();

        if (mAutoSubmit) {
            mBuffer->submit();
        }
    }
}

ActiveRenderPass
RecordingCommandBuffer::beginRenderpass(const RenderPassBeginInfo &info)
{
    return {*this, info};
}

ActiveRenderPass
RecordingCommandBuffer::beginRenderpass(const SharedFramebuffer &fbo)
{
    RenderPassBeginInfo info;
    info.setRenderPass(fbo->pass());
    info.setFramebuffer(fbo);
    info.setRenderArea({{0, 0}, {fbo->width(), fbo->height()}});

    auto const &clear = fbo->pass()->mClearValues;
    info.setClearValues(clear.data(), uint32_t(clear.size()));

    return {*this, info};
}

void
RecordingCommandBuffer::bindDescriptorSets(
    const std::vector<SharedDescriptorSet> &descriptors,
    const std::vector<uint32_t> &offsets, uint32_t firstSet,
    vk::PipelineBindPoint bindPoint, const SharedPipelineLayout &layout)
{
    std::vector<vk::DescriptorSet> sets;
    auto handleOf = [](SharedDescriptorSet const &d) {
        return d->handle();
    };
    std::transform(begin(descriptors), end(descriptors),
                   back_inserter(sets), handleOf);
    vk::PipelineLayout pipLayout;
    if (layout) {
        pipLayout = layout->handle();
    } else {
        assert(mLastLayout);
        pipLayout = mLastLayout->handle();
    }

    for (auto const &desc : descriptors)
        buffer()->attachResource(desc);
    mBuffer->handle().bindDescriptorSets(bindPoint, pipLayout, firstSet,
                                         sets, offsets);
}

void
RecordingCommandBuffer::bindDescriptorSets(
    const std::vector<SharedDescriptorSet> &descriptors, uint32_t firstSet,
    vk::PipelineBindPoint bindPoint, const SharedPipelineLayout &layout)
{
    std::vector<vk::DescriptorSet> sets;
    auto handleOf = [](SharedDescriptorSet const &d) {
        return d->handle();
    };
    std::transform(begin(descriptors), end(descriptors),
                   back_inserter(sets), handleOf);
    vk::PipelineLayout pipLayout;
    if (layout) {
        pipLayout = layout->handle();
    } else {
        assert(mLastLayout);
        pipLayout = mLastLayout->handle();
    }

    for (auto const &desc : descriptors)
        attachResource(desc);
    mBuffer->handle().bindDescriptorSets(bindPoint, pipLayout, firstSet,
                                         sets, {});
}

void
RecordingCommandBuffer::pushConstants(uint32_t dataSize, const void *data,
                                      uint32_t offset,
                                      vk::ShaderStageFlags stageFlags,
                                      const SharedPipelineLayout &layout)
{
    vk::PipelineLayout pipLayout;
    if (layout) {
        pipLayout = layout->handle();
    } else {
        assert(mLastLayout);
        pipLayout = mLastLayout->handle();
    }

    mBuffer->handle().pushConstants(pipLayout, stageFlags, offset, dataSize,
                                    data);
}

void
RecordingCommandBuffer::pushConstantBlock(uint32_t size, const void *data)
{
    auto const &layout = mLastLayout;
    (void)layout;
    assert(layout->createInfo().ranges().size() == 1 &&
           "Can only use pushConstantBlock with a single push constant "
           "range.");
    assert(layout->createInfo().ranges().front().size == size &&
           "The size of the constant block doesn't match the one in the "
           "pipeline layout");
    pushConstants(size, data, 0, vk::ShaderStageFlagBits::eAll,
                  mLastLayout);
}

void
RecordingCommandBuffer::bindPipeline(const SharedComputePipeline &pip)
{
    mLastLayout = pip->layout();
    mBuffer->handle().bindPipeline(vk::PipelineBindPoint::eCompute,
                                   pip->handle());
    attachResource(pip);
}

void
RecordingCommandBuffer::dispatch(uint32_t x, uint32_t y, uint32_t z)
{
    mBuffer->handle().dispatch(x, y, z);
}

// this is controlled by the CMake variable with the same name
#ifdef LAVA_EMIT_CONCURRENT_COMMANDBUFFER_WARNING
void
RecordingCommandBuffer::convenienceBufferCheck(const char *method)
{
    if (sRecordingBufferCount > 0) {
        lava::warning() << "You're calling the convenience variant of "
                        << method
                        << ", which creates, records and submits its own "
                           "CommandBuffer, while another CommandBuffer is "
                           "currently recording commands.";
    }
}
#else
void
RecordingCommandBuffer::convenienceBufferCheck()
{
}

#endif

RecordingCommandBuffer::RecordingCommandBuffer(
    const SharedCommandBuffer &buffer)
    : mBuffer(buffer)
{

    sRecordingBufferCount++;
    vk::CommandBufferBeginInfo info;
    buffer->handle().begin(&info);
}
} // namespace lava
