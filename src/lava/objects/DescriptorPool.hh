#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/createinfos/DescriptorPoolCreateInfo.hh>
#include <lava/objects/Device.hh>

namespace lava
{
class DescriptorPool : public std::enable_shared_from_this<DescriptorPool>
{
  public:
    LAVA_NON_MOVABLE(DescriptorPool);

    DescriptorPool(SharedDevice const &device,
                   DescriptorPoolCreateInfo const &info);
    ~DescriptorPool();

    SharedDescriptorSet
    createDescriptorSet(SharedDescriptorSetLayout const &layout);

    vk::DescriptorPool handle() const { return mHandle; }
    SharedDevice const &device() const { return mDevice; }

  protected:
    SharedDevice mDevice;
    DescriptorPoolCreateInfo mCreateInfo;
    vk::DescriptorPool mHandle;
};

} // namespace lava
