#pragma once
#include "ISelectionStrategy.hh"

namespace lava
{

class NthOfTypeStrategy : public ISelectionStrategy
{
  public:
    ~NthOfTypeStrategy() override {}
    NthOfTypeStrategy(vk::PhysicalDeviceType type, size_t n = 0)
        : mType(type), mN(n)
    {
    }

    vk::PhysicalDevice
    selectFrom(std::vector<vk::PhysicalDevice> const &phys) const override;

  protected:
    vk::PhysicalDeviceType mType;
    size_t mN;
};

} // namespace lava
