#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{

class ISelectionStrategy
{
  public:
    virtual ~ISelectionStrategy() {}
    virtual vk::PhysicalDevice
    selectFrom(std::vector<vk::PhysicalDevice> const &phys) const = 0;
};

} // namespace lava
