#include "NthOfTypeStrategy.hh"

namespace lava
{

vk::PhysicalDevice
NthOfTypeStrategy::selectFrom(
    const std::vector<vk::PhysicalDevice> &phys) const
{
    size_t counter = 0u;

    for (size_t i = 0; i < phys.size(); i++) {
        auto dev = phys[i];
        auto props = dev.getProperties();
        if (props.deviceType == mType) {
            if (counter++ == mN)
                return dev;
        }
    }

    return {};
}

} // namespace lava
