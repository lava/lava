#pragma once

#include <vector>

#include <vulkan/vulkan.hpp>

#include "IGroupAssemblyStrategy.hh"

namespace lava
{

class NthGroupStrategy : public IGroupAssemblyStrategy
{
  public:
    NthGroupStrategy(int n) : mN(n) {}
    ~NthGroupStrategy() override {}
    std::vector<vk::PhysicalDevice> assembleFrom(
        std::vector<vk::PhysicalDeviceGroupProperties> const &groups)
        const override;

  protected:
    int mN;
};

} // namespace lava
