#pragma once

namespace lava
{

class IGroupAssemblyStrategy
{
  public:
    virtual ~IGroupAssemblyStrategy() {}
    virtual std::vector<vk::PhysicalDevice> assembleFrom(
        std::vector<vk::PhysicalDeviceGroupProperties> const &groups)
        const = 0;
};

} // namespace lava
