#include "NthGroupStrategy.hh"

namespace lava
{

std::vector<vk::PhysicalDevice>
NthGroupStrategy::assembleFrom(
    const std::vector<vk::PhysicalDeviceGroupProperties> &groups) const
{
    assert(int(groups.size()) > mN);
    auto const &group = groups[mN];
    return {std::begin(group.physicalDevices),
            std::end(group.physicalDevices)};
}

} // namespace lava
