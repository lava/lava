#include "DescriptorIndexing.hh"

namespace lava
{
namespace features
{

DescriptorIndexing::DescriptorIndexing() {}

DescriptorIndexing::~DescriptorIndexing() {}

std::vector<const char *>
DescriptorIndexing::deviceExtensions()
{
    return {VK_KHR_MAINTENANCE3_EXTENSION_NAME,
            VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME};
}

void
DescriptorIndexing::onPhysicalDeviceSelected(vk::PhysicalDevice dev)
{
    vk::PhysicalDeviceFeatures2 feats;
    feats.setPNext(&mFeatures);
    dev.getFeatures2(&feats);
}

void
DescriptorIndexing::addPhysicalDeviceFeatures2(
    IFeature::NextPtr &next) const
{
    auto *nextVal = &next;
    while (*nextVal != nullptr) {
        nextVal = &(*nextVal)->next;
    }
    (*nextVal) = reinterpret_cast<VkStructHeader *>(&mFeatures);
    (*nextVal)->next = nullptr;
}

DescriptorIndexing &
DescriptorIndexing::disableUpdateAfterBinding()
{
    mFeatures.descriptorBindingUpdateUnusedWhilePending = false;
    mFeatures.descriptorBindingSampledImageUpdateAfterBind = false;
    mFeatures.descriptorBindingStorageImageUpdateAfterBind = false;
    mFeatures.descriptorBindingStorageBufferUpdateAfterBind = false;
    mFeatures.descriptorBindingUniformBufferUpdateAfterBind = false;
    mFeatures.descriptorBindingStorageTexelBufferUpdateAfterBind = false;
    mFeatures.descriptorBindingUniformTexelBufferUpdateAfterBind = false;
    return *this;
}

DescriptorIndexing &
DescriptorIndexing::disableDynamicArrayIndexing()
{
    mFeatures.shaderInputAttachmentArrayDynamicIndexing = false;
    mFeatures.shaderStorageTexelBufferArrayDynamicIndexing = false;
    mFeatures.shaderStorageBufferArrayNonUniformIndexing = false;
    return *this;
}

DescriptorIndexing &
DescriptorIndexing::disableNonUniformArrayIndexing()
{
    mFeatures.shaderSampledImageArrayNonUniformIndexing = false;
    mFeatures.shaderStorageImageArrayNonUniformIndexing = false;
    mFeatures.shaderStorageBufferArrayNonUniformIndexing = false;
    mFeatures.shaderUniformBufferArrayNonUniformIndexing = false;
    mFeatures.shaderInputAttachmentArrayNonUniformIndexing = false;
    mFeatures.shaderStorageTexelBufferArrayNonUniformIndexing = false;
    mFeatures.shaderUniformTexelBufferArrayNonUniformIndexing = false;
    return *this;
}

} // namespace features
} // namespace lava
