#pragma once
#ifdef LAVA_GLFW_AVAILABLE
#include "IFeature.hh"
#include <functional>
#include <lava/common/NoCopy.hh>

struct GLFWwindow;

namespace lava
{

namespace features
{

class Context;

/**
 * @brief The GlfwWindow class encapsulates a window opened through GLFW.
 * If you get any linker errors regarding this class, make sure glfw is
 * available as target
 */
class GlfwWindow
{
  public:
    class Frame;
    using SwapchainBuildHandler =
        std::function<void(std::vector<SharedImageView>)>;

    void
    buildSwapchainWith(const GlfwWindow::SwapchainBuildHandler &handler);

    /// index of the image that should be drawn to next
    uint32_t nextIndex();

    /// wait for this semaphore before drawing to the current image
    vk::Semaphore imageReady() const { return mImageReady; }

    /// signal this semaphore when the image is ready to be presented
    vk::Semaphore renderingComplete() const
    {
        mRenderingCompleteCalled = true;
        return mRenderingComplete;
    }

    /// the Format of the images in the swapchain.
    /// Make sure the attachment format in your Framebuffers matches this.
    vk::Format format() { return mChainFormat.format; }

    uint32_t width() const { return mWidth; }
    uint32_t height() const { return mHeight; }

    /// Call this when window is resized
    void onResize(uint32_t w, uint32_t h);

    /// Don't use this directly, use openWindow in GlfwOutput instead
    GlfwWindow(SharedDevice device, vk::SurfaceFormatKHR format,
               uint32_t width, uint32_t height, bool resizable,
               char const *title);
    ~GlfwWindow();

    /// Call this before you start rendering to the window.
    /// Use the included index to select the right FBO to render to.
    /// Automatically presents the image when the return value goes
    /// out-of-scope
    Frame startFrame();

    class Frame
    {
      public:
        LAVA_RAII_CLASS(Frame);
        Frame(Frame &&rhs);
        ~Frame();

        uint32_t imageIndex() const { return window->mPresentIndex; }
        vk::Semaphore imageReady() const { return window->mImageReady; }
        vk::Semaphore renderingComplete() const
        {
            return window->mRenderingComplete;
        }

        SharedImage const &image() const
        {
            return window->mChainImages[window->mPresentIndex];
        }

      private:
        Frame(GlfwWindow *parent);
        GlfwWindow *window;
        friend class GlfwWindow;
    };

    GLFWwindow *window() { return mWindow; }
    GLFWwindow const *window() const { return mWindow; }

    vk::SurfaceKHR const &surface() const { return mSurface; }
    vk::SurfaceFormatKHR const &surfaceFormat() const
    {
        return mChainFormat;
    }
    vk::SwapchainCreateInfoKHR const &swapchainInfo() const
    {
        return mSwapchainInfo;
    }
    size_t swapchainImageCount() const { return mChainImages.size(); }
    std::vector<SharedImage> const &chainImages() const
    {
        return mChainImages;
    }
    std::vector<SharedImageView> const &chainViews() const
    {
        return mChainViews;
    }

    bool shouldClose() const;

  protected:
    void buildSwapchain();

    SharedDevice mDevice;
    vk::SurfaceFormatKHR mChainFormat;

    uint32_t mWidth;
    uint32_t mHeight;
    bool mResizable;
    std::string mTitle;
    GLFWwindow *mWindow;
    vk::SurfaceKHR mSurface;
    vk::SwapchainKHR mChain;

    vk::Semaphore mImageReady;
    vk::Semaphore mRenderingComplete;
    mutable bool mRenderingCompleteCalled = false;

    uint32_t mPresentIndex;
    lava::Queue *mQueue;

    vk::UniqueCommandBuffer mCopyCmdBuffer;
    vk::UniqueCommandBuffer mPresentCmdBuffer;

    std::vector<SharedImage> mChainImages;
    std::vector<SharedImageView> mChainViews;
    SwapchainBuildHandler mSwapchainHandler;

    std::vector<vk::PresentModeKHR> mModePriority;

    vk::SwapchainCreateInfoKHR mSwapchainInfo;

    friend class Frame;
};
} // namespace features
} // namespace lava

#else
#warning "GlfwWindow.hh was included, but GLFW isn't available."
#endif
