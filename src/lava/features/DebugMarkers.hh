#pragma once
#include "IFeature.hh"

namespace lava
{
namespace features
{

class DebugMarkers : public IFeature
{
  public:
    virtual ~DebugMarkers();
    DebugMarkers();
    static std::shared_ptr<DebugMarkers> create()
    {
        return std::make_shared<DebugMarkers>();
    }

    std::vector<const char *> deviceExtensions() override;
    void onLogicalDeviceCreated(SharedDevice const &device) override;

    void mark(SharedImage const &image, const char *name);
    void mark(SharedBuffer const &buffer, const char *name);

    template <class Resource>
    void mark(Resource const &res, std::string const &name)
    {
        mark(res, name.c_str());
    }

    template <class Resource>
    void mark(Resource const &res, const char *name)
    {
        static_assert(
            !sizeof(Resource),
            "specialization of mark for this type not implemented.");
    }

  protected:
    SharedDevice mDevice;
};
} // namespace features
} // namespace lava
