#include "Validation.hh"
#include <iostream>
#include <lava/common/log.hh>
#include <lava/objects/Instance.hh>

namespace
{
static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugReportFlagsEXT flags,
              VkDebugReportObjectTypeEXT objType, uint64_t obj,
              size_t location, int32_t code, const char *layerPrefix,
              const char *msg, void *userData)
{
    auto validation =
        reinterpret_cast<lava::features::Validation *>(userData);

    if (validation->paused())
        return VK_FALSE;

    validation->bumpMessageCount();
    lava::validation() << msg;

    return VK_FALSE;
}
} // namespace

namespace lava
{
namespace features
{

Validation::~Validation()
{
    if (mCallback) {
        mInstance.destroyDebugReportCallbackEXT(mCallback);
        mCallback = vk::DebugReportCallbackEXT{};
    }
}

std::vector<const char *>
Validation::layers()
{
    return {"VK_LAYER_KHRONOS_validation"};
}

std::vector<const char *>
Validation::instanceExtensions()
{
    return {"VK_EXT_debug_report"};
}

void
Validation::onInstanceCreated(Instance *instance)
{
    mInstance = instance->handle();

    vk::DebugReportCallbackCreateInfoEXT debug;
    debug.setPfnCallback(debugCallback);
    debug.setFlags(mFlags);
    debug.pUserData = this;
    mCallback = mInstance.createDebugReportCallbackEXT(debug);
}

void
Validation::beforeInstanceDestruction()
{
    if (mCallback) {
        mInstance.destroyDebugReportCallbackEXT(mCallback);
        mCallback = vk::DebugReportCallbackEXT{};
    }
}
} // namespace features
} // namespace lava
