#pragma once
#include "IFeature.hh"

namespace lava
{
namespace features
{

class MultiView : public IFeature
{
  public:
    virtual ~MultiView();
    static std::shared_ptr<MultiView> create()
    {
        return std::make_shared<MultiView>();
    }

    bool supportsDevice(vk::PhysicalDevice device) const override;

    std::vector<const char *> deviceExtensions() override;

    std::vector<const char *> instanceExtensions() override;

    void addPhysicalDeviceFeatures(
        vk::PhysicalDeviceFeatures &features) const override;
    void addPhysicalDeviceFeatures2(NextPtr &next) const override;

    void requireGeometryShader(bool val = true)
    {
        mRequireGeometryShader = val;
    }

    void requireTesselationShader(bool val = true)
    {
        mRequireTesselationShader = val;
    }

    void requirePerViewAttributes(bool val = true)
    {
        mRequirePerViewAttributes = val;
    }

    /// Without this, the positions for different viewports may only differ
    /// in their X component (sufficient for single-pass stereo on most
    /// HMDs)
    void requirePerViewAttributesAllComponents(bool val = true)
    {
        mRequirePerViewAttributes |= val;
        mRequirePerViewAllComponents = val;
    }

  protected:
    bool mRequireGeometryShader = false;
    bool mRequireTesselationShader = false;
    bool mRequirePerViewAttributes = false;
    bool mRequirePerViewAllComponents = false;

    mutable vk::PhysicalDeviceMultiviewFeaturesKHR mMultiviewFeatures;
};
} // namespace features
} // namespace lava
