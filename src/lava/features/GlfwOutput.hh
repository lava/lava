#pragma once
#ifdef LAVA_GLFW_AVAILABLE
#include "GlfwWindow.hh"
#include "IFeature.hh"

struct GLFWwindow;

namespace lava
{

namespace features
{

class GlfwOutput : public IFeature
{
  public:
    static std::shared_ptr<GlfwOutput> create()
    {
        return std::make_shared<GlfwOutput>();
    }

    GlfwOutput();

    std::shared_ptr<GlfwWindow>
    openWindow(uint32_t width = 800, uint32_t height = 600,
               bool resizable = false, char const *title = "Lava Window");

    /* IFeature overrides */

    std::vector<const char *> instanceExtensions() override;

    void onInstanceCreated(Instance *instance) override;

    std::vector<const char *> deviceExtensions() override;
    void onPhysicalDeviceSelected(vk::PhysicalDevice phy) override;
    std::vector<QueueRequest> queueRequests(
        std::vector<vk::QueueFamilyProperties> const &families) override;
    bool supportsDevice(vk::PhysicalDevice dev) const override;

    void onLogicalDeviceCreated(SharedDevice const &device) override;

    vk::Format format() const { return mChainFormat.format; }

    void pollEvents();

  protected:
    lava::Instance *mInstance;
    lava::Device *mDevice;

    GLFWwindow *mTempWindow;
    vk::SurfaceKHR mTempSurface;

    vk::SurfaceFormatKHR mChainFormat;

    vk::PhysicalDevice mPhysicalDevice;
    uint32_t mPresentIndex;
};
} // namespace features
} // namespace lava

#else
#warning "GlfwOutput.hh was included, but GLFW isn't available."
#endif
