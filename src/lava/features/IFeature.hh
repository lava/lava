#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>
#include <memory>
#include <vector>

namespace lava
{
class GenericContext;
struct QueueRequest;

namespace features
{
class IFeature
{
  public:
    virtual ~IFeature() = default;

    /// implement these to allow for optional extensions/layers
    virtual std::vector<const char *>
    layers(std::vector<const char *> const &available);
    virtual std::vector<const char *>
    instanceExtensions(std::vector<const char *> const &available);
    virtual std::vector<const char *>
    deviceExtensions(std::vector<const char *> const &available);

    /// implement these if you only have hard requirements, the above fall
    /// back on these.
    virtual std::vector<const char *> layers() { return {}; }
    virtual std::vector<const char *> instanceExtensions() { return {}; }
    virtual std::vector<const char *> deviceExtensions() { return {}; }

    virtual bool supportsDevice(vk::PhysicalDevice) const { return true; }

    virtual std::vector<QueueRequest>
    queueRequests(std::vector<vk::QueueFamilyProperties> const &families);
    virtual void
    addPhysicalDeviceFeatures(vk::PhysicalDeviceFeatures &) const
    {
    }

    struct VkStructHeader {
        vk::StructureType type;
        VkStructHeader *next;
    };
    using NextPtr = VkStructHeader *;
    virtual void addPhysicalDeviceFeatures2(NextPtr &) const {}

    virtual void onInstanceCreated(Instance *instance) {}
    virtual void onPhysicalDeviceSelected(vk::PhysicalDevice phy) {}
    virtual void onLogicalDeviceCreated(SharedDevice const &device) {}
    virtual void beforeDeviceDestruction() {}
    virtual void beforeInstanceDestruction() {}
};

using SharedFeature = std::shared_ptr<IFeature>;
} // namespace features
} // namespace lava
