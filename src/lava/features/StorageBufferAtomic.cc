#include "StorageBufferAtomic.hh"
#include <lava/objects/Device.hh>
#include <lava/objects/Instance.hh>

namespace lava
{
namespace features
{

StorageBufferAtomic::~StorageBufferAtomic() {}

bool
StorageBufferAtomic::supportsDevice(vk::PhysicalDevice device) const
{
    vk::PhysicalDeviceFeatures temp = {};
    temp = device.getFeatures();
    return temp.fragmentStoresAndAtomics &&
           temp.vertexPipelineStoresAndAtomics;
}

std::vector<const char *>
StorageBufferAtomic::deviceExtensions()
{
    return {};
}

std::vector<const char *>
StorageBufferAtomic::instanceExtensions()
{
    return {};
}

void
StorageBufferAtomic::onInstanceCreated(lava::Instance *instance)
{
    mInstance = instance;
}

void
StorageBufferAtomic::onLogicalDeviceCreated(
    const lava::SharedDevice &device)
{
}

void
StorageBufferAtomic::addPhysicalDeviceFeatures(
    vk::PhysicalDeviceFeatures &feat) const
{
    feat.vertexPipelineStoresAndAtomics = true;
    feat.fragmentStoresAndAtomics = true;
}

} // namespace features
} // namespace lava
