#include "RayTracing.hh"
#include <lava/objects/Device.hh>
#include <lava/objects/Instance.hh>

namespace lava
{
namespace features
{

RayTracing::~RayTracing() {}

bool
RayTracing::supportsDevice(vk::PhysicalDevice device) const
{
    auto props =
        device.getProperties2<vk::PhysicalDeviceProperties2,
                              vk::PhysicalDeviceRayTracingPropertiesNV>();
    auto rtprop = props.get<vk::PhysicalDeviceRayTracingPropertiesNV>();

    return rtprop.maxGeometryCount;
}

std::vector<const char *>
RayTracing::deviceExtensions()
{
    return {VK_NV_RAY_TRACING_EXTENSION_NAME,
            VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME};
}

std::vector<const char *>
RayTracing::instanceExtensions()
{
    return {VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME};
}

void
RayTracing::onInstanceCreated(lava::Instance *instance)
{
    mInstance = instance;
}

void
RayTracing::onLogicalDeviceCreated(const lava::SharedDevice &device)
{
    device->handle().getProcAddr("vkCreateAccelerationStructureNVX");
}

void
RayTracing::addPhysicalDeviceFeatures(
    vk::PhysicalDeviceFeatures &feat) const
{
    feat.vertexPipelineStoresAndAtomics = true;
}

} // namespace features
} // namespace lava
