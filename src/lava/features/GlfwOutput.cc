#ifdef LAVA_GLFW_AVAILABLE

#include "GlfwOutput.hh"
#include <GLFW/glfw3.h>
#include <lava/objects/Device.hh>
#include <lava/objects/Instance.hh>

namespace lava
{
namespace features
{

GlfwOutput::GlfwOutput()
{
    if (!glfwInit()) {
        const char *error;
        glfwGetError(&error);
        throw std::runtime_error(std::string{"Could initialize GLFW: "} +
                                 error);
    }
}

std::shared_ptr<GlfwWindow>
GlfwOutput::openWindow(uint32_t width, uint32_t height, bool resizable,
                       const char *title)
{

    return std::make_shared<GlfwWindow>(mDevice->shared_from_this(),
                                        mChainFormat, width, height,
                                        resizable, title);
}

std::vector<const char *>
lava::features::GlfwOutput::instanceExtensions()
{
    std::vector<const char *> results;
    uint32_t count;
    const char **extensions = glfwGetRequiredInstanceExtensions(&count);
    for (uint32_t i = 0; i < count; i++)
        results.push_back(extensions[i]);
    return results;
}

void
GlfwOutput::onInstanceCreated(Instance *instance)
{
    mInstance = instance;

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    mTempWindow = glfwCreateWindow(1, 1, "", 0, 0);

    if (!mTempWindow) {
        const char *error;
        glfwGetError(&error);
        throw std::runtime_error(
            std::string{"Could not create temporary window: "} + error);
    }

    VkSurfaceKHR surf;
    glfwCreateWindowSurface(instance->handle(), mTempWindow, nullptr,
                            &surf);
    mTempSurface = surf;
    if (!mTempSurface)
        throw std::runtime_error("Could not create temporary Surface");
}

std::vector<const char *>
GlfwOutput::deviceExtensions()
{
    return {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
}

static vk::SurfaceFormatKHR
bestFormat(vk::PhysicalDevice dev, vk::SurfaceKHR surf)
{
    auto formats = dev.getSurfaceFormatsKHR(surf);

    if (formats.size() == 1 &&
        formats[0].format == vk::Format::eUndefined) {
        vk::SurfaceFormatKHR format;
        format.format = vk::Format::eR8G8B8A8Srgb;
        format.colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;
        return format;
    }

    for (auto &&f : formats)
        if (f.format == vk::Format::eB8G8R8A8Srgb ||
            f.format == vk::Format::eR8G8B8A8Srgb ||
            f.format == vk::Format::eB8G8R8Srgb ||
            f.format == vk::Format::eR8G8B8Srgb)
            return f;

    for (auto &&f : formats)
        if (f.format == vk::Format::eB8G8R8A8Unorm ||
            f.format == vk::Format::eR8G8B8A8Unorm ||
            f.format == vk::Format::eB8G8R8Unorm ||
            f.format == vk::Format::eR8G8B8Unorm)
            return f;

    throw std::runtime_error("No suitable format found");
}

void
GlfwOutput::onPhysicalDeviceSelected(vk::PhysicalDevice phy)
{
    mPhysicalDevice = phy;

    mChainFormat = bestFormat(phy, mTempSurface);
}

std::vector<QueueRequest>
GlfwOutput::queueRequests(
    const std::vector<vk::QueueFamilyProperties> &families)
{
    std::vector<QueueRequest> result;
    for (uint32_t i = 0; i < uint32_t(families.size()); i++) {
        if (mPhysicalDevice.getSurfaceSupportKHR(i, mTempSurface)) {
            mPresentIndex = i;
            result.push_back(QueueRequest::byFamily("present", i, 1.0f));
            break;
        }
    }
    if (result.empty())
        throw std::runtime_error("Device can't present to this surface.");

    return result;
}

bool
GlfwOutput::supportsDevice(vk::PhysicalDevice dev) const
{
    auto families = dev.getQueueFamilyProperties();
    for (uint32_t i = 0; i < families.size(); i++) {
        if (dev.getSurfaceSupportKHR(i, mTempSurface))
            return true;
    }
    return false;
}

void
GlfwOutput::onLogicalDeviceCreated(const SharedDevice &device)
{
    mDevice = device.get();
}

void
GlfwOutput::pollEvents()
{
    glfwPollEvents();
}

} // namespace features
} // namespace lava

#endif
