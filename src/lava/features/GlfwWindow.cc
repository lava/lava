#ifdef LAVA_GLFW_AVAILABLE

#include "GlfwWindow.hh"
#include <GLFW/glfw3.h>
#include <lava/common/FormatInfo.hh>
#include <lava/common/log.hh>
#include <lava/createinfos/Images.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/ImageView.hh>
#include <lava/objects/Instance.hh>

namespace lava
{
namespace features
{

GlfwWindow::~GlfwWindow()
{

    mChainViews.clear();
    if (mImageReady)
        mDevice->handle().destroySemaphore(mImageReady);
    if (mRenderingComplete)
        mDevice->handle().destroySemaphore(mRenderingComplete);

    if (mChain)
        mDevice->handle().destroySwapchainKHR(mChain);

    mDevice->instance()->handle().destroySurfaceKHR(mSurface);

    if (mWindow)
        glfwDestroyWindow(mWindow);
}

GlfwWindow::Frame
GlfwWindow::startFrame()
{
    assert(mChain &&
           "You need to provide a handler for swapchain creation.");

    while (true) {
        // TODO: count retries, risk of infinite loop!
        // TODO: consolidate into abstract window base class? ->
        // DisplayWindow
        try {
            auto result = mDevice->handle().acquireNextImageKHR(
                mChain, 1e9, mImageReady, {});

            if (result.result == vk::Result::eTimeout ||
                result.result == vk::Result::eNotReady) {
                lava::error() << "GlfwWindow::startFrame(): "
                                 "acquireNextImage timed out (>1s)";
                continue;
            }
            if (result.result == vk::Result::eSuboptimalKHR) {
                mDevice->handle().waitIdle();
                buildSwapchain();
                continue;
            }

            mPresentIndex = result.value;
        } catch (vk::OutOfDateKHRError) {
            mDevice->handle().waitIdle();
            buildSwapchain();
            continue;
        }

        return {this};
    }
}

bool
GlfwWindow::shouldClose() const
{
    return glfwWindowShouldClose(mWindow);
}

GlfwWindow::Frame::Frame(GlfwWindow::Frame &&rhs) : window(rhs.window)
{
    rhs.window = nullptr;
}

GlfwWindow::Frame::~Frame()
{
    if (!window)
        return;

    vk::PresentInfoKHR info;
    info.pImageIndices = &window->mPresentIndex;
    info.pSwapchains = &window->mChain;
    info.swapchainCount = 1;
    info.pWaitSemaphores = &window->mRenderingComplete;
    info.waitSemaphoreCount = 1;

    try {
        window->mQueue->handle().presentKHR(info);
    } catch (vk::OutOfDateKHRError const &) {
        window->mDevice->handle().waitIdle();
        window->buildSwapchain();
    }
}

GlfwWindow::Frame::Frame(GlfwWindow *parent) : window(parent) {}

void
GlfwWindow::buildSwapchainWith(
    const GlfwWindow::SwapchainBuildHandler &handler)
{
    mSwapchainHandler = handler;
    buildSwapchain();
}

void
GlfwWindow::onResize(uint32_t w, uint32_t h)
{
    mWidth = w;
    mHeight = h;
    buildSwapchain();
}

static vk::PresentModeKHR
bestMode(vk::PhysicalDevice dev, vk::SurfaceKHR surf)
{
    std::vector<vk::PresentModeKHR> prios = {
        vk::PresentModeKHR::eMailbox,
        // Fifo-Modes are bugged on nvidia currently, so rather accept
        // tearing than the whole system freezing
        vk::PresentModeKHR::eImmediate,
        vk::PresentModeKHR::eFifo,
        vk::PresentModeKHR::eFifoRelaxed,
    };

    auto modes = dev.getSurfacePresentModesKHR(surf);
    for (auto target : prios) {
        for (auto candidate : modes) {
            if (target == candidate)
                return target;
        }
    }
    return modes[0];
}

GlfwWindow::GlfwWindow(SharedDevice device, vk::SurfaceFormatKHR format,
                       uint32_t width, uint32_t height, bool resizable,
                       const char *title)
    : mDevice(device), mChainFormat(format), mWidth(width), mHeight(height),
      mResizable(resizable), mTitle(title)
{

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, mResizable ? GLFW_TRUE : GLFW_FALSE);
    mWindow = glfwCreateWindow(mWidth, mHeight, mTitle.c_str(), 0, 0);
    mQueue = &device->namedQueue("present");

    if (!mWindow) {
        const char *error;
        glfwGetError(&error);
        throw std::runtime_error(
            std::string{"Could not create GLFW window: "} + error);
    }

    VkSurfaceKHR surf;
    glfwCreateWindowSurface(mDevice->instance()->handle(), mWindow, nullptr,
                            &surf);
    mSurface = surf;
    if (!mSurface)
        throw std::runtime_error("Could not create Surface");

    mImageReady = mDevice->handle().createSemaphore({});
    mRenderingComplete = mDevice->handle().createSemaphore({});
}

void
GlfwWindow::buildSwapchain()
{
    mChainViews.clear();
    mChainImages.clear();
    if (mChain) {
        mDevice->handle().destroySwapchainKHR(mChain);
    }

    auto dev = mDevice->physical();
    auto cap = dev.getSurfaceCapabilitiesKHR(mSurface);
    auto pres = bestMode(dev, mSurface);

    vk::SwapchainCreateInfoKHR info;
    info.surface = mSurface;
    info.minImageCount = cap.minImageCount;
    info.imageFormat = mChainFormat.format;
    info.imageColorSpace = mChainFormat.colorSpace;
    info.imageExtent = cap.currentExtent;
    info.imageArrayLayers = 1;
    info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment |
                      vk::ImageUsageFlagBits::eTransferDst;
    info.preTransform = cap.currentTransform;
    info.presentMode = pres;
    info.clipped = true;

    mWidth = info.imageExtent.width;
    mHeight = info.imageExtent.height;

    {
        auto supp = dev.getSurfaceSupportKHR(mQueue->family(), mSurface);
        if (!supp)
            throw std::runtime_error(
                "The selected queue family can't present to this device.");
    }

    if (mDevice->graphicsQueue().family() == mQueue->family()) {
        info.setImageSharingMode(vk::SharingMode::eExclusive);
    } else {
        uint32_t families[] = {uint32_t(mDevice->graphicsQueue().family()),
                               uint32_t(mQueue->family())};
        info.setImageSharingMode(vk::SharingMode::eConcurrent)
            .setPQueueFamilyIndices(families)
            .setQueueFamilyIndexCount(2);
    }

    mChain = mDevice->handle().createSwapchainKHR(info);
    {
        auto chainHandles = mDevice->handle().getSwapchainImagesKHR(mChain);
        auto imgCreateInfo =
            attachment2D(mWidth, mHeight, mChainFormat.format);
        for (vk::Image handle : chainHandles) {
            auto image = std::make_shared<Image>(
                mDevice, imgCreateInfo, handle, vk::ImageViewType::e2D);
            mChainViews.push_back(image->createView());
            mChainImages.push_back(move(image));
        }
    }

    mSwapchainHandler(mChainViews);
    mSwapchainInfo = info;
}
} // namespace features
} // namespace lava

#endif
