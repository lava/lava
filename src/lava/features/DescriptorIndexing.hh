#pragma once
#include "IFeature.hh"

namespace lava
{
namespace features
{

/**
 * Feature to enable Discriptor Indexing for a device.
 * (e.g. access an array of textures in a shader with a dynamic array)
 * By default, all available aspects of this extension are requested.
 * Disabling unneeded ones is optional.
 *
 * DynamicIndexing == index by a uniform constant
 * NonUniformIndexing == index by any variable
 */
class DescriptorIndexing : public IFeature
{
  public:
    DescriptorIndexing();
    ~DescriptorIndexing();
    static std::shared_ptr<DescriptorIndexing> create()
    {
        return std::make_shared<DescriptorIndexing>();
    }

    std::vector<const char *> deviceExtensions() override;

    void onPhysicalDeviceSelected(vk::PhysicalDevice dev) override;
    void addPhysicalDeviceFeatures2(NextPtr &) const override;

    vk::PhysicalDeviceDescriptorIndexingFeaturesEXT &features()
    {
        return mFeatures;
    }
    vk::PhysicalDeviceDescriptorIndexingFeaturesEXT const &features() const
    {
        return mFeatures;
    }

    /// Don't request allowing descriptors to be updated while bound.
    DescriptorIndexing &disableUpdateAfterBinding();

    /// Don't request allowing dynamic descriptor array indexing (for arrays
    /// of input attachments and storage (texel) buffers)
    DescriptorIndexing &disableDynamicArrayIndexing();

    /// Don't request allowing non-uniform descriptor array indexing.
    DescriptorIndexing &disableNonUniformArrayIndexing();

  protected:
    mutable vk::PhysicalDeviceDescriptorIndexingFeaturesEXT mFeatures;
};

} // namespace features
} // namespace lava
