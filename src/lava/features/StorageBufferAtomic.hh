#pragma once
#include <lava/features/IFeature.hh>
#include <lava/fwd.hh>

namespace lava
{
namespace features
{

class StorageBufferAtomic : public lava::features::IFeature
{
  public:
    virtual ~StorageBufferAtomic();
    static std::shared_ptr<StorageBufferAtomic> create()
    {
        return std::make_shared<StorageBufferAtomic>();
    }

    bool supportsDevice(vk::PhysicalDevice device) const override;

    std::vector<const char *> deviceExtensions() override;
    std::vector<const char *> instanceExtensions() override;

    void onInstanceCreated(lava::Instance *instance) override;
    void onLogicalDeviceCreated(lava::SharedDevice const &device) override;

    void
    addPhysicalDeviceFeatures(vk::PhysicalDeviceFeatures &) const override;

  protected:
    lava::Instance *mInstance;
};

} // namespace features
} // namespace lava
