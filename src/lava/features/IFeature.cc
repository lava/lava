#include "IFeature.hh"
#include <lava/objects/Queue.hh>

namespace lava
{
namespace features
{

std::vector<const char *>
IFeature::layers(const std::vector<const char *> &available)
{
    return layers();
}

std::vector<const char *>
IFeature::instanceExtensions(const std::vector<const char *> &available)
{
    return instanceExtensions();
}

std::vector<const char *>
IFeature::deviceExtensions(const std::vector<const char *> &available)
{
    return deviceExtensions();
}

std::vector<QueueRequest>
IFeature::queueRequests(
    const std::vector<vk::QueueFamilyProperties> &families)
{
    return {};
}
} // namespace features
} // namespace lava
