#include "DebugMarkers.hh"
#include <lava/common/log.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/Image.hh>

namespace lava
{
namespace features
{

DebugMarkers::~DebugMarkers() {}

DebugMarkers::DebugMarkers() {}

std::vector<const char *>
DebugMarkers::deviceExtensions()
{
    return {VK_EXT_DEBUG_MARKER_EXTENSION_NAME};
}

void
DebugMarkers::onLogicalDeviceCreated(const SharedDevice &device)
{
    mDevice = device;
}

void
DebugMarkers::mark(const SharedImage &image, char const *name)
{
    vk::DebugMarkerObjectNameInfoEXT ni;
    ni.objectType = vk::DebugReportObjectTypeEXT::eImage;
    ni.object = (uint64_t)VkImage(image->handle());
    ni.pObjectName = name;

    mDevice->handle().debugMarkerSetObjectNameEXT(ni);

    lava::debug() << "Image " << ni.object << " = " << name;
}

void
DebugMarkers::mark(const SharedBuffer &buffer, const char *name)
{
    vk::DebugMarkerObjectNameInfoEXT ni;
    ni.objectType = vk::DebugReportObjectTypeEXT::eBuffer;
    ni.object = (uint64_t)VkBuffer(buffer->handle());
    ni.pObjectName = name;

    mDevice->handle().debugMarkerSetObjectNameEXT(ni);

    lava::debug() << "Buffer " << ni.object << " = " << name;
}

} // namespace features
} // namespace lava
