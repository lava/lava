#include "MultiView.hh"
#include <lava/objects/Instance.hh>

namespace lava
{
namespace features
{

namespace
{
struct VkStruct {
    vk::StructureType type;
    void *next;
};
} // namespace

MultiView::~MultiView() {}

bool
MultiView::supportsDevice(vk::PhysicalDevice device) const
{
    auto features = device.getFeatures2KHR();

    auto supportsMultiview = [&]() {
        auto next = features.pNext;
        while (next != nullptr) {
            auto vk = reinterpret_cast<VkStruct *>(next);
            if (vk->type ==
                vk::StructureType::ePhysicalDeviceMultiviewFeaturesKHR) {
                auto pdmp = reinterpret_cast<
                    vk::PhysicalDeviceMultiviewFeaturesKHR *>(next);
                return pdmp->multiview &&
                       (pdmp->multiviewGeometryShader ||
                        !mRequireGeometryShader) &&
                       (pdmp->multiviewTessellationShader ||
                        !mRequireTesselationShader);
            } else {
                next = vk->next;
            }
        }
        return false;
    }();

    auto perViewSufficient = [&]() {
        auto props = device.getProperties2KHR();
        auto next = props.pNext;
        while (next != nullptr) {
            auto vk = reinterpret_cast<VkStruct *>(next);
            if (vk->type ==
                vk::StructureType::
                    ePhysicalDeviceMultiviewPerViewAttributesPropertiesNVX) {
                auto pdmpvap = reinterpret_cast<
                    vk::PhysicalDeviceMultiviewPerViewAttributesPropertiesNVX
                        *>(next);
                return !mRequirePerViewAllComponents ||
                       pdmpvap->perViewPositionAllComponents;
            }
        }
        return false;
    }();

    return supportsMultiview && perViewSufficient;
}

std::vector<const char *>
lava::features::MultiView::deviceExtensions()
{
    std::vector<const char *> result;
    result.push_back(VK_KHR_MULTIVIEW_EXTENSION_NAME);
    if (mRequirePerViewAttributes)
        result.push_back(
            VK_NVX_MULTIVIEW_PER_VIEW_ATTRIBUTES_EXTENSION_NAME);

    return result;
}

std::vector<const char *>
MultiView::instanceExtensions()
{
    return {VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME};
}

void
MultiView::addPhysicalDeviceFeatures(
    vk::PhysicalDeviceFeatures &features) const
{
    features.tessellationShader = mRequireTesselationShader;
    features.geometryShader = mRequireGeometryShader;
}

void
MultiView::addPhysicalDeviceFeatures2(NextPtr &next) const
{
    auto *nextVal = &next;
    while (*nextVal != nullptr) {
        if ((*nextVal)->type ==
            vk::StructureType::ePhysicalDeviceMultiviewFeaturesKHR) {
            auto ptr =
                reinterpret_cast<vk::PhysicalDeviceMultiviewFeaturesKHR *>(
                    *nextVal);
            ptr->multiview = true;
            ptr->multiviewGeometryShader |= mRequireGeometryShader;
            ptr->multiviewTessellationShader |= mRequireTesselationShader;
            return;
        }
        nextVal = &(*nextVal)->next;
    }

    mMultiviewFeatures.multiview = true;
    mMultiviewFeatures.multiviewGeometryShader = mRequireGeometryShader;
    mMultiviewFeatures.multiviewTessellationShader =
        mRequireTesselationShader;

    (*nextVal) = reinterpret_cast<VkStructHeader *>(&mMultiviewFeatures);
}
} // namespace features
} // namespace lava
