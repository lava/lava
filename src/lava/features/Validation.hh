#pragma once
#include "IFeature.hh"

namespace lava
{
namespace features
{

class Validation : public IFeature
{
  public:
    virtual ~Validation();
    static std::shared_ptr<Validation> create()
    {
        return std::make_shared<Validation>();
    }

    std::vector<const char *> layers() override;
    std::vector<const char *> instanceExtensions() override;
    void onInstanceCreated(Instance *instance) override;
    void beforeInstanceDestruction() override;

    void setDebugReportFlags(vk::DebugReportFlagsEXT flags)
    {
        mFlags = flags;
    }
    void addDebugReportFlags(vk::DebugReportFlagsEXT flags)
    {
        mFlags |= flags;
    }

    void pause() { mPaused = true; }
    void resume() { mPaused = false; }

    bool paused() { return mPaused; }

    size_t messageCount() const { return mMessageCount; }
    void bumpMessageCount() { mMessageCount++; }

  protected:
    bool mPaused = false;
    vk::Instance mInstance;
    vk::DebugReportCallbackEXT mCallback;
    vk::DebugReportFlagsEXT mFlags = vk::DebugReportFlagBitsEXT::eError |
                                     vk::DebugReportFlagBitsEXT::eWarning;
    size_t mMessageCount = 0;
};
} // namespace features
} // namespace lava
