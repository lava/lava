#pragma once
#include <memory>

#define LAVA_FORWARD_DECLARE_CLASS(T)                                      \
    class T;                                                               \
    using Shared##T = std::shared_ptr<T>;                                  \
    using Weak##T = std::weak_ptr<T>;                                      \
    using Unique##T = std::unique_ptr<T>

#define LAVA_FORWARD_DECLARE_STRUCT(T)                                     \
    struct T;                                                              \
    using Shared##T = std::shared_ptr<T>;                                  \
    using Weak##T = std::weak_ptr<T>;                                      \
    using Unique##T = std::unique_ptr<T>

namespace lava
{
LAVA_FORWARD_DECLARE_CLASS(Instance);
LAVA_FORWARD_DECLARE_CLASS(Device);
LAVA_FORWARD_DECLARE_CLASS(Buffer);
LAVA_FORWARD_DECLARE_CLASS(Image);
LAVA_FORWARD_DECLARE_CLASS(ImageView);
LAVA_FORWARD_DECLARE_CLASS(Memory);
LAVA_FORWARD_DECLARE_CLASS(CommandBuffer);
LAVA_FORWARD_DECLARE_CLASS(RenderPass);
LAVA_FORWARD_DECLARE_CLASS(PipelineLayout);
LAVA_FORWARD_DECLARE_CLASS(GraphicsPipeline);
LAVA_FORWARD_DECLARE_CLASS(ComputePipeline);
LAVA_FORWARD_DECLARE_CLASS(DescriptorSetLayout);
LAVA_FORWARD_DECLARE_CLASS(DescriptorPool);
LAVA_FORWARD_DECLARE_CLASS(DescriptorSet);
LAVA_FORWARD_DECLARE_CLASS(ShaderModule);
LAVA_FORWARD_DECLARE_CLASS(Framebuffer);
LAVA_FORWARD_DECLARE_CLASS(Sampler);
LAVA_FORWARD_DECLARE_CLASS(MemoryChunk);
LAVA_FORWARD_DECLARE_CLASS(Suballocator);
LAVA_FORWARD_DECLARE_CLASS(ImageData);
LAVA_FORWARD_DECLARE_CLASS(RayTracingPipeline);
LAVA_FORWARD_DECLARE_CLASS(BottomLevelAccelerationStructure);
LAVA_FORWARD_DECLARE_CLASS(TopLevelAccelerationStructure);

LAVA_FORWARD_DECLARE_STRUCT(Subpass);

namespace features
{
LAVA_FORWARD_DECLARE_CLASS(GlfwOutput);
LAVA_FORWARD_DECLARE_CLASS(GlfwWindow);
LAVA_FORWARD_DECLARE_CLASS(RayTracing);
LAVA_FORWARD_DECLARE_CLASS(Validation);
} // namespace features

class RecordingCommandBuffer;
class InlineSubpass;
class PipelineVertexInputStateCreateInfo;
class GraphicsPipelineCreateInfo;
class ComputePipelineCreateInfo;
class Queue;
class ActiveRenderPass;
struct QueueRequest;

struct BufferBarrier;
} // namespace lava

#undef LAVA_FORWARD_DECLARE_CLASS
