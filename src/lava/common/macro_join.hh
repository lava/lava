#pragma once

#define LAVA_MACRO_JOIN_IMPL(arg1, arg2) arg1##arg2
#define LAVA_MACRO_JOIN(arg1, arg2) LAVA_MACRO_JOIN_IMPL(arg1, arg2)
