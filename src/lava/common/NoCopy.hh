#pragma once

#define LAVA_NON_COPYABLE(T)                                               \
    T(T const &) = delete;                                                 \
    T &operator=(T const &) = delete;                                      \
    T &operator=(T &&) = default;                                          \
    T(T &&) = default

#define LAVA_NON_MOVABLE(T)                                                \
    T(T const &) = delete;                                                 \
    T(T &&) = delete;                                                      \
    T &operator=(T const &) = delete;                                      \
    T &operator=(T &&) = delete

#define LAVA_RAII_CLASS(CLASS)                                             \
    CLASS(CLASS const &) = delete;                                         \
    CLASS &operator=(CLASS const &) = delete;                              \
    CLASS &operator=(CLASS &&) = delete
