#pragma once

#include "macro_join.hh"

/** LAVA_GETTER(name) LAVA_SETTER(name) LAVA_PROPERTY(name)
 * as non-typed getter/setter macros
 *
 * Usage:
 * class Foo {
 * private:
 *    int mBar;
 *    bool mFinished;
 *
 * public:
 *    LAVA_PROPERTY(Bar); // generates getBar() and setBar(...) with const&
 * set and get LAVA_PROPERTY_IS(Finished); // isFinished() and setFinished()
 * };
 *
 * CAUTION: these macros can only be used _after_ the member is declared
 * (due to type deduction)
 */

#define LAVA_GETTER(name)                                                  \
    auto get##name() const->decltype(m##name) const & { return m##name; }  \
    friend class LAVA_MACRO_JOIN(___get_, __COUNTER__)

#define LAVA_SETTER(name)                                                  \
    void set##name(decltype(m##name) const &value) { m##name = value; }    \
    friend class LAVA_MACRO_JOIN(___set_, __COUNTER__)

#define LAVA_GETTER_IS(name)                                               \
    auto is##name() const->decltype(m##name) const & { return m##name; }   \
    friend class LAVA_MACRO_JOIN(___get_is_, __COUNTER__)

#define LAVA_PROPERTY(name)                                                \
    LAVA_GETTER(name);                                                     \
    LAVA_SETTER(name)

#define LAVA_PROPERTY_IS(name)                                             \
    LAVA_GETTER_IS(name);                                                  \
    LAVA_SETTER(name)
