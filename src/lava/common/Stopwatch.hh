#pragma once

#include <chrono>

namespace lava
{
class Stopwatch
{
  public:
    using Clock = std::chrono::high_resolution_clock;
    using Point = Clock::time_point;
    using Duration = Clock::duration;

    void reset() { mStarted = Clock::now(); }
    Duration time() const { return Clock::now() - mStarted; }

    double seconds() const
    {
        return std::chrono::duration<double>(time()).count();
    }

  private:
    Point mStarted = Clock::now();
};
} // namespace lava
