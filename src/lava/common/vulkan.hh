#pragma once

#ifdef _WIN32
#define VK_USE_PLATFORM_WIN32_KHR 1
#define VK_KHR_win32_surface 1
#ifndef NOMINMAX
#define NOMINMAX
#endif
#endif

#ifndef VK_NO_PROTOTYPES
#define VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#if VK_HEADER_VERSION < 124
#include <volk.h>
#endif
#endif

#include <vulkan/vulkan.hpp>