#pragma once
#include <chrono>
#include <lava/common/log.hh>
#include <math.h>

namespace lava
{

class FrameTimer
{
  public:
    using Clock = std::chrono::high_resolution_clock;
    using Point = Clock::time_point;
    using Duration = Clock::duration;

    FrameTimer() { mLastPrint = Clock::now(); }

    void beginCpuBlock() { mCpuStart = Clock::now(); }

    void endCpuBlock() { mCpuTime += Clock::now() - mCpuStart; }

    void frame(Duration maxTimeBeforePrint = std::chrono::seconds(1),
               uint32_t maxFramesBeforePrint = 90)
    {
        mNumFrames++;

        auto now = Clock::now();
        if ((mLastPrint + maxTimeBeforePrint <= now) ||
            (mNumFrames >= maxFramesBeforePrint)) {
            auto millis =
                std::chrono::duration<double, std::milli>(now - mLastPrint)
                    .count();
            auto cpu_millis =
                std::chrono::duration<double, std::milli>(mCpuTime).count();
            mCpuTime = {};
            auto fps = double(mNumFrames) / millis * 1000.0;

            lava::info() << "[FrameTimer]: " << mNumFrames << " drawn in "
                         << std::round(millis) << "ms (" << cpu_millis
                         << "ms on CPU; " << fps << " FPS / "
                         << std::round(millis / double(mNumFrames))
                         << "ms per frame)";

            mNumFrames = 0;
            mLastPrint = now;
        }
    }

  private:
    Point mLastPrint;
    uint32_t mNumFrames = 0;
    Duration mCpuTime = {};
    Point mCpuStart;
};
} // namespace lava
