#include "log.hh"

#include <cassert>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>

std::ostream *lava::internal::logStream = nullptr;
std::ostream *lava::internal::logStreamError = nullptr;
std::string lava::internal::logPrefix = "[$t][$l] ";
uint8_t lava::internal::logMask = 0xFF;

void
lava::setLogStream(std::ostream *ossNormal, std::ostream *ossError)
{
    internal::logStream = ossNormal;
    internal::logStreamError = ossError;
}

lava::internal::LogObject::LogObject(std::ostream *oss, LogLevel lvl)
    : oss(oss)
{
    auto prefix = logPrefix; // copy

    // replace $l
    auto pos = prefix.find("$l");
    if (pos != std::string::npos) {
        const char *stype = nullptr;
        switch (lvl) {
        case LogLevel::Info:
            stype = "Info";
            break;
        case LogLevel::Debug:
            stype = "Debug";
            break;
        case LogLevel::Error:
            stype = "Error";
            break;
        case LogLevel::Warning:
            stype = "Warning";
            break;
        case LogLevel::Validation:
            stype = "Validation";
            break;
        }

        prefix.replace(pos, 2, stype);
    }

    // replace $t
    pos = prefix.find("$t");
    if (pos != std::string::npos) {
        auto now = std::chrono::system_clock::to_time_t(
            std::chrono::system_clock::now());
        char timestr[10];
        std::strftime(timestr, sizeof(timestr), "%H:%M:%S",
                      std::localtime(&now));

        prefix.replace(pos, 2, timestr);
    }

    *this << prefix;
}

lava::internal::LogObject::~LogObject()
{
    if (oss)
        *oss << std::endl;
}

void
lava::setLogPrefix(const std::string &prefix)
{
    internal::logPrefix = prefix;
}

std::ostream *
lava::getLogStreamError()
{
    return internal::logStreamError ? internal::logStreamError : &std::cerr;
}

std::ostream *
lava::getLogStream()
{
    return internal::logStream ? internal::logStream : &std::cout;
}

lava::internal::LogObject
lava::log(LogLevel lvl)
{
    if (!((uint8_t)lvl & internal::logMask))
        return {nullptr, lvl}; // masked

    switch (lvl) {
    case LogLevel::Info:
        return {getLogStream(), lvl};
    case LogLevel::Warning:
    case LogLevel::Error:
    case LogLevel::Validation:
        return {getLogStreamError(), lvl};
    case LogLevel::Debug:
#ifdef LAVA_DEBUG
        return {getLogStream(), lvl};
#else
        return {nullptr, lvl}; // no output
#endif
    default:
        assert(0 && "unknown log level");
        return {nullptr, lvl}; // error, wrong log level
    }
}

void
lava::setLogMask(uint8_t mask)
{
    internal::logMask = (uint8_t)mask;
}

void
lava::setLogMask(lava::LogLevel mask)
{
    internal::logMask = (uint8_t)mask;
}

uint8_t
lava::getLogMask()
{
    return internal::logMask;
}
