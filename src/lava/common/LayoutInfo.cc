#include "LayoutInfo.hh"

namespace lava
{

vk::AccessFlags
flagsForLayout(vk::ImageLayout layout)
{
    switch (layout) {
    case vk::ImageLayout::eUndefined:
        return {};
    case vk::ImageLayout::eGeneral:
        return vk::AccessFlagBits::eShaderRead |                  //
               vk::AccessFlagBits::eShaderWrite |                 //
               vk::AccessFlagBits::eColorAttachmentRead |         //
               vk::AccessFlagBits::eColorAttachmentWrite |        //
               vk::AccessFlagBits::eDepthStencilAttachmentRead |  //
               vk::AccessFlagBits::eDepthStencilAttachmentWrite | //
               vk::AccessFlagBits::eTransferRead |                //
               vk::AccessFlagBits::eTransferWrite |               //
               vk::AccessFlagBits::eHostRead |                    //
               vk::AccessFlagBits::eHostWrite |                   //
               vk::AccessFlagBits::eMemoryRead |                  //
               vk::AccessFlagBits::eMemoryWrite;                  //
    case vk::ImageLayout::eColorAttachmentOptimal:
        return vk::AccessFlagBits::eColorAttachmentWrite;
    case vk::ImageLayout::eDepthStencilAttachmentOptimal:
        return vk::AccessFlagBits::eDepthStencilAttachmentWrite;
    case vk::ImageLayout::eDepthStencilReadOnlyOptimal:
        return vk::AccessFlagBits::eDepthStencilAttachmentRead;
    case vk::ImageLayout::eShaderReadOnlyOptimal:
        return vk::AccessFlagBits::eShaderRead;
    case vk::ImageLayout::eTransferSrcOptimal:
        return vk::AccessFlagBits::eTransferRead;
    case vk::ImageLayout::eTransferDstOptimal:
        return vk::AccessFlagBits::eTransferWrite;
    case vk::ImageLayout::ePreinitialized:
        return vk::AccessFlagBits::eHostWrite;
    case vk::ImageLayout::ePresentSrcKHR:
    case vk::ImageLayout::eSharedPresentKHR:
        return {};
    case vk::ImageLayout::eDepthReadOnlyStencilAttachmentOptimalKHR:
        return vk::AccessFlagBits::eDepthStencilAttachmentRead |
               vk::AccessFlagBits::eDepthStencilAttachmentWrite;
    case vk::ImageLayout::eDepthAttachmentStencilReadOnlyOptimalKHR:
        return vk::AccessFlagBits::eDepthStencilAttachmentRead |
               vk::AccessFlagBits::eDepthStencilAttachmentWrite;
    }
    return {};
}

vk::PipelineStageFlagBits
stageForLayout(vk::ImageLayout layout)
{
    switch (layout) {
    case vk::ImageLayout::eGeneral:
        return vk::PipelineStageFlagBits::eAllCommands;
    case vk::ImageLayout::eColorAttachmentOptimal:
        return vk::PipelineStageFlagBits::eColorAttachmentOutput;
    case vk::ImageLayout::eDepthStencilAttachmentOptimal:
    case vk::ImageLayout::eDepthStencilReadOnlyOptimal:
        return vk::PipelineStageFlagBits::eLateFragmentTests;
    case vk::ImageLayout::eShaderReadOnlyOptimal:
        return vk::PipelineStageFlagBits::eFragmentShader;
    case vk::ImageLayout::eTransferSrcOptimal:
    case vk::ImageLayout::eTransferDstOptimal:
        return vk::PipelineStageFlagBits::eTransfer;
    case vk::ImageLayout::ePreinitialized:
        return vk::PipelineStageFlagBits::eHost;

    default:
        // vk::ImageLayout::ePresentSrcKHR:
        // vk::ImageLayout::eSharedPresentKHR:
        // vk::ImageLayout::eUndefined:
        return vk::PipelineStageFlagBits::eTopOfPipe;
    }
}
} // namespace lava
