#include "ShaderExtensions.hh"
#include <vector>

namespace lava
{
using Stage = vk::ShaderStageFlagBits;

namespace
{

std::vector<std::pair<std::string, Stage>> stageForEnding = {
    {"vert.spv", Stage::eVertex},
    {"vert", Stage::eVertex},
    {"frag.spv", Stage::eFragment},
    {"frag", Stage::eFragment},
    {"geom.spv", Stage::eGeometry},
    {"geom", Stage::eGeometry},
    {"tcsh.spv", Stage::eTessellationControl},
    {"tcsh", Stage::eTessellationControl},
    {"tesc.spv", Stage::eTessellationControl},
    {"tesc", Stage::eTessellationControl},
    {"tesh.spv", Stage::eTessellationEvaluation},
    {"tesh", Stage::eTessellationEvaluation},
    {"tese.spv", Stage::eTessellationEvaluation},
    {"tese", Stage::eTessellationEvaluation},
    {"comp.spv", Stage::eCompute},
    {"comp", Stage::eCompute},
    {"rgen.spv", Stage::eRaygenNV},
    {"rgen", Stage::eRaygenNV},
    {"rint.spv", Stage::eIntersectionNV},
    {"rint", Stage::eIntersectionNV},
    {"rahit.spv", Stage::eAnyHitNV},
    {"rahit", Stage::eAnyHitNV},
    {"rchit.spv", Stage::eClosestHitNV},
    {"rchit", Stage::eClosestHitNV},
    {"rmiss.spv", Stage::eMissNV},
    {"rmiss", Stage::eMissNV},
    {"rcall.spv", Stage::eCallableNV},
    {"rcall", Stage::eCallableNV},
    {"task.spv", Stage::eTaskNV},
    {"task", Stage::eTaskNV},
    {"mesh.spv", Stage::eMeshNV},
    {"mesh", Stage::eMeshNV}};

} // namespace

optional<vk::ShaderStageFlagBits>
identifyShader(const std::string &filename)
{
    for (auto const &s : stageForEnding)
        if (filename.rfind(s.first) == filename.length() - s.first.length())
            return s.second;

    return nullopt;
}
} // namespace lava
