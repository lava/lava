#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{

/// The formats in Vulkan are pretty verbose compared to their OpenGL
/// counterparts
/// this enum class offers a shorter alternative for formats known from
/// OpenGL
enum class Format {
    R8 = VK_FORMAT_R8_UNORM,
    R8_SNORM = VK_FORMAT_R8_SNORM,
    R16 = VK_FORMAT_R16_UNORM,
    R16_SNORM = VK_FORMAT_R16_SNORM,
    RG8 = VK_FORMAT_R8G8_UNORM,
    RG8_SNORM = VK_FORMAT_R8G8_SNORM,
    RG16 = VK_FORMAT_R16G16_UNORM,
    RG16_SNORM = VK_FORMAT_R16G16_SNORM,
    // R3_G3_B2
    // RGB4
    // RGB5
    RGB8 = VK_FORMAT_R8G8B8_UNORM,
    RGB8_SNORM = VK_FORMAT_R8G8B8_SNORM,
    // RGB10
    // RGB12
    RGB16_SNORM = VK_FORMAT_R16G16B16_SNORM,
    // RGBA2
    RGBA4 = VK_FORMAT_R4G4B4A4_UNORM_PACK16,
    RGB5_A1 = VK_FORMAT_R5G5B5A1_UNORM_PACK16,
    RGBA8 = VK_FORMAT_R8G8B8A8_UNORM,
    RGBA8_SNORM = VK_FORMAT_R8G8B8A8_SNORM,
    // RGB10_A2
    // RGB10_A2UI
    // RGBA12
    RGBA16 = VK_FORMAT_R16G16B16A16_UNORM,
    SRGB8 = VK_FORMAT_R8G8B8_SRGB,
    SRGB8_ALPHA8 = VK_FORMAT_R8G8B8A8_SRGB,
    SBGR8_ALPHA8 = VK_FORMAT_B8G8R8A8_SRGB,
    R16F = VK_FORMAT_R16_SFLOAT,
    RG16F = VK_FORMAT_R16G16_SFLOAT,
    RGB16F = VK_FORMAT_R16G16B16_SFLOAT,
    RGBA16F = VK_FORMAT_R16G16B16A16_SFLOAT,
    R32F = VK_FORMAT_R32_SFLOAT,
    RG32F = VK_FORMAT_R32G32_SFLOAT,
    RGB32F = VK_FORMAT_R32G32B32_SFLOAT,
    RGBA32F = VK_FORMAT_R32G32B32A32_SFLOAT,
    // R11F_G11F_B10F
    // RGB9_E5
    R8I = VK_FORMAT_R8_SINT,
    R8UI = VK_FORMAT_R8_UINT,
    R16I = VK_FORMAT_R16_SINT,
    R16UI = VK_FORMAT_R16_UINT,
    R32I = VK_FORMAT_R32_SINT,
    R32UI = VK_FORMAT_R32_UINT,
    RG8I = VK_FORMAT_R8G8_SINT,
    RG8UI = VK_FORMAT_R8G8_UINT,
    RG16I = VK_FORMAT_R16G16_SINT,
    RG16UI = VK_FORMAT_R16G16_UINT,
    RG32I = VK_FORMAT_R32G32_SINT,
    RG32UI = VK_FORMAT_R32G32_UINT,
    RGB8I = VK_FORMAT_R8G8B8_SINT,
    RGB8UI = VK_FORMAT_R8G8B8_UINT,
    RGB16I = VK_FORMAT_R16G16B16_SINT,
    RGB16UI = VK_FORMAT_R16G16B16_UINT,
    RGB32I = VK_FORMAT_R32G32B32_SINT,
    RGB32UI = VK_FORMAT_R32G32B32_UINT,
    RGBA8I = VK_FORMAT_R8G8B8A8_SINT,
    RGBA8UI = VK_FORMAT_R8G8B8A8_UINT,
    RGBA16I = VK_FORMAT_R16G16B16A16_SINT,
    RGBA16UI = VK_FORMAT_R16G16B16A16_UINT,
    RGBA32I = VK_FORMAT_R32G32B32A32_SINT,
    RGBA32UI = VK_FORMAT_R32G32B32A32_UINT,
    DEPTH_COMPONENT16 = VK_FORMAT_D16_UNORM,
    DEPTH24_STENCIL8 = VK_FORMAT_D24_UNORM_S8_UINT,
    DEPTH_COMPONENT32F = VK_FORMAT_D32_SFLOAT,
    ALPHA8_SBGR8 = VK_FORMAT_A8B8G8R8_SRGB_PACK32,
    ABGR8 = VK_FORMAT_A8B8G8R8_SNORM_PACK32
};
/// This type is mainly meant for function parameters, such that the
/// function can take the Vulkan (C) formats, the vulkan.hpp formats, or the
/// GlLikeFormats defined by lava
struct GenericFormat {
  public:
    GenericFormat(VkFormat format) { mFormat = vk::Format(format); }
    GenericFormat(vk::Format format) : mFormat(format) {}
    GenericFormat(Format format) { mFormat = vk::Format(VkFormat(format)); }

    vk::Format vkhpp() const { return mFormat; }
    VkFormat vk() const { return VkFormat(mFormat); }
    Format lava() const { return Format(VkFormat(mFormat)); }

  protected:
    vk::Format mFormat;
};
} // namespace lava
