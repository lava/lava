#pragma once
#include "Activatable.hh"

namespace lava
{

namespace internal
{

template <class T> struct ActivatableState {
    static thread_local std::shared_ptr<T> tCurrent;
    static std::shared_ptr<T> sMain;
    static std::mutex sMainMutex;
};

template <class T>
thread_local std::shared_ptr<T> ActivatableState<T>::tCurrent;
template <class T> std::shared_ptr<T> ActivatableState<T>::sMain;
template <class T> std::mutex ActivatableState<T>::sMainMutex;

template <class T>
void
Activatable<T>::registerInstance(const std::shared_ptr<T> &ptr)
{
    if (!ActivatableState<T>::sMain) {
        std::lock_guard<std::mutex> lock(ActivatableState<T>::sMainMutex);
        if (!ActivatableState<T>::sMain)
            ActivatableState<T>::sMain = ptr;
    }
}

template <class T>
Activatable<T>::Active::Active(const std::shared_ptr<T> &target)
{
    mPrevious = ActivatableState<T>::tCurrent;
    ActivatableState<T>::tCurrent = target;
}

template <class T>
const std::shared_ptr<T> &
Activatable<T>::getCurrent()
{
    if (!ActivatableState<T>::tCurrent) {
        ActivatableState<T>::tCurrent = ActivatableState<T>::sMain;
    }
    return ActivatableState<T>::tCurrent;
}

} // namespace internal

} // namespace lava

#define LAVA_ACTIVATABLE_INSTANTIATE(Type)                                 \
    template class lava::internal::Activatable<Type>;
