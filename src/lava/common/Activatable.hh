#pragma once
#include <lava/common/NoCopy.hh>
#include <memory>
#include <mutex>

namespace lava
{

namespace internal
{

template <class T>
class Activatable : public std::enable_shared_from_this<T>
{
  private:
    using std::enable_shared_from_this<T>::shared_from_this;

  protected:
    static void registerInstance(std::shared_ptr<T> const &ptr);

  public:
    class Active
    {
      public:
      protected:
        std::shared_ptr<T> mPrevious;
        Active(std::shared_ptr<T> const &target);
        ~Active();
        friend class Activatable<T>;
    };

    Active use() { return {shared_from_this()}; }
    static std::shared_ptr<T> const &getCurrent();
};

} // namespace internal

} // namespace lava
