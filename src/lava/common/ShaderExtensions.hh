#pragma once
#include <lava/common/optional.hh>
#include <lava/common/vulkan.hh>

namespace lava
{
/**
 * Try and guess the shader stage from the filename.
 * The stage is identified based on the four-letter-codes that
 *glslLangValidator (the GLSL compiler that comes with the VulkanSDK) uses.
 *
 * Example file names:
 * example.vert.spv <- Vertex Shader
 * example.frag.spv <- Fragment Shader
 * example.vert <- Vertex Shader
 *
 **/
optional<vk::ShaderStageFlagBits>
identifyShader(std::string const &filename);
} // namespace lava
