#pragma once
#include <algorithm>
#include <cstddef>
#include <iostream>

namespace lava
{

static constexpr size_t arena_page_size = 512;
static constexpr size_t first_page_size = 512;
class Arena
{
  public:
    void *alloc(size_t size)
    {
        if (!head) {
            head = makePage(std::max(size, arena_page_size));
        } else if (head->end - head->head < ptrdiff_t(size)) {
            auto next_head = makePage(std::max(size, arena_page_size));
            next_head->prev = head;
            head = next_head;
        }

        void *res = head->head;
        head->head += size;
        return res;
    }

    template <class T> T *alloc(size_t count = 1)
    {
        void *ptr = alloc(count * sizeof(T));
        return new (ptr) T[count];
    }

    Arena()
    {
        head = new (sbo) Page();
        head->prev = nullptr;
        head->head = sbo + sizeof(Page);
        head->end = head->head + first_page_size;
    }

    ~Arena()
    {
        while ((char *)head != sbo) {
            auto next = head->prev;
            delete[] reinterpret_cast<char *>(head);
            head = next;
        }
    }

  private:
    struct Page {
        Page *prev;
        char *head;
        char *end;
    };

    Page *makePage(size_t size)
    {
        auto bytes = sizeof(Page) + size;
        auto *mem = new char[bytes];
        auto *page = new (mem) Page();
        page->prev = nullptr;
        page->head = mem + sizeof(Page);
        page->end = page->head + size;
        return page;
    }

    Page *head = nullptr;
    char sbo[first_page_size + sizeof(Page)];
};

} // namespace lava
