#pragma once

#ifdef LAVA_GLM_AVAILABLE
#include <glm/mat4x3.hpp>
#endif

namespace lava
{

struct RigidTransform {
    RigidTransform() = default;
    RigidTransform(RigidTransform const &) = default;

    /// Expects a 4x3 values (column-major order)
    RigidTransform(float const *vals)
    {
        for (int i = 0; i < (4 * 3); i++)
            matrix[i] = vals[i];
    }

#ifdef LAVA_GLM_AVAILABLE
    RigidTransform(glm::mat4x3 const &m) : RigidTransform(&m[0].x) {}
#endif

    float matrix[12] = {1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0};
};

} // namespace lava
