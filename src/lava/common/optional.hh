#pragma once
#include <exception>
#include <memory>
#include <type_traits>
#include <utility>

namespace lava
{
struct nullopt_t {
};
struct bad_optional_access : std::exception {
};

template <typename T> class optional
{
  public:
    using self_t = optional<T>;

    /* (de)construction */

    constexpr optional() noexcept {}
    constexpr optional(nullopt_t) noexcept {}
    constexpr optional(const optional &other) { *this = other; }
    constexpr optional(T const &d) { *this = d; }
    constexpr optional(T &&d) noexcept { *this = std::move(d); }
    ~optional() { reset(); }

    /* assignment */

    self_t &operator=(self_t const &o)
    {
        if (!o.mActive) {
            reset();
        } else {
            (*this) = *o;
        }
        return *this;
    }

    self_t &operator=(self_t &&o)
    {
        if (!o.mActive) {
            reset();
        } else {
            (*this) = *std::move(o);
        }
        return *this;
    }

    self_t &operator=(T const &d)
    {
        if (mActive) {
            **this = d;
        } else {
            emplace(d);
        }
        return *this;
    }

    self_t &operator=(T &&d)
    {
        if (mActive) {
            **this = std::move(d);
        } else {
            emplace(std::move(d));
        }
        return *this;
    }

    /* pointer-like access */

    T *operator->() { return &(**this); }
    T const *operator->() const { return &(**this); }

    /* dereferencing */

    T &operator*() & { return *reinterpret_cast<T *>(&mStorage); }
    T const &operator*() const &
    {
        return *reinterpret_cast<T const *>(&mStorage);
    }
    T &&operator*() &&
    {
        return std::move(*reinterpret_cast<T *>(&mStorage));
    }

    T &value()
    {
        if (!mActive)
            throw bad_optional_access();
        return **this;
    }
    T const &value() const
    {
        if (!mActive)
            throw bad_optional_access();
        return **this;
    }

    /* checking for contents */

    operator bool() const { return mActive; }
    bool has_value() const { return mActive; }

    /* modifiers */

    void reset()
    {
        if (mActive) {
            (**this).~T();
            mActive = false;
        }
    }

    template <typename... TS> void emplace(TS &&... args)
    {
        reset();
        new (&mStorage) T(std::forward<TS>(args)...);
        mActive = true;
    }

  private:
    using storage_t =
        typename std::aligned_storage<sizeof(T), alignof(T)>::type;

    bool mActive = false;
    storage_t mStorage;
};
const auto nullopt = nullopt_t{};
} // namespace lava
