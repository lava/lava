#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{

vk::AccessFlags flagsForLayout(vk::ImageLayout layout);
vk::PipelineStageFlagBits stageForLayout(vk::ImageLayout layout);

} // namespace lava
