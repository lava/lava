#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{
vk::ImageAspectFlags aspectsOf(vk::Format format);
size_t bytePerPixel(vk::Format format);

template <class DataT> struct vkTypeOf {
    static vk::Format format;
};

/* to declare your own types for lava:
 * template <> vk::Format lava::vkTypeOf<YourOwnType>::format
 *     = vk::Format::TheSuitableVulkanFormat;
 */
} // namespace lava
