#include "Barriers.hh"
#include <lava/objects/Buffer.hh>
#include <lava/objects/CommandBuffer.hh>

namespace lava
{

BufferBarrier::BufferBarrier() { mEnabled = false; }

BufferBarrier::BufferBarrier(RecordingCommandBuffer &cmd, vk::Buffer buffer)
{
    vk.dstQueueFamilyIndex = vk.srcQueueFamilyIndex =
        VK_QUEUE_FAMILY_IGNORED;
    vk.buffer = buffer;
    vk.offset = 0;
    vk.size = VK_WHOLE_SIZE;
    mTarget = cmd.buffer()->handle();
}

BufferBarrier::~BufferBarrier()
{
    if (mEnabled) {
        mTarget.pipelineBarrier(mSrcStage, mDstStage, {}, {}, {vk}, {});
    }
}

} // namespace lava
