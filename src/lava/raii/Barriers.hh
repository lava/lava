#pragma once
#include <lava/common/vulkan.hh>
#include <lava/fwd.hh>

namespace lava
{

struct Barrier {
    void drop() { mEnabled = false; }

  protected:
    bool mEnabled = true;
    vk::CommandBuffer mTarget;
    vk::PipelineStageFlags mSrcStage;
    vk::PipelineStageFlags mDstStage;
};

struct BufferBarrier : Barrier {
    BufferBarrier(); /// returns an inactive barrier
    BufferBarrier(RecordingCommandBuffer &cmd, vk::Buffer buffer);

    BufferBarrier &addSrcStage(vk::PipelineStageFlags bits)
    {
        mSrcStage |= bits;
        return *this;
    }
    BufferBarrier &addDstStage(vk::PipelineStageFlags bits)
    {
        mDstStage |= bits;
        return *this;
    }

    BufferBarrier &addSrcAccess(vk::AccessFlags bits)
    {
        vk.srcAccessMask |= bits;
        return *this;
    }
    BufferBarrier &addDstAccess(vk::AccessFlags bits)
    {
        vk.dstAccessMask |= bits;
        return *this;
    }

    BufferBarrier &setRange(vk::DeviceSize offset,
                            vk::DeviceSize size = VK_WHOLE_SIZE)
    {
        vk.offset = offset;
        vk.size = size;
        return *this;
    }

    vk::BufferMemoryBarrier vk;

    ~BufferBarrier();
};

} // namespace lava
