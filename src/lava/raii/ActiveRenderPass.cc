#include "ActiveRenderPass.hh"
#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/GraphicsPipeline.hh>
#include <lava/objects/PipelineLayout.hh>

namespace lava
{

SecondarySubpass::SecondarySubpass(const ActiveRenderPass &pass,
                                   uint32_t subpass,
                                   vk::CommandBuffer commandBuffer)
    : mPass(pass), mSubpassIndex(subpass), mCommandBuffer(commandBuffer)
{
}

void
SecondarySubpass::checkSubpass()
{
    assert((mSubpassIndex == mPass.currentPassIndex()) &&
           "You can't issue commands to this subpass after starting "
           "another one.");
}

InlineSubpass::InlineSubpass(const ActiveRenderPass &pass, uint32_t subpass,
                             RecordingCommandBuffer &commandBuffer)
    : mPass(pass), mSubpassIndex(subpass), mCommandBuffer(commandBuffer)
{
}

void
InlineSubpass::bindPipeline(const SharedGraphicsPipeline &pip)
{
    mCommandBuffer.mLastLayout = pip->layout();
    mCommandBuffer->bindPipeline(vk::PipelineBindPoint::eGraphics,
                                 pip->handle());
}

void
InlineSubpass::setScissors(vk::ArrayProxy<const Scissor> scissors,
                           uint32_t first)
{
    mCommandBuffer->setScissor(
        first, scissors.size(),
        static_cast<const vk::Rect2D *>(scissors.data()));
}

void
InlineSubpass::bindDescriptorSets(
    const std::vector<SharedDescriptorSet> &descriptors,
    const std::vector<uint32_t> &offsets, uint32_t firstSet,
    vk::PipelineBindPoint bindPoint, const SharedPipelineLayout &layout)
{
    mCommandBuffer.bindDescriptorSets(descriptors, offsets, firstSet,
                                      bindPoint, layout);
}

void
InlineSubpass::bindDescriptorSets(
    const std::vector<SharedDescriptorSet> &descriptors, uint32_t firstSet,
    vk::PipelineBindPoint bindPoint, const SharedPipelineLayout &layout)
{

    mCommandBuffer.bindDescriptorSets(descriptors, firstSet, bindPoint,
                                      layout);
}

void
InlineSubpass::bindIndexBuffer(const SharedBuffer &buffer,
                               vk::IndexType type, size_t offset)
{
    mCommandBuffer->bindIndexBuffer(buffer->handle(), offset, type);
    mCommandBuffer.attachResource(buffer);
}

void
InlineSubpass::bindVertexBuffers(const std::vector<SharedBuffer> &buffers,
                                 uint32_t first)
{
    std::vector<vk::Buffer> vkBuffers;
    for (auto &&b : buffers) {
        vkBuffers.push_back(b->handle());
        mCommandBuffer.attachResource(b);
    }
    std::vector<vk::DeviceSize> offsets;
    offsets.resize(vkBuffers.size());

    mCommandBuffer->bindVertexBuffers(first, vkBuffers, offsets);
}

void
InlineSubpass::bindVertexBuffers(const std::vector<SharedBuffer> &buffers,
                                 const std::vector<vk::DeviceSize> &offsets,
                                 uint32_t first)
{
    assert(buffers.size() == offsets.size());

    std::vector<vk::Buffer> vkBuffers;
    for (auto &&b : buffers) {
        vkBuffers.push_back(b->handle());
        mCommandBuffer.attachResource(b);
    }

    mCommandBuffer->bindVertexBuffers(
        first, vkBuffers, vk::ArrayProxy<const vk::DeviceSize>(offsets));
}

void
InlineSubpass::pushConstants(uint32_t dataSize, const void *data,
                             uint32_t offset,
                             vk::ShaderStageFlags stageFlags,
                             const SharedPipelineLayout &layout)
{
    mCommandBuffer.pushConstants(dataSize, data, offset, stageFlags,
                                 layout);
}

void
InlineSubpass::pushConstantBlock(uint32_t size, const void *data)
{
    mCommandBuffer.pushConstantBlock(size, data);
}

void
InlineSubpass::draw(uint32_t vertices, uint32_t instances,
                    uint32_t firstVertex, uint32_t firstInstance)
{

    mCommandBuffer->draw(vertices, instances, firstVertex, firstInstance);
}

void
InlineSubpass::drawIndexed(uint32_t indices, uint32_t instances,
                           int32_t vertexOffset, uint32_t firstIndex,
                           uint32_t firstInstance)
{
    mCommandBuffer->drawIndexed(indices, instances, firstIndex,
                                vertexOffset, firstInstance);
}

void
InlineSubpass::setViewports(vk::ArrayProxy<const Viewport> viewports,
                            uint32_t first)
{
    mCommandBuffer->setViewport(
        first, viewports.size(),
        static_cast<const vk::Viewport *>(viewports.data()));
}

void
InlineSubpass::checkSubpass()
{
    assert((mSubpassIndex == mPass.currentPassIndex()) &&
           "You can't issue commands to this subpass after starting "
           "another one.");
}

ActiveRenderPass::~ActiveRenderPass()
{
    if (mNextSubpass != 0) {
        mCmd.buffer()->handle().endRenderPass();
    }
}

InlineSubpass
ActiveRenderPass::startInlineSubpass()
{
    auto cmd = mCmd.buffer()->handle();
    if (mNextSubpass == 0) {
        cmd.beginRenderPass(mBeginInfo, vk::SubpassContents::eInline);
    } else {
        cmd.nextSubpass(vk::SubpassContents::eInline);
    }
    mNextSubpass++;
    return InlineSubpass(*this, mNextSubpass, mCmd);
}

SecondarySubpass
ActiveRenderPass::startSecondarySubpass()
{
    auto cmd = mCmd.buffer()->handle();
    if (mNextSubpass == 0) {
        cmd.beginRenderPass(mBeginInfo,
                            vk::SubpassContents::eSecondaryCommandBuffers);
    } else {
        cmd.nextSubpass(vk::SubpassContents::eSecondaryCommandBuffers);
    }
    mNextSubpass++;
    return SecondarySubpass(*this, mNextSubpass, cmd);
}
} // namespace lava
