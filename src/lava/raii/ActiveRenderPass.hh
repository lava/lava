#pragma once
#include <lava/common/NoCopy.hh>
#include <lava/common/vulkan.hh>
#include <lava/createinfos/RenderPassBeginInfo.hh>
#include <lava/createinfos/Scissor.hh>
#include <lava/createinfos/Viewport.hh>
#include <lava/objects/CommandBuffer.hh>

namespace lava
{

class ActiveRenderPass;

class InlineSubpass
{
  public:
    LAVA_NON_COPYABLE(InlineSubpass);
    InlineSubpass(ActiveRenderPass const &pass, uint32_t subpass,
                  RecordingCommandBuffer &commandBuffer);

    void bindPipeline(SharedGraphicsPipeline const &pip);
    void setViewports(vk::ArrayProxy<const Viewport> viewports,
                      uint32_t first = 0);
    void setScissors(vk::ArrayProxy<const Scissor> scissors,
                     uint32_t first = 0);

    /// Bind the given descriptor sets without offsetting, infers layout
    /// from last bound pipeline if omitted
    void bindDescriptorSets(
        std::vector<SharedDescriptorSet> const &descriptors,
        uint32_t firstSet = 0,
        vk::PipelineBindPoint bindPoint = vk::PipelineBindPoint::eGraphics,
        SharedPipelineLayout const &layout = nullptr);
    /// Bind the given descriptor sets with respective offsets
    /// infers layout from last bound pipeline if omitted
    void bindDescriptorSets(
        std::vector<SharedDescriptorSet> const &descriptors,
        std::vector<uint32_t> const &offsets, uint32_t firstSet = 0,
        vk::PipelineBindPoint bindPoint = vk::PipelineBindPoint::eGraphics,
        SharedPipelineLayout const &layout = nullptr);

    void bindIndexBuffer(SharedBuffer const &buffer,
                         vk::IndexType type = vk::IndexType::eUint32,
                         size_t offset = 0);
    void bindVertexBuffers(std::vector<SharedBuffer> const &buffers,
                           uint32_t first = 0);
    void bindVertexBuffers(std::vector<SharedBuffer> const &buffers,
                           std::vector<vk::DeviceSize> const &offsets,
                           uint32_t first = 0);
    void pushConstants(uint32_t dataSize, void const *data,
                       uint32_t offset = 0,
                       vk::ShaderStageFlags stageFlags =
                           vk::ShaderStageFlagBits::eAllGraphics,
                       SharedPipelineLayout const &layout = nullptr);

    template <typename T>
    void pushConstants(std::vector<T> const &data, uint32_t offset = 0,
                       vk::ShaderStageFlags stageFlags =
                           vk::ShaderStageFlagBits::eAllGraphics,
                       SharedPipelineLayout const &layout = nullptr)
    {
        pushConstants(sizeof(T) * data.size(), data.data(), offset,
                      stageFlags, layout);
    }

    void pushConstantBlock(uint32_t size, void const *data);

    template <typename T> void pushConstantBlock(T const &data)
    {
        pushConstantBlock(sizeof(T), &data);
    }

    void draw(uint32_t vertices, uint32_t instances = 1,
              uint32_t firstVertex = 0, uint32_t firstInstance = 0);
    void drawIndexed(uint32_t indices, uint32_t instances = 1,
                     int32_t vertexOffset = 0, uint32_t firstIndex = 0,
                     uint32_t firstInstance = 0);

    vk::CommandBuffer getCommandBuffer()
    {
        return mCommandBuffer.buffer()->handle();
    }

    ActiveRenderPass const &pass() const { return mPass; }

  protected:
    void checkSubpass();

    ActiveRenderPass const &mPass;
    uint32_t mSubpassIndex;
    RecordingCommandBuffer &mCommandBuffer;
};

class SecondarySubpass
{
  public:
    LAVA_NON_COPYABLE(SecondarySubpass);
    SecondarySubpass(ActiveRenderPass const &pass, uint32_t subpass,
                     vk::CommandBuffer commandBuffer);

    vk::CommandBuffer getCommandBuffer();

  protected:
    void checkSubpass();

    ActiveRenderPass const &mPass;
    uint32_t mSubpassIndex;
    vk::CommandBuffer mCommandBuffer;
};

class ActiveRenderPass
{
  public:
    LAVA_NON_COPYABLE(ActiveRenderPass);

    ActiveRenderPass(RecordingCommandBuffer &cmd,
                     RenderPassBeginInfo const &info)
        : mBeginInfo(info), mCmd(cmd)
    {
    }
    ~ActiveRenderPass();

    InlineSubpass startInlineSubpass();
    SecondarySubpass startSecondarySubpass();

    uint32_t currentPassIndex() const { return mNextSubpass - 1u; }

    SharedFramebuffer const &framebuffer() const
    {
        return mBeginInfo.framebuffer();
    }

  protected:
    RenderPassBeginInfo mBeginInfo;
    uint32_t mNextSubpass = 0;
    RecordingCommandBuffer &mCmd;
};
} // namespace lava
