#pragma once
#include <lava/common/vulkan.hh>

namespace lava
{

/// Defers the actual submission of the command buffer to the destruction of
/// the submitter
class Submitter
{
  public:
    Submitter &after(vk::Semaphore semaphore);
    Submitter &after(vk::Fence fence);

    Submitter &then(vk::Semaphore semaphore);
    Submitter &then(vk::Fence fence);

    void wait();

  protected:
    std::vector<vk::Fence> mFences;
    std::vector<vk::Semaphore> mSignalSemaphores;
    std::vector<vk::Semaphore> mWaitSemaphores;
};
} // namespace lava
