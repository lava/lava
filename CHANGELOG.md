# CommandBuffer GC

Big breaking change: In the past, CommandBuffers always waited for completion when submitted. This makes it easy to reason about object lifetimes (e.g. you're not allowed to delete a resource that is currently used inside a running CommandBuffer) and synchronization (e.g. you were safe to use `setData` on a Buffer and directly use the Buffer after that, because the CommandBuffer with the upload was already completed when `setData` returned).

The old behavior completely breaks the pipelining between CPU and GPU, as they always have to wait for the other.

As of now, submitting a CommandBuffer will thus return almost immediately. Under the hood, lava will use a fence to determine when the CommandBuffer submission has completed and keep the CommandBuffer (and thus any resources bound to it) alive until at least that point. It is thus still safe to submit a CommandBuffer and discard any references in your code.

This also means that the CPU can now get further "ahead" in recording CommandBuffers of the GPU, allowing you to have multiple frames "in-flight". To stop the CPU from getting too far ahead (which would increase delay and memory usage), Queues now come with the method `catchUp(int)` which will wait until at most the given numbers of submissions are pending (you will want to call this at the beginning of each frame usually).

The `setData()` method on Buffers now returns a `BufferBarrier`, when this object is destroyed, it'll insert a pipeline barrier that properly syncs the upload against upcoming usages (so in most cases, you'll just want to ignore the return value).

In later updates, features to join multiple barriers into one or replace multiple specific barriers with a single more generic one (e.g. BufferMemoryBarriers -> MemoryBarrier) can/will/should be added.

# GLM and GLFW

Glm and glfw are not included as submodules with lava anymore, but instead have to be provided by the user as CMakeTargets `glfw` and `glm_static`. This lets you use system-installed versions of these libraries, sharing libraries with other dependencies of your project, etc. Additionally, these dependencies might become optional in the future (core lava already works without glm, but most lava-extras modules still need it).

Existing applications can be fixed by adding glm and glfw as submodules (of the application, not lava) and using `add_subdirectory()` to add their targets before calling `add_subdirectory()` on lava itself.

# ec352c9d

Functions like `texture2D` (from `<lava/createinfos/Images.hh>`) now return a `lava::ImageCreateInfo`, rather than a `vk::ImageCreateInfo`, which also contains the desired view type of the image.

`Device::createImage` has been removed, instead use `ImageCreateInfo::create(device)`.
